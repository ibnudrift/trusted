<div class="leftmenu">        
    <ul class="nav nav-tabs nav-stacked">
        <li class="nav-header">Main Menu</li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/about/index')); ?>"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'About Us') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/about/index')); ?>">Why Choose a Trusted Surgeon</a></li>
                <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/admin/about/mission')); ?>">Our Mission</a></li> -->
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/about/why')); ?>">Trusted Team</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/about/me')); ?>">Founder</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/index')); ?>"><span class="fa fa-play"></span> <?php echo Tt::t('admin', 'Get Started') ?></a>
            <ul>
                <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/index')); ?>"><span class="fa fa-play"></span> <?php echo Tt::t('admin', 'Get Started') ?></a>
                    <ul>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/index')); ?>">Step 1</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step3')); ?>">Step 2</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step5')); ?>">Step 3</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step4')); ?>">Step 4</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step7')); ?>">Step 5</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step8')); ?>">Step 6</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/journey/step9')); ?>">Step 7</a></li>
                        <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/admin/journey/step6')); ?>">Step 6</a></li> -->
                        <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/admin/journey/step7')); ?>">Step 7</a></li>
                        <li><a href="<?php // echo CHtml::normalizeUrl(array('/admin/journey/step8')); ?>">Step 8</a></li>
                        <li><a href="<?php // echo CHtml::normalizeUrl(array('/admin/journey/step9')); ?>">Step 9</a></li>
                        <li><a href="<?php // echo CHtml::normalizeUrl(array('/admin/journey/step10')); ?>">Step 10</a></li> -->
                    </ul>
                </li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/gettingstarted/mentoring')); ?>">Mentoring</a></li>
                <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/education/faq')); ?>"><span class="fa fa-play"></span> <?php echo Tt::t('admin', 'FAQ') ?></a>
                    <ul>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/education/faq')); ?>">Page FAQ</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/faq/index')); ?>">List FAQ</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/gettingstarted/help')); ?>">Botched and Need Help?</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/procedures')); ?>"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'Procedures') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/procedures')); ?>">Landing Page Procedures</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/listprocedure/index')); ?>">View Procedures</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/listprocedure/create')); ?>">Add Procedures</a></li>
            </ul>
        </li>
        <!-- update professional -->
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/index')); ?>"><span class="fa fa-tag"></span> <?php echo Tt::t('admin', 'Industry') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/index')); ?>">Become a Trusted</a></li>
                <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>"><span class="fa fa-play"></span> <?php echo Tt::t('admin', 'Industry News') ?></a>
                    <ul>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/blog')); ?>">Cover</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/industryblog/index')); ?>">View Industry News</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/industryblog/create')); ?>">Add Industry News</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/clinic')); ?>">Clinic FAQ</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/advertise')); ?>">Become A Trusted Surgeon</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/advertise2')); ?>">Advestise With Us</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>"><span class="fa fa-tag"></span> <?php echo Tt::t('admin', 'News & Media') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/youtube/cover')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Cover') ?></a></li>
                <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/youtube/index')); ?>"><span class="fa fa-play"></span> <?php echo Tt::t('admin', 'Video') ?></a>
                    <ul>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/youtube/index')); ?>">View Video</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/youtube/create')); ?>">Add Video</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>"><span class="fa fa-play"></span> <?php echo Tt::t('admin', 'News') ?></a>
                    <ul>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>">View News</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/create')); ?>">Add News</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/contact/index')); ?>"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Contact Us') ?></a></li>
        <li class="nav-header">Other Menu</li>
        <li class="dropdown"><a href="#"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Surgeons Directory') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/customer/landing')); ?>">Landing Page Surgeons</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/customer/index')); ?>">Manage Patients/Doctors</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/memberreview/index')); ?>">Manage Reviews</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/memberclaim/index')); ?>">Manage Claimings</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/memberfeatured/index')); ?>">Manage Featured Doctors</a></li>
                <li class="dropdown"><a href="#"><span class="fa fa-table"></span> <?php echo Tt::t('admin', 'Taxonomy') ?></a>
                    <ul>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/spesialis/index')); ?>"><?php echo Tt::t('admin', 'List Specialization') ?></a></li>
                        <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/lsprocedure/index')); ?>"><?php echo Tt::t('admin', 'List Procedures') ?></a></li> -->
                    </ul>
                </li>
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Footer') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/privacy')); ?>">Privacy Policy</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/tos')); ?>">Terms and Conditions</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/download')); ?>">Advertise With Us</a></li>
            </ul>
        </li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Slide') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/insta/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Instagram Token') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/ads/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Advertising') ?></a></li>
        <?php /*
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/procedures/index')); ?>"><span class="fa fa-life-ring"></span> <?php echo Tt::t('admin', 'Procedures') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/procedures/index')); ?>">Landing Page Procedures</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/procedures/breast')); ?>">Breast</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/procedures/body')); ?>">Body</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/procedures/face')); ?>">Face</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/listprocedure/index')); ?>">List Procedures</a></li>
            </ul>
        </li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/coaching/index')); ?>"><span class="fa fa-group"></span> <?php echo Tt::t('admin', 'Coaching') ?></a></li>

        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>"><span class="fa fa-life-ring"></span> <?php echo Tt::t('admin', 'Manage Product') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>">View Product</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/create')); ?>">Add Product</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/promo/index')); ?>">Promo Code</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/category/index')); ?>">View Category</a></li>
            </ul>
        </li>
        */ ?>
        <?php /*
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/education/index')); ?>"><span class="fa fa-rocket"></span> <?php echo Tt::t('admin', 'Education') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/education/index')); ?>">3D Surgery Animation</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/education/faq')); ?>">Page FAQ</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/faq/index')); ?>">List FAQ</a></li>
            </ul>
        </li>
        */ ?>


        <?php /*
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/customer/index')); ?>"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Member/Doctor') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/customer/index')); ?>"><span class="fa fa-group"></span> <?php echo Tt::t('admin', 'Member/Doctor') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/about/index')); ?>"><span class="fa fa-file"></span> <?php echo Tt::t('admin', 'About Us') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-camera"></span> <?php echo Tt::t('admin', 'Slides') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/service/index')); ?>"><span class="fa fa-wrench"></span> <?php echo Tt::t('admin', 'Services Content') ?></a></li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/gallery/index')); ?>"><span class="fa fa-wrench"></span> <?php echo Tt::t('admin', 'Our Services') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/service/index')); ?>">Services Content</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/trip/index')); ?>">View Trip</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/pages/index')); ?>"><span class="fa fa-folder-open"></span> <?php echo Tt::t('admin', 'Pages') ?></a>
            <ul>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/pages/update', 'id'=>3)); ?>">About US</a></li> -->
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>">Blog/Artikel</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/pages/update', 'id'=>4)); ?>">Contact US</a></li>
            </ul>
        </li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/order/index')); ?>"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'Orders') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/customer/index')); ?>"><span class="fa fa-group"></span> <?php echo Tt::t('admin', 'Customers') ?></a></li>
        <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/admin/toko/index')); ?>"><span class="fa fa-group"></span> <?php // echo Tt::t('admin', 'Toko') ?></a></li> -->
        <!-- <li><a href="#"><span class="fa fa-bullhorn"></span> <?php // echo Tt::t('admin', 'Promotions') ?></a></li> -->
        <!-- <li><a href="#"><span class="fa fa-file-text-o"></span> <?php //'' echo Tt::t('admin', 'Reports') ?></a></li> -->
        <!-- class="dropdown" -->
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>"><span class="fa fa-camera"></span> <?php echo Tt::t('admin', 'Blogs') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>">View Blogs</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/create')); ?>">Add Blog</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/topik/index')); ?>">Category Blog</a></li> -->
            </ul>
        </li>
        */ ?>
        <li><a href="<?php echo CHtml::normalizeUrl(array('setting/index')); ?>"><span class="fa fa-cogs"></span> <?php echo Tt::t('admin', 'General Setting') ?></a>
             <!--  <ul>
                <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/admin/administrator/index')); ?>">Administrator Manager</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/language/index')); ?>">Language (Bahasa)</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/access_block/index')); ?>">Access Blok</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/contact/index')); ?>">Contact & Form Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/meta_page/index')); ?>">Default Meta Page</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/google_tools/index')); ?>">Google Tools</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/#/index')); ?>">Import/Export Product</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/purechat/index')); ?>">Integrasi PureChat</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/invoice_setting/index')); ?>">Invoice Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/logo_setting/index')); ?>">Logo Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/mail_setting/index')); ?>">Mail Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/mailchimp/index')); ?>">MailChimp</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/marketplace/index')); ?>">Market Place</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/mobile_text/index')); ?>">Mobile Text Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/payment/index')); ?>">Payment Setting</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/shipping/index')); ?>">Pengaturan Shipping</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/popOut/index')); ?>">Setting PopOut</a></li>
            </ul> -->
        </li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/home/logout')); ?>"><span class="fa fa fa-sign-out"></span> Logout</a></li>
    </ul>
</div><!--leftmenu-->
