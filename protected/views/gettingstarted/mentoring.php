<?php
$this->pageTitle = $this->setting['mentoring_title'].' - '.$this->pageTitle;
?>
<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['mentoring_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h2>GETTING STARTED</h2>
      <h3><?php echo $this->setting['mentoring_title'] ?></h3>
    </div>
  </div>
  <div class="clear"></div>
</div>
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="tagline"><?php echo $this->setting['mentoring_title'] ?></h1>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906 page_faq_content">
                <h4><?php echo nl2br($this->setting['mentoring_subtitle']) ?></h4>
                <div class="clear"></div>
                <?php echo $this->setting['mentoring_content'] ?>

                <?php 
                  $criteria = new CDbCriteria;
                  $criteria->with = array('description');

                  $criteria->addCondition('description.language_id = :language_id');
                  $criteria->params[':language_id'] = $this->languageID;
                  $criteria->addCondition('topic = :topic');
                  $criteria->params[':topic'] = 1;

                  $faq = new CActiveDataProvider('Faq', array(
                    'criteria'=>$criteria,
                      'pagination'=>array(
                          'pageSize'=>10,
                      ),
                  ));
                ?>
                <div class="clear height-40"></div>

                 <div class="panel-group" id="accordion">
                    <?php foreach ($faq->getData() as $key => $value): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel_<?php echo $key ?>"><?php echo $value->description->question ?></a>
                            </h4>
                        </div>
                        <div id="panel_<?php echo $key ?>" class="panel-collapse collapse <?php if ($key == 0): ?>in<?php endif ?>">
                            <div class="panel-body">
                                <?php echo $value->description->answer ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>

                </div>
                <div class="clear height-20"></div>
                <?php $this->widget('CLinkPager', array(
                    'pages' => $faq->getPagination(),
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                )) ?>

                <!-- end accordion -->
                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">GETTING STARTED</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <?php echo $this->renderPartial('_menu', array('active'=>'mentoring')) ?>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>
<script type="text/javascript">
  $(function(){
      var iconOpen = 'icon_minus',
        iconClose = 'icon_plus';
        // icons 

    var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    $active.find('a').prepend('<i class="icons icon_minus"></i>');
    $('#accordion .panel-heading').not($active).find('a').prepend('<i class="icons icon_plus"></i>');
    $('#accordion').on('show.bs.collapse', function (e) {
      $('#accordion .panel-heading.active').removeClass('active').find('.icons').toggleClass('icon_plus icon_minus');
      $(e.target).prev().addClass('active').find('.icons').toggleClass('icon_plus icon_minus');
    });
    // end function accordion

  });
</script>
<style type="text/css">
  .panel-default .panel-body p{
    margin-bottom: 0 !important;
  }
</style>