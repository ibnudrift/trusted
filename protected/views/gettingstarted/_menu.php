<ul class="list-unstyled">
    <li <?php if ($active == 'journey'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/journey', 'step'=>1)); ?>">Patient journey</a></li>
    <li <?php if ($active == 'mentoring'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/gettingstarted/mentoring')); ?>"><?php echo $this->setting['mentoring_title'] ?></a></li>
    <li <?php if ($active == 'faq'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/education/faq')); ?>">FAQ</a></li>
    <li <?php if ($active == 'help'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/gettingstarted/help')); ?>"><?php echo $this->setting['help_title'] ?></a></li>
</ul>
