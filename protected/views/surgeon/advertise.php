<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['advertise_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h2>SURGEONS</h2>
      <h3><?php echo $this->setting['advertise_title'] ?></h3>
    </div>
  </div>
  <div class="clear"></div>
</div>
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="tagline"><?php echo $this->setting['advertise_title'] ?></h1>

        <div class="clear"></div>
      </div>
      <div class="row">
          <div class="col-md-12 text-left">
            <div class="left_cont" style="border-right: 0px;">
              <!-- <div class="mw906"> -->
                <?php /*
                <h4><?php echo nl2br($this->setting['advertise_subtitle']) ?></h4>
                <div class="clear"></div>
                <?php echo $this->setting['advertise_content']; */ ?>
                <div class="clear height-50"></div>
                <?php echo $this->renderPartial('//surgeon/advertisements_d', array()); ?>
                <!-- end data -->

                <!-- <div class="clear"></div>
              </div> -->
            </div>

          </div>
          <?php /*
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">FOR PROFESSIONAL</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
<?php echo $this->renderPartial('_menu', array('active'=>'advertise')) ?>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>*/
          ?>
        </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>