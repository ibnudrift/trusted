<ul class="list-unstyled">
	<li <?php if ($active == 'privacy'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/privacy/')); ?>"><?php echo $this->setting['privacy_title'] ?></a></li>
	<li <?php if ($active == 'tos'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/tos/')); ?>"><?php echo $this->setting['tos_title'] ?></a></li>
</ul>
