<ul class="list-unstyled">
	<li <?php if ($active == 'whyClinic'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/whyClinic/')); ?>"><?php echo $this->setting['why_clinic_title'] ?></a></li>
	<li <?php if ($active == 'surgeons_blog'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/industryblog/index/')); ?>"><?php echo $this->setting['blog_prof_title'] ?></a></li>
	<li <?php if ($active == 'clinic_faq'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/clinic_faq/')); ?>"><?php echo $this->setting['clinic_faq_title'] ?></a></li>
	<li <?php if ($active == 'advertise'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/advertise/')); ?>"><?php echo $this->setting['advertise_title'] ?></a></li>
</ul>
