<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['advertise_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h1>SURGEONS</h1>
      <h3>Download Media Kit</h3>
    </div>
  </div>
  <div class="clear"></div>
</div>
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h3 class="tagline">DOWNLOAD <b class="blue">NOW</b></h3>
        
        <div class="maw615 tengah text-left flns_cont_enQuire">
          <p>Please fill out the form below, to download the media guide.</p>
          
          <div class="clear height-10"></div>
          <div class="boxs_form">
            <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                // 'type'=>'',
                'enableAjaxValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>false,
                ),
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                ),
            )); ?>
              <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
              <?php if(Yii::app()->user->hasFlash('success')): ?>
                  <?php $this->widget('bootstrap.widgets.TbAlert', array(
                      'alerts'=>array('success'),
                  )); ?>
              <?php endif; ?>
              <div class="row default">
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <?php echo $form->textField($model, 'name', array('class'=>'form-control', 'placeholder'=>'Name')); ?>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=>'Email')); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div id="recaptcha2"></div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="fright">
                    <button class="btn btn-link btns_submitFrm">SEND</button>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            <?php $this->endWidget(); ?>

            <div class="clear"></div>
          </div>
          <!-- End form -->

          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>