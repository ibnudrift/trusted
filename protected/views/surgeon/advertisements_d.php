<div class="packages-container">
	<div class="wrapper">
		
<div class="entry-content package-list">
			<div class="package silver">
				<div class="top">

				</div>
				<div class="package-box">
					<div class="heading">
						<p>Silver Free</p>
					</div>
					<div class="tagline">

					</div>
					<ul class="list">
					<?php for ($i=1; $i < 17; $i++) { ?>
						<li <?php if ($i > 5): ?>class="unavailable"<?php endif ?>><?php echo $this->setting['advertise_silver_line_'.$i] ?></li>
					<?php } ?>
						<!-- <li>Professional registrations and or memberships ie. ASAPS, ASPS, FRACS</li>
						<li>Location - suburb and state only</li>
						<li>Up to 3 procedures listed</li>
						<li>1 phone number</li>
						<li >Website URL</li>
						<li class="unavailable">Clinic and surgeon bio</li>
						<li class="unavailable">Book a consult form</li>
						<li class="unavailable">Social media links</li>
						<li class="unavailable">Contribution to blogs – as available</li>
						<li class="unavailable">Photos</li>
						<li class="unavailable">Videos</li>
						<li class="unavailable">Featured promotional spots on homepage and social media as available</li>
						<li class="unavailable">Static Promotional Image at top of profile</li>
						<li class="unavailable">Qualifications</li>
						<li class="unavailable">Priority placement in all areas</li> -->
					</ul>
				</div>
				<div class="enquire-btn">
					<a href="<?php echo CHtml::normalizeUrl(array('/surgeon/enquire', 'package'=>'silver')); ?>">Enquire Now</a>
				</div>
			</div>
			<div class="package gold">
				<div class="top popular">
					<div class="prelatife">
						<p></p>
						<span>Most popular</span>
					</div>
				</div>
				<div class="package-box">
					<div class="heading">
						<p>Gold $<?php echo $this->setting['advertise_gold_price'] ?><span>/month</span></p>
					</div>
					<div class="tagline 3d">
						Join now to receive your first month free
					</div>
					<ul class="list">
					<?php for ($i=1; $i < 17; $i++) { ?>
						<li <?php if ($i > 12): ?>class="unavailable"<?php endif ?>><?php echo $this->setting['advertise_gold_line_'.$i] ?></li>
					<?php } ?>
						<!-- <li>Surgeon name and surgeon image</li>
						<li>Professional registrations and or memberships ie. ASAPS, ASPS, FRACS</li>
						<li>Locations and maps - whole address</li>
						<li>Up to 6 procedures listed</li>
						<li>Email and phone</li>
						<li>Website URL</li>
						<li>Clinic and surgeon bio</li>
						<li>Book a consult form</li>
						<li>Social media links</li>
						<li>Contribution to blogs – as available</li>
						<li>Before and after photos - up to 10</li>
						<li>Videos - up to 3</li>
						<li class="unavailable">Featured promotional spots on homepage and social media as available</li>
						<li class="unavailable">Banner at the top of the page</li>
						<li class="unavailable">Qualifications</li>
						<li class="unavailable">Priority placement in all areas</li> -->
					</ul>
				</div>
				<div class="enquire-btn">
					<a href="<?php echo CHtml::normalizeUrl(array('/surgeon/enquire', 'package'=>'gold')); ?>">Join Now</a>
				</div>
				<!--<p class="note">*6 months free offer expires December 31st, 2016</p>-->
			</div>
			<div class="package platinum">
				<div class="top">

				</div>
				<div class="package-box">
					<div class="heading">
						<p>Platinum $<?php echo $this->setting['advertise_platinum_price'] ?><span>/month</span></p>
					</div>
					<div class="tagline">

					</div>
					<ul class="list">
					<?php for ($i=1; $i < 17; $i++) { ?>
						<li <?php if ($i > 17): ?>class="unavailable"<?php endif ?>><?php echo $this->setting['advertise_platinum_line_'.$i] ?></li>
					<?php } ?>
						<!-- <li>Surgeon name and surgeon Image</li>
						<li>Professional registrations and or memberships ie. ASAPS, ASPS, FRACS</li>
						<li>Locations and maps - whole address</li>
						<li>Unlimited procedures listed</li>
						<li>Email and phone</li>
						<li>Website URL</li>
						<li>Clinic and surgeon bio</li>
						<li>Book a consult form</li>
						<li>Social media links</li>
						<li>Contribution to blogs – as available</li>
						<li>Before and after photos - unlimited</li>
						<li>Videos - up to 10</li>
						<li>Featured promotional spots on homepage and social media as available</li>
						<li>Static Promotional Image at top of profile</li>
						<li>Qualifications</li>
						<li>Priority placement in all areas</li> -->
					</ul>
				</div>
				<div class="enquire-btn">
					<a href="<?php echo CHtml::normalizeUrl(array('/surgeon/enquire', 'package'=>'platinum')); ?>">Join Now</a>
				</div>
			</div>
		<div class="clear"></div>
		</div>


	</div>
</div>