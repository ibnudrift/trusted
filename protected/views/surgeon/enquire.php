<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['advertise_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h2>SURGEONS</h2>
      <?php if ($_GET['package'] == 'gold'): ?>
      <h3>Gold Membership Form</h3>
      <?php elseif ($_GET['package'] == 'platinum'): ?>
      <h3>Platinum Membership Form</h3>
      <?php else: ?>
      <h3>Join Now</h3>
      <?php endif ?>
    </div>
  </div>
  <div class="clear"></div>
</div>
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <?php if ($_GET['package'] == 'gold'): ?>
          <h1 class="tagline"><b class="blue">GOLD</b> MEMBERSHIP FORM</h1>
        <?php elseif ($_GET['package'] == 'platinum'): ?>
          <h1 class="tagline"><b class="blue">PLATINUM</b> MEMBERSHIP FORM</h1>
        <?php else: ?>
          <h1 class="tagline">JOIN <b class="blue">NOW</b></h1>
        <?php endif ?>
        
        <div class="maw615 tengah text-left flns_cont_enQuire">
            <p>Simply fill out the below enquiry form and a member of our team will be in touch with you very soon.</p>
          
          <div class="clear height-10"></div>
          <div class="boxs_form">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    // 'type'=>'',
    'enableAjaxValidation'=>false,
    'clientOptions'=>array(
        'validateOnSubmit'=>false,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>
              <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
              <?php if(Yii::app()->user->hasFlash('success')): ?>
                  <?php $this->widget('bootstrap.widgets.TbAlert', array(
                      'alerts'=>array('success'),
                  )); ?>
              <?php endif; ?>
              <div class="row default">
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <?php echo $form->textField($model, 'name', array('class'=>'form-control', 'placeholder'=>'Name')); ?>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'placeholder'=>'Phone')); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <?php echo $form->dropDownList($model, 'package', array(
                      'Platinum'=>'Platinum',
                      'Gold'=>'Gold',
                      'Silver'=>'Silver',
                    ),array('class'=>'form-control', 'empty'=>'Type Of Package')); ?>
                  </div>
                </div>
              </div>
              <div class="row default">
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=>'Email')); ?>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <?php echo $form->textField($model, 'address', array('class'=>'form-control', 'placeholder'=>'Practice Address')); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <?php echo $form->textField($model, 'website', array('class'=>'form-control', 'placeholder'=>'Website URL')); ?>
                  </div>
                </div>
              </div>
              <?php /*
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'placeholder'=>'Message', 'rows'=>4)); ?>
                  </div>
                </div>
              </div>*/ ?>
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div id="recaptcha2"></div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="fright">
                    <button class="btn btn-link btns_submitFrm">SIGN UP</button>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
<?php $this->endWidget(); ?>
            <div class="clear"></div>
          </div>
          <!-- End form -->

          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>