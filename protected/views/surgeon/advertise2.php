<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['advertise_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>
<div class="outers_pg_banner pages_advertise2">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1900,500, '/images/static/'.$this->setting['download_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="blocks_txt_infcs">
      <div class="prelatife container">
                <h3><?php echo $this->setting['download_banner_title'] ?></h3>
                <div class="clear"></div>
                <p><?php echo nl2br($this->setting['download_banner_content']) ?></p>
                <div class="clear height-30"></div>
                <a href="<?php echo CHtml::normalizeUrl(array('/surgeon/advertiseenquire')); ?>" class="btn btn-link btns_vwf_slide">DOWNLOAD MEDIA KIT</a>
              <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>

<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static advertise_us2">
    <div class="block_default block_1 back-white">
      <div class="prelatife container content-text mw1170">
        <div class="clear height-50"></div>
        <h1 class="text-center"><?php echo $this->setting['download_why_title'] ?></h1>
        <div class="clear height-20"></div>
        <div class="row default outs_blocks_info">
          <div class="col-md-6 col-sm-6">
            <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(562,190, '/images/static/'.$this->setting['download_why_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="info">
              <p>
                <b><?php echo nl2br($this->setting['download_why_subtitle']) ?></b>
              </p>
              <?php echo $this->setting['download_why_content'] ?>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="clear height-35"></div>
      </div>
      <div class="clear"></div>
    </div>
    
    <div class="block_default block_2">
      <div class="prelatife container content-text mw1170">
        <div class="clear height-50"></div>
        <h5 class="text-center"><?php echo $this->setting['download_our_title'] ?></h5>
        <div class="clear height-20"></div>
        <div class="row default outs_blocks_info text-center">
          <div class="col-md-12">
            <?php echo $this->setting['download_our_content'] ?>
            <div class="clear"></div>
          </div>
        </div>

        <div class="clear height-35"></div>
      </div>
    </div>

    <div class="block_default block_3 back-white">
      <div class="prelatife container content-text mw1170">
        <div class="clear height-50"></div>
        <h5 class="text-center"><?php echo $this->setting['download_user_title'] ?></h5>
        <div class="clear height-20"></div>
        <div class="row default outs_blocks_info mw985">
          <?php for ($i=1; $i < 3; $i++) { ?>
          <div class="col-md-6 col-sm-6">
            <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(469 ,217 , '/images/static/'.$this->setting['download_users_image_'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
            <div class="clear height-15"></div>
            <div class="info">
              <h6><?php echo $this->setting['download_users_title_'.$i] ?></h6>
              <div class="clear"></div>
              <p><?php echo $this->setting['download_users_content_'.$i] ?></p>
            </div>
          </div>
          <?php } ?>
        </div>
        <div class="clear height-45"></div>
      </div>
      <div class="clear"></div>
    </div>

    <div class="block_default block_4">
      <div class="prelatife container content-text mw1170">
        <div class="clear height-50"></div>
        <div class="row default outs_blocks_info mw985">
          <div class="col-md-6 col-sm-6">
            <div class="pict"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(469 ,250  , '/images/static/'.$this->setting['download_extras_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="info">
              <h5><?php echo $this->setting['download_extras_title'] ?></h5>
              <div class="clear height-0"></div>
              <?php echo $this->setting['download_extras_content'] ?>
            </div>
          </div>
        </div>
        <div class="clear height-10"></div>
        <div class="clear height-50"></div>
      </div>
      <div class="clear"></div>
    </div>

    <div class="block_default block_5 back-white">
      <div class="prelatife container mw1170">
        <div class="clear height-50"></div>
        <h5 class="text-center"><?php echo $this->setting['download_stats_title'] ?></h5>
        <div class="info text-center">
          <?php echo $this->setting['download_stats_content'] ?>
        </div>
        <div class="clear height-5"></div>
        <div class="mw985">
          <div class="lines-grey"></div>
        </div>
        <div class="clear height-30"></div>

        <div class="outers_list_circles_data">
          <div class="row default">
            <?php for ($i=1; $i < 6; $i++) { ?>
            <div class="col-md-15 col-sm-4">
              <div class="items">
                <div class="circles"><span><?php echo $this->setting['download_stats_title_'.$i] ?></span></div>
                <div class="info">
                  <p><?php echo $this->setting['download_stats_content_'.$i] ?></p>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
            <?php } ?>
            
          </div>
        </div>

        <div class="clear height-45"></div>
      </div>
      <div class="clear"></div>
    </div>

    <div class="block_default block_2">
      <div class="prelatife container content-text mw1170">
        <div class="clear height-50"></div>
        <h5 class="text-center"><?php echo $this->setting['download_our_title'] ?></h5>
        <div class="clear height-20"></div>
        <div class="row default outs_blocks_info text-center">
          <div class="col-md-12">
            <?php echo $this->setting['download_our_content'] ?>
            <div class="clear"></div>
          </div>
        </div>

        <div class="clear height-35"></div>
      </div>
    </div>

    <div class="clear"></div>
  </div>
</section>