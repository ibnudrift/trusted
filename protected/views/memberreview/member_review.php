<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <div class="lefts_cont_member">
              <ul class="list-unstyled">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberE')); ?>">Manage Account</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberClinic')); ?>">Manage Clinic</a></li>
                <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/design/memberReview')); ?>">Manage Reviews</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberBlog')); ?>">Manage Messages</a></li>
              </ul>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              
              <!-- start review cont -->
              <div class="list_box_default_member_white">
                <div class="items prelatife">
                  <div class="padding">
                    <h4 class="titles_s">Make Me a Yummy Mummy! - Castle Hill, AU</h4>
                    <div class="clear"></div>
                    <span class="dates">4eva30 - 1 day ago</span>
                    <div class="clear height-10"></div>
                    <div class="clear height-10"></div>
                    <span>&nbsp;</span>
                    <div class="clear height-50"></div>
                    <div class="clear height-30"></div>
                  </div>
                   <div class="blocks_bottom_d">
                      <div class="padding h143 pt0">
                        <div class="row default prelatife h143">
                          <div class="col-md-10">
                            <p>Dr Lim is honest and professional. I would highly reommend him to anyone considering surgery.</p>
                          </div>
                          <div class="col-md-2 border-left h143">
                            <div class="buttons_d">
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-times"></i>&nbsp;&nbsp;Delete</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-quote-right"></i>&nbsp;&nbsp;Feature this review</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-comments"></i>&nbsp;&nbsp;Reply</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-check"></i>&nbsp;&nbsp;Approve</a>
                            </div>
                            <div class="clear"></div>
                          </div>
                        </div>

                        <div class="clear"></div>
                      </div>
                    </div>
                </div>

                <div class="items prelatife">
                  <div class="padding">
                    <h4 class="titles_s">Waiting for my Tummy Tuck - Parramatta, AU</h4>
                    <div class="clear"></div>
                    <span class="dates">4eva30 - 1 day ago</span>
                    <div class="clear height-10"></div>
                    <div class="clear height-10"></div>
                    <span>&nbsp;</span>
                    <div class="clear height-50"></div>
                    <div class="clear height-30"></div>
                  </div>
                   <div class="blocks_bottom_d">
                      <div class="padding h143 pt0">
                        <div class="row default prelatife h143">
                          <div class="col-md-10">
                            <p>This Doctor is AWESOME!!!</p>
                          </div>
                          <div class="col-md-2 border-left h143">
                            <div class="buttons_d">
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-times"></i>&nbsp;&nbsp;Delete</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-quote-right"></i>&nbsp;&nbsp;Feature this review</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-comments"></i>&nbsp;&nbsp;Reply</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-check"></i>&nbsp;&nbsp;Approve</a>
                            </div>
                            <div class="clear"></div>
                          </div>
                        </div>

                        <div class="clear"></div>
                      </div>
                    </div>
                </div>

                <div class="items prelatife">
                  <div class="padding">
                    <span class="b_star">
                      <i class="fa fa-star"></i> 
                      <i class="fa fa-star"></i> 
                      <i class="fa fa-star"></i>&nbsp;Body Lift
                    </span>
                    <div class="clear"></div>
                    <h4 class="titles_s">Body Lift - Castle Hill, AU</h4>
                    <div class="clear"></div>
                    <span class="dates">mkw000 - 1 day ago</span>
                    <div class="clear height-10"></div>
                    <span>&nbsp;</span>
                    <div class="clear height-50"></div>
                    <div class="clear height-30"></div>
                  </div>
                   <div class="blocks_bottom_d">
                      <div class="padding h143 pt0">
                        <div class="row default prelatife h143">
                          <div class="col-md-10">
                            <p>After losing 50kg of weight I hade a lot of loose skin. Since I was so big to start with I felt the
loose skin was still fat I had to lose. I had read all the scary internet stories and was not
convinced to go in for surgery. Than the questions do I do it in Australia or go overseas? After
being recommended by a school friend to see Dr. Lim I went to see if I could have a tummy tuck.
I had seen other surgeons.</p>
                          </div>
                          <div class="col-md-2 border-left h143">
                            <div class="buttons_d">
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-times"></i>&nbsp;&nbsp;Delete</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-quote-right"></i>&nbsp;&nbsp;Feature this review</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-comments"></i>&nbsp;&nbsp;Reply</a>
                              <div class="clear height-3"></div>
                              <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-check"></i>&nbsp;&nbsp;Approve</a>
                            </div>
                            <div class="clear"></div>
                          </div>
                        </div>

                        <div class="clear"></div>
                      </div>
                    </div>
                </div>

                <div class="clear"></div>
              </div>
              <!-- end review cont -->

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>