<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['about_who_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h2>ALCL</h2>
      <h3><?php echo $this->setting['about_who_title'] ?></h3>
    </div>
  </div>
  <div class="clear"></div>
</div>
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      <!-- <div class="pict_full illustration_picture"><img src="<?php echo $this->assetBaseurl ?>ill_about_11.jpg" alt="" class="img-responsive"></div>
      <div class="clear height-50"></div><div class="height-5"></div> -->

      <div class="content-text insides_static">
        <h1 class="tagline"><?php echo $this->setting['about_who_title'] ?></h1>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">
                <h4><?php echo nl2br($this->setting['about_who_subtitle']) ?></h4>
                <div class="pict_full"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(909,1000, '/images/static/'.$this->setting['about_who_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                <div class="clear"></div>
                <?php echo $this->setting['about_who_content'] ?>

                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">ALCL</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                  <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('index')); ?>"><?php echo $this->setting['about_who_title'] ?></a></li>
                  <!-- <li><a href="<?php //echo CHtml::normalizeUrl(array('mission')); ?>"><?php // echo $this->setting['about_mission_title'] ?></a></li> -->
                  <li><a href="<?php echo CHtml::normalizeUrl(array('why')); ?>"><?php echo $this->setting['about_why_title'] ?></a></li>
                  <li><a href="<?php echo CHtml::normalizeUrl(array('me')); ?>"><?php echo $this->setting['about_me_title'] ?></a></li>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>