<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static contact_page">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      <div class="pict_full illustration_picture"><img src="<?php echo $this->assetBaseurl ?>ill-contact-1.jpg" alt="" class="img-responsive"></div>
      <div class="clear height-50"></div>

      <div class="content-text insides_static inside_contact_page">
        <h1 class="title_page">CONTACT US</h1>
        <div class="clear height-45"></div>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-4">
            <div class="left_content">
              <p><span>Customer Service Manager</span>
                For Patients &nbsp;&nbsp; &nbsp;+61 0406048100x<br />
                For Surgeons &nbsp;&nbsp;+61 04064048100</p>

                <p><span>EMAIL</span>
                For Patients &nbsp;&nbsp;&nbsp; &nbsp;support@trustedsurgeons.com.au<br />
                For Surgeons &nbsp;&nbsp;&nbsp;support@trustedsurgeons.com.au</p>

                <p><span>ADDRESS</span>
                Suite 1602, Level 16, Tower 1<br />
                Westfield Bondi Junction<br />
                520 Oxford Street<br />
                Bondi Junction NSW 2022</p>

                <p><span>Opening Hour</span>
                <b>Monday - Friday</b> &nbsp;&nbsp; &nbsp;9am - 5pm<br />
                <b>Saturday</b> Existing clients only<br />
                <b>Sundays and Public Holidays</b> Closed<br />
                <b>Strictly by appointment only</b></p>

              <div class="clear"></div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="rights_content padding-left-30">
              
              <div class="sBox_contact_form">
              <div>
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">FOR CUSTOMER</a></li>
                  <li role="presentation"><a href="#surgeons" aria-controls="surgeons" role="tab" data-toggle="tab">FOR SURGEONS</a></li>
                </ul>

                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="customer">
                      <p>Please use the form below to enquire about any of our services. Be sure to indicate what type of procedures you're interested in and a member of our team will get back to you shortly.</p>
                      <div class="contact_form">
                        <form action="#" method="post">
                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput1">First Name</label>
                                <input type="text" class="form-control" id="exampleInput1">
                              </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput2">Last Name</label>
                                <input type="text" class="form-control" id="exampleInput2">
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput3">Phone Number</label>
                                <input type="text" class="form-control" id="exampleInput3">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                              <label for="exampleInput4">Email Address</label>
                              <input type="email" class="form-control" id="exampleInput4">
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                              <label for="exampleInput5">Procedure I am interested in</label>
                              <select name="" class="form-control" id="">
                                <option value="">Please Select</option>
                                <option value="">Select 1</option>
                                <option value="">Select 2</option>
                                <option value="">Select 3</option>
                              </select>
                              </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                              <label for="exampleInput6">Booking a coaching service</label>
                               <div class="checkbox">
                                <label>
                                  <input type="checkbox"> Yes
                                </label>
                              </div>
                              </div>
                            </div>
                          </div>
                          <div class="row default">
                            <div class="col-sm-12 col-md-12">
                              <div class="form-group">
                              <label for="exampleInput6">Any question for Trusted Surgeons?</label>
                              <textarea name="" id="exampleInput6" rows="5" class="form-control"></textarea>
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <!-- <div class="g-recaptcha" data-sitekey="6LehEyYTAAAAAHB_k-y4DdfPrdJMwJ9bU11A4_0Q"></div> -->
                              <div id="recaptcha1"></div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group fright">
                                  <button type="submit" class="btn btn-default submits_form"></button>
                              </div>
                            </div>
                          </div>
                          
                        </form>
                        <div class="clear"></div>
                      </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="surgeons">
                      <p>Please use the form below to enquire about any of our services. Be sure to indicate what type of procedures you're interested in and a member of our team will get back to you shortly.</p>
                      <div class="contact_form">
                        <form action="#" method="post">
                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput1">First Name</label>
                                <input type="text" class="form-control" id="exampleInput1">
                              </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput2">Last Name</label>
                                <input type="text" class="form-control" id="exampleInput2">
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput3">Phone Number</label>
                                <input type="text" class="form-control" id="exampleInput3">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                              <label for="exampleInput4">Email Address</label>
                              <input type="email" class="form-control" id="exampleInput4">
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-sm-12 col-md-12">
                              <div class="form-group">
                              <label for="exampleInput6">Any question for Trusted Surgeons?</label>
                              <textarea name="" id="exampleInput6" rows="5" class="form-control"></textarea>
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <!-- <div class="g-recaptcha" data-sitekey="6LehEyYTAAAAAHB_k-y4DdfPrdJMwJ9bU11A4_0Q"></div> -->
                              <div id="recaptcha2"></div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group fright">
                                  <button type="submit" class="btn btn-default submits_form"></button>
                              </div>
                            </div>
                          </div>
                          
                        </form>
                        <div class="clear"></div>
                      </div>
                  </div>

                </div>

              </div>

              <div class="clear"></div>
            </div>
            <!-- End detail surgeons -->

              <div class="clear"></div>
            </div>
          </div>
        </div>
        <!-- end inside page contact -->

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<script src='https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit'></script>
<script>
      var recaptcha1;
      var recaptcha2;
      var myCallBack = function() {
        //Render the recaptcha1 on the element with ID "recaptcha1"
        recaptcha1 = grecaptcha.render('recaptcha1', {
          'sitekey' : '6LehEyYTAAAAAHB_k-y4DdfPrdJMwJ9bU11A4_0Q', //Replace this with your Site key
          'theme' : 'light'
        });
        
        //Render the recaptcha2 on the element with ID "recaptcha2"
        recaptcha2 = grecaptcha.render('recaptcha2', {
          'sitekey' : '6LehEyYTAAAAAHB_k-y4DdfPrdJMwJ9bU11A4_0Q', //Replace this with your Site key
          'theme' : 'light'
        });
      };
    </script>