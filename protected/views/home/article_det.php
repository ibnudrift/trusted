<section class="outers_inside_page p_gallery">
	<div class="insides_page">
		<div class="back-white backs_cloud_insidep content-text text-center">
			<div class="prelatife container">
				<div class="clear h60"></div>
				<h1 class="title-pages blacks">Titles Articles</h1>
				<div class="clear height-15"></div>
				<div class="back_tarticles"><a href="<?php echo CHtml::normalizeUrl(array('/home/articles')); ?>"><i class="fa fa-caret-left"></i> Back</a></div>
				<div class="clear height-50 hidden-xs"></div>
				<div class="clear height-25 visible-xs"></div>
				<div class="height-20"></div>
				<div class="details_news">
					<div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>test_big_article.jpg" alt="" class="img-responsive"></div>
					<div class="clear height-50 hidden-xs"></div> <div class="clear height-25 visible-xs"></div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam deleniti enim, aspernatur repellat hic nemo dicta illo sint. Consequatur ex voluptatem blanditiis minus ipsa nihil iure eveniet laboriosam veritatis ad.
						Corporis impedit tenetur hic, natus omnis vel ut! Perspiciatis eum, tenetur id unde aspernatur porro deserunt obcaecati corporis nisi atque numquam deleniti perferendis molestias distinctio, expedita quas tempora sint consequatur.
						Nostrum, fugiat neque exercitationem voluptate voluptas hic ratione, commodi, harum veniam blanditiis id adipisci ea. Nihil soluta qui sit quaerat sunt eaque, minus fugiat consectetur. Assumenda nesciunt delectus officia reprehenderit.
						Impedit atque eaque, et, quia ipsa, deserunt laudantium sit eius inventore delectus libero saepe. Vitae nisi aut neque ipsa, architecto, culpa eum odio eos voluptatibus hic minus dolores soluta reiciendis!
						Molestias dolorem dolorum iste necessitatibus mollitia molestiae accusantium eligendi officiis ipsam minima ullam velit, provident, eaque numquam. Quaerat soluta quas praesentium, ullam reiciendis nostrum consequuntur ratione, saepe ipsum quo illo.
					</p>
					<div class="clear height-30"></div>
					<div class="shares-text text-center p_shares_article">
						<span class="inline-t">SHARE</span>&nbsp; / &nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=#">FACEBOOK</a>&nbsp; /
						&nbsp;<a target="_blank" href="https://plus.google.com/share?url=#">GOOGLE PLUS</a>&nbsp; /
						&nbsp;<a target="_blank" href="https://twitter.com/home?status=#">TWITTER</a>
						</div>
					
					<div class="clear"></div>
				</div>
				<div class="clear height-20"></div>
				<div class="lines-grey"></div>
				<div class="clear height-50"></div>
				<div class="clear height-20"></div>

				<div class="listing_article_default">
					<h4 class="others_art">Others Articles</h4>
					<div class="clear height-40"></div>
					<div class="row">

						<div class="col-lg-4 col-md-6">
							<div class="items">
								<div class="pict">
									<img src="<?php echo $this->assetBaseurl ?>artikel-1.jpg" alt="" class="img-responsive">
								</div>
								<div class="desc">
									<div class="d-inline text-left v-top mw290">
										<span class="title d-inline">
											Promosi 20% discount perawatan kulit di Cahaya Medical Centre
										</span>
									</div>
									<div class="d-inline text-right v-top">
										<div class="hidden-xs">
							                <a href="#"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights.png" alt=""></a>
							                </div>
							                <div class="visible-xs">
							                  <a href="#"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights2.png" alt=""></a>
							                </div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="items">
								<div class="pict">
									<img src="<?php echo $this->assetBaseurl ?>artikel-2.jpg" alt="" class="img-responsive">
								</div>
								<div class="desc">
									<div class="d-inline text-left v-top mw290">
										<span class="title d-inline">
											Mengapa penting untuk tidur lebih pagi
										</span>
									</div>
									<div class="d-inline text-right v-top">
										<div class="hidden-xs">
							                <a href="#"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights.png" alt=""></a>
							                </div>
							                <div class="visible-xs">
							                  <a href="#"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights2.png" alt=""></a>
							                </div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="items">
								<div class="pict">
									<img src="<?php echo $this->assetBaseurl ?>artikel-3.jpg" alt="" class="img-responsive">
								</div>
								<div class="desc">
									<div class="d-inline text-left v-top mw290">
										<span class="title d-inline">
											Mengapa hati yang gembira dapat menyembuhkan penyakit
										</span>
									</div>
									<div class="d-inline text-right v-top">
										<div class="hidden-xs">
							                <a href="#"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights.png" alt=""></a>
							                </div>
							                <div class="visible-xs">
							                  <a href="#"><img src="<?php echo $this->assetBaseurl ?>btns-grey-chevron-rights2.png" alt=""></a>
							                </div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>

					</div>
				</div>

				<div class="clear height-50"></div><div class="height-40"></div>

				<div class="clear"></div>
			</div>
		</div>

		<div class="clear"></div>
	</div>
</section>
<div class="clear"></div>