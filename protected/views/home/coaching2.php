<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static coaching">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="height-5"></div>

      <div class="content-text insides_static text-center coaching2_form">
        <div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>pict-big_coaching_form.jpg" alt="" class="img-responsive"></div>
        <div class="clear height-50"></div><div class="height-5"></div>

        <div class="tengah">
          <h1 class="title_page">COACHING</h1>
          <div class="clear"></div>
          <span>
            Accompanying patient to surgical or aesthetic treatment consultation <br>
            <b>$150.00 per hour</b>
          </span>
          <div class="clear height-40"></div>
          
          <div class="box_form_coaching_c">
            <form method="post" action="#">
              <p class="sub_title">Patient Information</p>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputFirstname">First Name</label>
                      <input type="text" class="form-control" id="exampleInputFirstname">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputlastname">Last Name</label>
                      <input type="text" class="form-control" id="exampleInputlastname">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputemail">Email Address</label>
                      <input type="email" class="form-control" id="exampleInputemail">
                    </div>
                </div>
              </div>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputPhonenumber">Phone Number</label>
                      <input type="text" class="form-control" id="exampleInputPhonenumber">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputmobilenumber">Mobile Number</label>
                      <input type="text" class="form-control" id="exampleInputmobilenumber">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputskype">SKype Number</label>
                      <input type="email" class="form-control" id="exampleInputskype">
                    </div>
                </div>
              </div>

              <div class="clear height-20"></div>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>Consultation Date & Time</b></label>
                      <div class="prelatife">
                      <input type="text" class="form-control datepicker" id="exampleInputconsdate">
                      </div>
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputmobilenumber">&nbsp;</label>
                      <input type="text" class="form-control" id="exampleInputmobilenumber" placeholder="Available Time">
                    </div>
                </div>
                <div class="col-md-4">
                </div>
              </div>

              <div class="clear height-10"></div>
              <p class="sub_title mb5">Procedures</p>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputconsdate">Requested Procedures</label>
                      <div class="prelatife">
                        <select name="#" id="" class="form-control selectpicker">
                          <option value="0">Please select</option>
                          <option value="0">Please select</option>
                          <option value="0">Please select</option>
                          <option value="0">Please select</option>
                        </select>
                      </div>
                    </div>
                </div>
                <div class="col-md-4">
                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate">Why you want the surgery, your expectations and desired outcome</label>
                      <div class="clear"></div>
                      <textarea name="" class="form-control" id="" rows="5"></textarea>
                    </div>
                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate">Please specify any other requirements</label>
                      <div class="clear"></div>
                      <textarea name="" class="form-control" id="" rows="5"></textarea>
                    </div>
                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>FAMILY MEDICAL CONDITIONS</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Heart Disease
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Diabetes
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Hypertension
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Asthma
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Cancer
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->

                      </div>
                    </div>

                </div>
              </div>
              
              <?php 
              $jpg_im = array(
                'Heart Disease',
                'Diabetes',
                'Hypertension',
                'Deep Vein Thrombosis',
                'Cardiovascular Accidents',
                'Asthma',
                'Bleeding Tendency',
                'Hyperthyriodism',
                'Adrenal Insufficiency',
                'Hepatitis',
                'HIV',
                'Keloid Scarrin',
                'Cancer',
                'Major Operation',
                'Underlying Disease',
                'Drug Allergies',
                'Food Allergies',
                'Current Medication & Dosage',
                'Current Vitamins, Food, Nutritional Support',
                );
              ?>
              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>PATIENT MEDICAL CONDITIONS</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <?php foreach ($jpg_im as $key => $value): ?>
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              <?php echo $value; ?>
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <?php endforeach ?>
                        
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>HABBITS</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Smoking
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Drinking
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>PAYMENT METHOD</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <div class="col-xs-15">
                          <div class="radio">
                            <label>
                              <input type="radio" value="">
                              <img src="<?php echo $this->assetBaseurl ?>logo-visa-mastercard-payment.png" alt="" class="img-responsive">
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="radio">
                            <label>
                              <input type="radio" value="">
                              <img src="<?php echo $this->assetBaseurl ?>logo-paypal-payment.png" alt="" class="img-responsive">
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">Credit Card Number</label>
                      <div class="clear"></div>
                      <input type="text" class="form-control" placeholder="Credit Card Number">
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">Expiration date (MM/YY)</label>
                      <div class="clear"></div>
                      <div class="row">
                        <div class="col-xs-5">
                          <input type="text" class="form-control" placeholder="Month">
                        </div>
                        <div class="col-xs-2 text-center">
                          /
                        </div>
                        <div class="col-xs-5">
                          <input type="text" class="form-control" placeholder="Year">
                        </div>
                      </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">Security Code</label>
                      <div class="clear"></div>
                      <div class="row">
                        <div class="col-xs-5">
                          <input type="text" class="form-control" placeholder="Security Code">
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="text-left">
              <button type="submit" class="btn btn-default backs_btn_submit"></button>
              </div>
            </form>

            <div class="clear"></div>
          </div>
          <!-- end form coaching -->
          <div class="clear height-30"></div>


          <div class="clear"></div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <!-- end middle content back pattern -->

    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {
  $( ".datepicker" ).datepicker({
    showOn: "button",
    buttonImage: "<?php echo $this->assetBaseurl ?>icon-datepicker.png",
    buttonImageOnly: true,
    buttonText: "Select date"
  });
  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>