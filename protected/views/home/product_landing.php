<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static page_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="title_page">SHOP</h1>
        <div class="clear height-35"></div>

        <!-- start list product_landing -->
        <div class="box_listing_productslanding mw1000 tengah">
          <?php for ($i=1; $i <= 2; $i++) { ?>
          <div class="items <?php if ($i == 2): ?>last<?php endif ?>">
            <h3 class="titles text-center">Pre Surgery Skincare Treatment</h3>
            <div class="clear"></div>
            <div class="middle_products_desc">
              <div class="row">
                <div class="col-md-9">
                  <div class="big_picture"><img src="<?php echo $this->assetBaseurl ?>pict_landing_products_<?php echo $i ?>.jpg" alt="" class="img-responsive"></div>
                </div>
                <div class="col-md-3">
                  <div class="thumb_picture">
                    <ul class="list-unstyled">
                      <li>
                        <a href="#">
                        <img src="<?php echo $this->assetBaseurl ?>ex_pict_product_1s.jpg" alt="" class="img-responsive">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                        <img src="<?php echo $this->assetBaseurl ?>ex_pict_product_1s.jpg" alt="" class="img-responsive">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                        <img src="<?php echo $this->assetBaseurl ?>ex_pict_product_1s.jpg" alt="" class="img-responsive">
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="clear height-25"></div>
              <div class="row">
                <div class="col-md-9 text-left">
                  <p>Before you have surgery making sure your body is in the best condition possible will really help improve the outcome. The fist thing I always recommend is do our 10 day detox at least a month before surgery to cleanse your body and allow the body to be at its best.</p>
                </div>
                <div class="col-md-3">
                  <div class="fright text-center">
                    <a href="#" class="back_purple_defaults_tl">SHOP NOW</a>
                  </div>
                </div>
              </div>
              <div class="clear"></div>
            </div>
            <!-- end middle product desc -->
          </div>
          <?php } ?>

          <div class="clear"></div>
        </div>
        <!-- // end list product_landing -->

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>