<section class="middle-content back-creamabout min-h500">
    <div class="prelatife container">
        <div class="clear height-50"></div>
        <div class="height-40"></div>
        <div class="content-text insides-container t-upper">
            <div class="text-center">
                <h1 class="title-pages wow fadeInDown">OUR SERVICES</h1>
            </div>
            <div class="clear height-40"></div>
            <div class="ills-about wow fadeInDown"><img src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['service_hero_image'] ?>" alt="" class="img-responsive"></div>
            <div class="clear height-50"></div><div class="height-15"></div>
            <?php echo $this->setting['service_text1'] ?>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</section>
<?php /*
<div class="clear"></div>
<div class="outers-back-bottms-about" style="background-image: url('<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $this->setting['service_background'] ?>')">
    <div class="prelatife container">
        <div class="clear h215"></div>
        <div class="insides-container">
            <?php echo $this->setting['service_text2'] ?>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
*/ ?>

       
        <div class="clear"></div>
    </div>

    <div class="outers-middle-contents">

        <section class="back-cream-middle mh450 padding-bottom-30">
            <div class="prelatife container"> <div class="clear height-50"></div>
              <!-- start gallery -->
              <div class="outers-cont-bgallery">
                  <div class="middle">
                    
                    <div class="list-gallery-data wow fadeInUp">
                      <div class="row">

                        <div class="col-md-4 col-sm-6">
                            <div class="items">
                              <div class="pict">
                                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(458,300, '/images/static/'.$this->setting['service_list_image_1'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                              </div>
                              <div class="clear"></div>
                              <div class="desc">
                                <h5 class="title"><?php echo $this->setting['service_list_title_1'] ?></h5>
                                <div class="clear height-5"></div><div class="height-2"></div>
                                <span class="chld1"><?php echo nl2br($this->setting['service_list_description_1']) ?></span> <div class="clear"></div>
                            </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="items">
                              <div class="pict">
                                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(458,300, '/images/static/'.$this->setting['service_list_image_2'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                              </div>
                              <div class="clear"></div>
                              <div class="desc">
                                <h5 class="title"><?php echo $this->setting['service_list_title_2'] ?></h5>
                                <div class="clear height-5"></div><div class="height-2"></div>
                                <span class="chld1"><?php echo nl2br($this->setting['service_list_description_2']) ?></span> <div class="clear"></div>
                            </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="items">
                              <div class="pict">
                                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(458,300, '/images/static/'.$this->setting['service_list_image_3'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                              </div>
                              <div class="clear"></div>
                              <div class="desc">
                                <h5 class="title"><?php echo $this->setting['service_list_title_3'] ?></h5>
                                <div class="clear height-5"></div><div class="height-2"></div>
                                <span class="chld1"><?php echo nl2br($this->setting['service_list_description_3']) ?></span> <div class="clear"></div>
                            </div>
                            </div>
                        </div>

                      </div>
                    </div>
                    <div class="clear"></div>

                    <div class="clear"></div>
                  </div>


                  <div class="clear"></div>
              </div>
              <!-- end gallery -->

                <div class="clear"></div>
            </div>
        </section>
       
        <div class="clear"></div>
    </div>
<?php /*
<script type="text/javascript">
    var feed = new Instafeed({
        get: 'user',
        userId: 2157781237,
        accessToken: '2291354346.1677ed0.d0e72faf47c54ecdbea660d33164f118',
        target: 'instafeed',
        limit: 8,
        // resolution : 'thumbnail',
        template: '<div class="items"><a href="{{link}}" target="_blank"><img class="img-responsive" src="{{image}}" /></a></div>',
        after: function(){
             // $("#instafeed").owlCarousel();
             // var isidataGal =  $('#instafeed').html();
             // var owl = $("#owl-example").html(isidataGal);
              $('#instafeed').html();
 
              // owl.owlCarousel({
              //     items : 6, 
              //     itemsDesktop : [1000,5], 
              //     itemsDesktopSmall : [900,3], 
              //     itemsTablet: [600,2], 
              //     pagination: false,
              //     itemsMobile : false, 
              // });

               // Custom Navigation Events
              $(".customNavigation a.arrow-right").click(function(){
                  // owl.trigger('owl.next');
              });

              $(".customNavigation a.arrow-left").click(function(){
                  // owl.trigger('owl.prev');
              });

            },
    });
  feed.run();

</script>
*/ ?>