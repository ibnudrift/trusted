<section class="outers_inside_page p_gallery">
  <div class="insides_page">
    <div class="back-white backs_cloud_insidep content-text text-center">
      <div class="prelatife container">
        <div class="clear h60"></div>
        <h1 class="title-pages">Gallery</h1>
        <div class="clear height-50"></div><div class="height-20"></div>
        <div class="mw1035 tengah">
          <span class="fRoboto">Silahkan melihat gallery foto kami untuk mengetahui lebih lanjut infrastruktur Cahaya Diagnostic Center.</span>

          <p>Klik thumbnail di bawah ini untuk memperbesar.</p>
        </div>
        <div class="clear height-50"></div><div class="height-0"></div>

        <div class="listing_services_dtdefault list_d_gallery">
          <div class="row">
            <?php if ($data): ?>
            <?php foreach ($data as $key => $value){ ?>

            <div class="col-lg-4 col-md-6">
              <div class="items">
                <div class="pict">
                  <!-- <img src="<?php //echo $this->assetBaseurl ?>ex_box_black.jpg" alt="" class="img-responsive"> -->
                  <ul id="light-gallery" class="gallery light-gallery" style="list-style:none;margin:0px; padding:0;">
                      <li data-src="<?php echo $this->assetBaseurl ?>gallery/<?php echo $value['folder'] ?>/<?php echo $value['images'][0] ?>" data-sub-html="">
                        <a href="<?php echo $this->assetBaseurl ?>gallery/<?php echo $value['folder'] ?>/<?php echo $value['images'][0] ?>">
                          <img src="<?php echo $this->assetBaseurl ?>gallery/<?php echo $value['folder'] ?>/cover.jpg" alt="" class="img-responsive">
                        </a>
                      </li>
                      <?php foreach ($value['images'] as $k => $val): ?>
                        <?php if ($k == 0): ?>
                        <?php continue; ?>
                        <?php endif ?>
                        <li data-src="<?php echo $this->assetBaseurl ?>gallery/<?php echo $value['folder'] ?>/<?php echo $val; ?>" data-sub-html="">
                        </li>
                      <?php endforeach ?>
                    </ul>
                </div>
                <div class="desc padding-top-35">
                  <h5><?php echo ($value['name']) ?></h5>
                  <div class="clear"></div>
                </div>
              </div>
            </div>

            <?php } ?>

            <?php endif ?>
          </div>
          <div class="clear"></div>
        </div>
        <div class="clear height-50"></div><div class="height-40"></div>

        <div class="clear"></div>
      </div>
    </div>

    <div class="clear"></div>
  </div>
</section>
<div class="clear"></div>