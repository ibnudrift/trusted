<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      
      <div class="content-text insides_static">
        <h1 class="title_page">SURGEONS</h1>
        <div class="clear"></div>
        <div class="clear height-30"></div>

        <!-- Start content surgeons -->
        <div class="insides_c_content_surgeons">
          
          <div class="tops">
            <div class="filter_surgeons_top">
              <form action="#" class="form-inline">
                 <div class="form-group w25p">
                  <label class="sr-only" for="">Services</label>
                  <select name="#" id="" class="form-control">
                    <option value="">Select Services</option>
                    <option value="1">Services 1</option>
                    <option value="2">Services 2</option>
                    <option value="3">Services 3</option>
                  </select>
                </div>
                 <div class="form-group w25p">
                  <label class="sr-only" for="">Doctor Name</label>
                  <input type="email" class="form-control" id="" placeholder="Doctor Name">
                </div>
                 <div class="form-group w25p">
                  <label class="sr-only" for="">Nearest Location</label>
                  <input type="email" class="form-control empty" id="" placeholder="&#xf041; Nearest Location">
                </div>
                <div class="form-group w25p">
                  <button class="btn btn-default btns_submit_filter">
                    <i class="fa fa-search"></i> SEARCH
                  </button>
                </div>
              </form>
              <div class="clear"></div>
            </div>
          </div>
          <!-- end tops -->

          <div class="middles">

            <div class="box_sorting text-right">
              <span class="d-inline">Sort by:</span> 
              &nbsp;&nbsp;
              <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/index')); ?>">Alphabetical order</a>&nbsp;&nbsp;
              <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/index2')); ?>" class="active">Most reviewed</a>&nbsp;&nbsp;
              <a href="#">Date joined</a>
            </div>
            <div class="clear height-30"></div>
            
            <div class="box_listing_surgeons2">
              <div class="row">
                <div class="col-md-12">
                  <?php for ($i=1; $i < 4; $i++) { ?>
                  <div class="items">
                    <div class="picture doctor_p">
                      <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>exam_pict_doctor2.jpg" alt="" class="img-responsive">
                      </a>
                    </div>
                    <div class="ml102">
                      <div class="desc">
                        <div class="doctor_info">
                          <div class="row">
                            <div class="col-md-10">
                              <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail')); ?>">
                              <span class="names">Alexander Phoon</span>
                              </a>
                              <p>MBBS, FRACS <br>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i> &nbsp; 150 reviews <br>
                                  520 Oxford St, Westfield Office Tower 1, Suite 1805, Level 18 <br>
                                  Bondi Junction, NSW 2022 Australia <br>
                                  Phone: 1800 180777</p>

                            </div>
                            <div class="col-md-2">
                              <div class="fright padding-top-5">
                                <a href="#" class="back_purple_defaults_tl d-inline">CONTACT CLINIC</a>
                                <div class="clear height-10"></div>
                                <a href="#" class="back_purple_defaults_tl d-inline">GET COACHING SERVICE</a>
                                <div class="clear"></div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- end description -->
                    </div>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <!-- end row -->

              <div class="clear"></div>
            </div>
            <!-- end box listing surgeons1 -->

            <div class="clear"></div>
          </div>
          <!-- end middles -->
          <div class="clear"></div>
        </div>
        <!-- End content surgeons -->
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>