<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static coaching">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="height-5"></div>

      <div class="content-text insides_static text-center">
        <div class="mw903 tengah">
          <h1 class="title_page">COACHING</h1>
          <div class="clear"></div>
          <h3 class="tagline">A Personal Approach <br>
            to a Personal Journey</h3>
          <div class="clear"></div>
          <div class="pict_full">
            <img src="<?php echo $this->assetBaseurl ?>coaching-1.jpg" alt="" class="img-responsive">
          </div>
          <p>You want a Diamond NOT a Cubic Zirconia. Cosmedic Coaching is a Premium service for patients who place high value on their expectations and safety.</p>

          <p>Undergoing Plastic Surgery is a real surgery with real risks. This decision should not be taken lightly. Trusted Surgeons has been developed to guide and support the most disconcerning patients who require platinum tailored care. If you want advice you can rely on, support without an invested interest and guidance from an independent source then Nicole is the perfect coach for you.</p>

          <p>Cosmedic Coaching will</p>

          <ul>
            <li>Clarify all the myths and miss-perceptions</li>
            <li>Guidance and peace of mind</li>
            <li>Facilitate a smooth journey and decrease potential risks</li>
            <li>Give you direction as to where to go and why</li>
            <li>Provide you answers and questions so as you are prepared for your consult</li>
            <li>Not rely on fake reviews and photoshopped before and after photos</li>
            <li>Support your aesthetic goals</li>
            <li>Save you time in research and money in numerous consulations</li>
            <li>Recommend on-going options and products to support your recovery</li>
            <li>Should a complication arise you have an advocate to support you</li>
            <li>Confidentiality and Discretion is assured</li>
          </ul>

          <p>The TOP 3 Goals of Cosmedic Coaching are;</p>

          <ol>
            <li>Your Safety, not all clinics are of the same standard and surgeons are not equally qualified or trained.</li>
            <li>Your emotional well-being, I guarantee to support and guide you with empathy and cosideration from beginning until the end.</li>
            <li>Educate you, your surgeon will work with you to create a surgical plan and is the only true expert. Ensuring you know what is involved and that youhave a trusted surgeon is imperative. There will be no need to spend time on forums, social media and seeking the advice of others. You are unique and your procedure is tailored to you.</li>
          </ol>

          <p>I know the glamour and I know the hype. I have been there as a patient and as an employee. I have spent many hours analyzing the numerous websites and meeting with surgeons. I am not paid by surgeons or clinics, my loyalty and only invested interest is you. I recognize surgeons on the merit of their qualifications, experience and results. Utilisatising our resources we have handpicked Australia&#39;s most talented, skilled and experienced surgeons. The surgeons we refer our patients to are of the highest calibre.</p>

          <p>Your body is your temple, your sugeon is both your architect and sculpture, a specialist in the procedure you desire. Through our discreet referral coaching service we ensure you are prepared for your initial consultation, matched with the most appropriate surgeon and achieve the result you desire.</p>



          <div class="clear"></div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <!-- end middle content back pattern -->
    <div class="block_rate_services_coaching back-white">
      <div class="prelatife container">
        <div class="insides content-text insides_static text-center p2">
          <h1 class="title_page">Rates and Services</h1>
          <div class="clear height-15"></div>
          <p>We have two Superior Coaching Services to suit your needs; <br>
            We will customized your individual needs including revision surgery.</p>
          <div class="clear height-25"></div>

          <div class="list_coaching_service_rate">
            <div class="row">
              <div class="col-md-6">
                <div class="items">
                  <div class="pict"><img src="<?php echo $this->assetBaseurl ?>it-coaching-1.jpg" alt="" class="img-responsive"></div>
                  <div class="description">
                      <div class="hdescbottom">
                        <span>
                          Accompanying patient to surgical or aesthetic treatment consultation <br>
                          <b>$150.00 per hour</b>
                        </span>
                        <p>3 surgeons will be recommended to suit your location and procedure<br />
                          Aesthetic goal counseling and in-depth discussion<br />
                          Procedure information and coaching<br />
                          Treatment planning and schedule<br />
                          Written questions to ask your surgeon<br />
                          Skincare information and education<br />
                          Pre-treatment and Post treatment support<br />
                          30- day email and phone follow-up<br />
                          *90-minute consultation is also available per request.</p>
                        </div>
                        <div class="clear height-0"></div>
                        <div class="text-center">
                            <a href="<?php echo CHtml::normalizeUrl(array('/coaching/index2')); ?>" class="btn btn-default btn_custom_default_purple_b">INQUIRE NOW</a>
                        </div>

                    <div class="clear"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="items">
                  <div class="pict"><img src="<?php echo $this->assetBaseurl ?>it-coaching-2.jpg" alt="" class="img-responsive"></div>
                  <div class="description">
                      <div class="hdescbottom">
                        <span>
                          Plastic Surgery Concierge Service <br>
                          <b>Starting at $1000 including recovery support</b>
                        </span>
                       <p>Surgeon referral and education<br />
                          Accompaniment to consultations &amp;/or appointments<br />
                          Counselling and on-going support before, during and throughout recovery<br />
                          Patient advocacy and support<br />
                          24-hour phone support<br />
                          Arranging &amp; scheduling personal services such as hair, stylist &amp; facial treatments<br />
                          Arranging luxury transport to and from your procedure<br />
                          Arranging meal delivery<br />
                          Arranging travel or hotel accommodations<br />
                          Hotel check-in<br />
                          Chaperone services the day of treatment or surgery<br />
                          Continued phone and email support (90 day email follow-up)</p>

                        </div>
                        <div class="clear height-0"></div>
                        <div class="text-center">
                            <a href="<?php echo CHtml::normalizeUrl(array('/coaching/index2')); ?>" class="btn btn-default btn_custom_default_purple_b">INQUIRE NOW</a>
                        </div>

                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end list coaching service rate -->
          <div class="clear height-50"></div>
          <div class="height-10"></div>

          <div class="mw1021 tengah fz16_p">
              <p>We will customize your services based on your unique personal and family needs. Everything about your planning and your procedure is tailored for you.</p>

              <p>* Full payment is required prior. A credit card will secure your appointment.</p>

              <p>* A 24 hour cancellation notice is required and payment will be refunded<br />
                All clients are required to sign a disclaimer prior to their consultation.<br />
                We will try to negotiate consultation fees or get them waived on your behalf.</p>

            <div class="clear"></div>
          </div>
          <div class="mw1021 tengah">
              <div class="blocks_purple_bottom_coaching_content">
                <p>
                  Consultation is not a substitute for medical advice. We do not give any medical advice. <br>
                  Any medical or psychological counseling is referred out.  <br>
                  * We are not associated with any medical offices or providers and do not accept monetary compensation for our referrals.
                </p>
                <div class="clear"></div>
              </div>
          </div>




          <div class="clear"></div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
    <!-- end back white bottom -->
    <div class="clear"></div>
  </div>
</section>