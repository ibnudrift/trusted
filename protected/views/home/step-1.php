<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['journey_step_1_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h2>Step 1</h2>
      <h3><?php echo $this->setting['journey_step_1_title'] ?></h3>
    </div>
  </div>
  <div class="clear"></div>
</div>

<section class="section_default back_cream mh710 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

    <div class="blocks_journey text-center step_1">
      <div class="tops">
        <h6 class="sub_title">START JOURNEY</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-10"></div>
        <?php /*
        */ ?>

        <!-- <h1 class="titlepage">Step 1</h1> -->
        <h1 class="c1"><?php echo $this->setting['journey_step_1_title'] ?></h1>
      </div>

      <div class="clear height-50"></div><div class="height-20"></div>

      <div class="middle prelatife">
        <div class="row default">
          <div class="col-md-8 col-lg-8">
            <div class="mw909 content-text page_faq_content">
              <h4><?php echo nl2br($this->setting['journey_step_1_subtitle']) ?></h4>
              <div class="landing_hero pict_full">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(909,1000, '/images/static/'.$this->setting['journey_step_1_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
              </div>
              <div class="clear"></div>
              <?php echo $this->setting['journey_step_1_content'] ?>
                <div class="clear height-20"></div>

                <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'3')); ?>" class="btns_goto_step">GO TO STEP 2</a>


                <?php /*
                <div class="box_subscribe_purple">
                  <div class="insides">
                    <h4>Subscribe to our mail list</h4>
                    <div class="clear"></div>
                    <p>Sign up for our newsletter to receive the latest news, tips and advice from Trusted Surgeons.</p>
                    <div class="clear height-20"></div><div class="height-3"></div>
                    <div class="box_form">
                      <form class="form-inline" method="post" action="//trustedsurgeons.us13.list-manage.com/subscribe/post?u=03556616b4e4bd1907259af43&amp;id=b2ba371703">
                          <div class="form-group">
                            <label class="sr-only" for=""></label>
                            <input type="text" name="NAME" class="form-control" id="" placeholder="Your Name" required="required">
                          </div>
                          <div class="form-group">
                            <label class="sr-only" for=""></label>
                            <input type="email" name="EMAIL" class="form-control" id="" placeholder="Your Email">
                          </div>
                          <button type="submit" class="btn btn-default btn_purple_subscribe">SUBMIT</button>
                        </form>
                      <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                  </div>
                </div>*/ ?>

                
              <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div class="blocks_right_stepmenu_inside">
              <?php echo $this->renderPartial('//layouts/_menu_step', array()); ?>
            </div>
          </div>
        </div>
        <div class="clear height-10"></div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
  </div>

</section>
<?php
$criteria = new CDbCriteria;
$criteria->addCondition('ads_id = :ads_id');
$criteria->params[':ads_id'] = 2;
$criteria->order = 'sort';
// $criteria->group = 't.id';
$dataAds = AdsImage::model()->findAll($criteria);
?>
    <p>
    <?php foreach ($dataAds as $key => $value): ?>
      <a href="<?php echo $value->url ?>">
      <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(728,90, '/images/ads_image/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block margin-bottom-20">
      </a>
    <?php $value->view = $value->view + 1; $value->save(false);  ?>
    <?php endforeach ?>

    </p>
    <div class="height-30"></div>
<script type="text/javascript">
  $(function(){
      var iconOpen = 'icon_minus',
        iconClose = 'icon_plus';
        // icons 

    var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    $active.find('a').prepend('<i class="icons icon_minus"></i>');
    $('#accordion .panel-heading').not($active).find('a').prepend('<i class="icons icon_plus"></i>');
    $('#accordion').on('show.bs.collapse', function (e) {
      $('#accordion .panel-heading.active').removeClass('active').find('.icons').toggleClass('icon_plus icon_minus');
      $(e.target).prev().addClass('active').find('.icons').toggleClass('icon_plus icon_minus');
    });
    // end function accordion

  });
</script>
<style type="text/css">
  .panel-default .panel-body p{
    margin-bottom: 0 !important;
  }
</style>