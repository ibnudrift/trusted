<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static">
        <h1 class="title_page">EDUCATION</h1>
        <div class="clear height-50"></div>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">
                <h4>So many procedures and techniques. Learn the basics by picking on your procedure below.</h4>
                <div class="clear height-10"></div>
                <div class="box_slider_step6">
                    <div class="pict_full">
                        <style>
                        /*.embed-container { position: relative; padding-bottom: 61.45%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }*/
                        </style>
                        <!-- <div class='embed-container'><iframe src='https://content.understand.com/tsaustralia.menu' style='border:0'></iframe></div> -->
                        <iframe src="https://content.understand.com/tsaustralia.menu" border="0" frameborder="0" scrolling="no" width="909" style="min-width:100%; width:100%;" height="557"></iframe>
                    </div>
                  <div class="clear"></div>
                </div>



                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">EDUCATION</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                  <li class="active"><a href="#">3D Surgery Animation</a></li>
                  <li><a href="#">FAQ</a></li>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>