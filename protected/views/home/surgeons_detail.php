<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us surgeons_detail_page">
    <div class="prelatife container">
      <div class="clear height-50"></div>
      <div class="clear height-10"></div>
      <div class="pict_full illustration_picture mxh379">
        <img src="<?php echo $this->assetBaseurl ?>picts_detail_psurgeons.jpg" alt="" class="img-responsive">
      </div>
      <div class="blocks_top_detail_surgeons back-white h80 prelatife">
        <div class="mlr20 prelatife">
          <div class="pos_tops">
            <div class="row prelatife">
              <div class="col-md-9">
                <div class="clear height-35"></div>
                <div class="picture_doctor d-inline v-top">
                  <img src="<?php echo $this->assetBaseurl ?>exam_pict_doctor3.jpg" alt="" class="img-responsive">
                </div>
                <div class="d-inline v-top padding-left-20 right_conts">
                  <div class="clear height-10"></div>
                  <div class="titles">
                    Alexander Phoon <br>
                    MBBS, FRACS</div>
                    <div class="clear"></div>
                    <div class="descs">
                      <span>Bondi Junction NSW 2022, Launceston TAS 7250, Kangaroo Point QLD 4169</span>
                      <div class="clear"></div>
                      <p>520 Oxford St, Westfield Office Tower 1, Suite 1805, Level 18. Bondi Junction, NSW 2022 Australia</p>
                      <div class="clear"></div>
                      <div class="blocks_social_surgeons">
                        <ul class="list-inline">
                          <li><i class="fa fa-phone"></i> &nbsp;1800 180777</li>
                          <li><a target="_blank" href="#"><i class="fa fa-link"></i> &nbsp;Website</a></li>
                          <li><a href="#" target="_blank"><i class="fa fa-facebook-square"></i> &nbsp;Facebook</a></li>
                          <li><a href="#" target="_blank"><i class="fa fa-instagram"></i> &nbsp;Instagram</a></li>
                          <li><a href="#" target="_blank"><i class="fa fa-twitter"></i> &nbsp;Twitter</a></li>
                        </ul>
                      </div>
                      <div class="clear"></div>
                    </div>
                </div>

              </div>
              <div class="col-md-3">
                <div class="fright padding-top-0 blocks_right_review">
                    <a href="#" class="back_purple_defaults_tl d-inline">CONTACT CLINIC</a>
                    <div class="clear height-10"></div>
                    <a href="#" class="back_purple_defaults_tl d-inline">GET COACHING SERVICE</a>
                    <div class="clear height-30"></div>
                    <p><i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> &nbsp; 50 reviews</p>
                    <div class="clear"></div>
                  </div>

              </div>
            </div>
            <div class="clear"></div>
          </div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
    <!-- end block white top -->

    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="content-text insides_static">
        <!-- Start content surgeons -->
        <div class="insides_c_content_surgeons s_detail">
            
            <div class="sBox_surgeons_details">
              <div>
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">ABOUT</a></li>
                  <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">REVIEWS</a></li>
                  <li role="presentation"><a href="#special" aria-controls="special" role="tab" data-toggle="tab">SPECIALIZATION</a></li>
                  <li role="presentation"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">PHOTOS</a></li>
                  <li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">VIDEOS</a></li>
                  <li role="presentation"><a href="#blogs" aria-controls="blogs" role="tab" data-toggle="tab">BLOGS</a></li>
                  <li role="presentation"><a href="#maps" aria-controls="maps" role="tab" data-toggle="tab">MAPS</a></li>
                </ul>

                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="about">
                      <h4>About Alexander Phoon</h4>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus neque, vulputate vitae finibus quis, faucibus vel mi. Nulla tincidunt efficitur justo in egestas. Curabitur molestie a ipsum eu bibendum. Quisque hendrerit ex sapien, ut facilisis quam fringilla vitae. Donec at tincidunt ante. Aliquam neque metus, lobortis a nunc id, pharetra tempus metus. Proin eleifend condimentum placerat.</p>
                      <p>Nullam vehicula libero a leo scelerisque ornare. Nulla lacinia dictum eros, a egestas quam posuere nec. Donec urna odio, commodo ac aliquam pellentesque, varius vitae nunc. Nulla suscipit efficitur tellus, vitae lacinia purus efficitur et. Nunc accumsan justo a leo ornare, et molestie massa fermentum. Ut turpis massa, eleifend sit amet finibus blandit, elementum a dui. Quisque luctus dolor ac augue venenatis tempus. Praesent aliquet fermentum urna sed pharetra. Curabitur quis lacinia eros, ac dignissim magna. Suspendisse non eros vehicula elit condimentum feugiat ut vel diam. Curabitur interdum felis tempor imperdiet consequat. Sed cursus justo a ante viverra, fringilla placerat velit volutpat. Suspendisse mattis nunc urna, a commodo felis porta id. Proin viverra nisl sed est elementum, ut consequat quam efficitur. Maecenas placerat efficitur est non eleifend. Donec consectetur lacus vel tortor sollicitudin, ut gravida nisl placerat.</p>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="reviews">
                      <div class="blocks_review_surgeonsDetails">
                        <h4>Reviews</h4>
                        <div class="row">
                          <div class="col-md-5">
                            <h5>Ratings based on 19 reviews</h5>
                            <div class="ratings_top">
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; Overall rating</div>
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; Doctor&#39;s bedside manner</div>
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; Answered my questions</div>
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; After care follow-up</div>
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; Time spent with me</div>
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; Phone or email responsiveness</div>
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; Staff professionalism &amp; courtesy</div>
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; Payment Process</div>
                              <div class="ls"><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i> &nbsp; Wait times</div>
                              <div class="clear"></div>
                            </div>
                            <div class="clear height-20"></div>
                          </div>
                          <div class="col-md-7">
                            <h5>Write a review</h5>
                            <p>Are you a patient of Scott J. Turner, MBBS, FRACS?</p>
                            <a href="#" class="back_purple_defaults_tl">WRITE A REVIEW</a>
                            <div class="clear height-20"></div>

                            <h5>Reviews (19)</h5>
                            <div class="filters">
                              <p>
                                Filter by: &nbsp;
                                 <a class="active" href="#">Featured</a>
                                 &nbsp;|&nbsp;
                                 <a href="#">Highest Rating</a>
                                 &nbsp;|&nbsp;
                                 <a href="#">Lowest Rating</a>
                                 &nbsp;|&nbsp;
                                 <a href="#">Recent</a>
                              </p>
                            </div>
                            <div class="clear height-20"></div>

                            <div class="listing_default_reviews">
                              <?php for ($i=0; $i < 3; $i++) { ?>
                              <div class="items">
                                <span class="top_nstar">
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i> &nbsp;  Rhinoplasty
                                </span>
                                <div class="clear height-5"></div>
                                <span class="titles_nm">Rhinoplasty. Newcastle, AU</span>
                                <div class="clear height-5"></div>
                                <span class="monts">few months ago.</span>
                                <div class="clear height-5"></div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel natus perferendis, autem tempora obcaecati, nihil voluptas. Accusamus sint laborum, assumenda corrupti, adipisci officia, nesciunt quas sed quidem fugit modi unde!</p>
                              </div>
                              <?php } ?>
                            </div>
                            <!-- end list reviews user -->

                            <div class="clear"></div>
                          </div>
                        </div>

                        <div class="clear"></div>
                      </div>
                      
                  </div>
                  <div role="tabpanel" class="tab-pane" id="special">
                      <h4>Specialization</h4>
                      <ul>
                        <li>Breast Implants</li>
                          <li>Breast Reconstruction</li>
                          <li>Breast Lift</li>
                          <li>Breast Reduction</li>
                          <li>Plastic Surgeon Consultation</li>
                          <li>Tummy Tuck</li>
                      </ul>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="photos">
                      <h4>Photos</h4>
                      <div class="clear"></div>
                      <div class="list_gallery_default">
                        <div class="row">
                          <?php for ($i=0; $i < 5; $i++) { ?>
                          <div class="col-md-3">
                            <div class="items">
                              <div class="pict">
                                <a href="#">
                                <img src="http://placehold.it/210x210" alt="" class="img-responsive">
                                </a>
                              </div>
                              <div class="clear height-15"></div>
                              <a href="#"><span class="titles">45-54 years old woman treated with Breast Lift</span></a>
                              <div class="clear"></div>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                      <!-- end list gallery -->

                  </div>
                  <div role="tabpanel" class="tab-pane" id="videos">
                      <h4>Videos</h4>

                      <div class="list_gallery_default video">
                        <div class="row">
                          <?php for ($i=0; $i < 5; $i++) { ?>
                          <div class="col-md-4">
                            <div class="items <?php if ($i == 0): ?>first<?php endif ?> <?php if ($i == 4): ?>last<?php endif ?>">
                              <div class="pict">
                                <a href="#">
                                  <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'>
                                  <iframe src='http://www.youtube.com/embed/qjNmcXnTpXE' frameborder='0' allowfullscreen></iframe></div>
                                </a>
                              </div>
                              <div class="clear height-15"></div>
                              <a href="#"><span class="titles">Title Video</span></a>
                              <div class="clear"></div>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                      <!-- end list gallery -->
                  </div>
                  <div role="tabpanel" class="tab-pane" id="blogs">
                      <h4>Blogs</h4>
                      <div class="list_blog_default">
                        <div class="row">
                          <?php for ($i=0; $i < 2; $i++) { ?>
                          <div class="col-md-12">
                            <div class="items">
                              <div class="row default">
                                <div class="col-md-3">
                                  <div class="pict">
                                    <a href="#">
                                    <img src="http://placehold.it/350x210" alt="" class="img-responsive">
                                    </a>
                                  </div>
                                </div>  
                                <div class="col-md-9">
                                  <div class="description">
                                    <span class="titles">Vectra 3D Imaging - See your "After" pic before surgery - Visualise</span>
                                    <div class="clear height-15"></div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis vel voluptate consectetur omnis reiciendis quam pariatur laudantium, hic quos optio modi ipsa, quis voluptatibus consequatur libero fugit velit repudiandae quaerat?</p>
                                    <a href="#" class="link_more">read more</a>
                                    <div class="clear"></div>
                                  </div>
                                </div>  
                              </div>

                              <div class="clear"></div>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                      <!-- end list gallery -->
                  </div>
                  <div role="tabpanel" class="tab-pane" id="maps">
                      <h4>Maps</h4>
                      <div class="row">
                        <div class="col-md-6">
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3316.957812482902!2d150.9922284185925!3d-33.76175601758621!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12a22e1a2f24fd%3A0x7ba197a89b5dac61!2sThe+Website+Marketing+Group+-+TWMG!5e0!3m2!1sid!2sid!4v1469528363912" width="569" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-6">
                          <div class="maps_info">
                            <p>10 Boland St, Launceston TAS 7250<br />
                              P: +61 (03) 6331 3479<br />
                              F: +61 (03) 6334 9678<br />
                              <a href="http://www.drrobertboyle.com" target="_blank">www.drrobertboyle.com</a></p>

                              <p>Level 1 21 Spring Street, Bondi Junction NSW 2022<br />
                              P: +61 (02) 9518 9497<br />
                              <a href="http://www.drrobertboyle.com" target="_blank">www.drrobertboyle.com</a></p>

                              <p>Cosmetic Surgery Advisory Clinic<br />
                              634 Main Street, Kangaroo Point QLD 4169<br />
                              P: +61 (07) 3395 5366<br />
                              <a href="http://www.drrobertboyle.com" target="_blank">www.drrobertboyle.com</a></p>

                            <div class="clear"></div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>

              </div>

              <div class="clear"></div>
            </div>
            <!-- End detail surgeons -->

          <div class="clear"></div>
        </div>
        <!-- End content surgeons -->
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>

    <div class="clear"></div>
  </div>
</section>