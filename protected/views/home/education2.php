<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_faq_content">
        <h1 class="title_page">EDUCATION</h1>
        <div class="clear height-50"></div>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">
                <h4>Frequently Asked Questions</h4>
                <div class="clear height-10"></div>
                <div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>faq-1.jpg" alt="" class="img-responsive"></div>
                <form action="#" class="form-inline">
                  <div class="form-group">
                    <label for="" class="control-label"><b>Topic</b></label>
                    <div class="d-inline box_select_f padding-left-15">
                      <select class="form-control selectpicker">
                        <option>Face</option>
                        <option>Body</option>
                        <option>Breast</option>
                      </select>
                    </div>
                  </div>
                </form>
                <div class="clear height-20"></div><div class="height-2"></div>
                <div class="lines-grey"></div>
                <div class="clear height-30"></div>

                <?php 
                  $questions_d = array(
                          array(
                            'question' => 'How safe is it?',
                            'answer' => 'Though we can never guarantee the forces of nature, and in these times of world wide unrest, we can say with sincerity that the hospital we have chosen has not had anything happen in its region for over 25 years.',
                            ),
                          array(
                            'question' => 'How qualified are the doctors?',
                            'answer' => 'Though we can never guarantee the forces of nature, and in these times of world wide unrest, we can say with sincerity that the hospital we have chosen has not had anything happen in its region for over 25 years.',
                            ),
                          array(
                            'question' => 'Do I get to choose the doctor?',
                            'answer' => 'Though we can never guarantee the forces of nature, and in these times of world wide unrest, we can say with sincerity that the hospital we have chosen has not had anything happen in its region for over 25 years.',
                            ),
                          array(
                            'question' => 'What can I expect from the results?',
                            'answer' => 'Though we can never guarantee the forces of nature, and in these times of world wide unrest, we can say with sincerity that the hospital we have chosen has not had anything happen in its region for over 25 years.',
                            ),
                          array(
                            'question' => 'What happen if something goes wrong?',
                            'answer' => 'Though we can never guarantee the forces of nature, and in these times of world wide unrest, we can say with sincerity that the hospital we have chosen has not had anything happen in its region for over 25 years.',
                            ),
                          array(
                            'question' => 'What care is offered for people on their own?',
                            'answer' => 'Though we can never guarantee the forces of nature, and in these times of world wide unrest, we can say with sincerity that the hospital we have chosen has not had anything happen in its region for over 25 years.',
                            ),
                          array(
                            'question' => 'Airfares and Accommodation.',
                            'answer' => 'Though we can never guarantee the forces of nature, and in these times of world wide unrest, we can say with sincerity that the hospital we have chosen has not had anything happen in its region for over 25 years.',
                            ),
                    );
                ?>

                 <div class="panel-group" id="accordion">
                    <?php foreach ($questions_d as $key => $value): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel_<?php echo $key ?>"><?php echo $value['question'] ?></a>
                            </h4>
                        </div>
                        <div id="panel_<?php echo $key ?>" class="panel-collapse collapse <?php if ($key == 0): ?>in<?php endif ?>">
                            <div class="panel-body">
                                <p><?php echo $value['answer'] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>

                </div>

                <!-- end accordion -->
                <div class="clear height-20"></div><div class="height-2"></div>
                <div class="lines-grey"></div>
                <div class="clear height-40"></div>

                <div class="pict_full"><a href="#"><img src="<?php echo $this->assetBaseurl ?>pict-faq-education-bottom.png" alt="" class="img-responsive"></a></div>


                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">EDUCATION</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                  <li><a href="#">3D Surgery Animation</a></li>
                  <li class="active"><a href="#">FAQ</a></li>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>
<script type="text/javascript">
  $(function(){
      var iconOpen = 'icon_minus',
        iconClose = 'icon_plus';
        // icons 

    var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    $active.find('a').prepend('<i class="icons icon_minus"></i>');
    $('#accordion .panel-heading').not($active).find('a').prepend('<i class="icons icon_plus"></i>');
    $('#accordion').on('show.bs.collapse', function (e) {
      $('#accordion .panel-heading.active').removeClass('active').find('.icons').toggleClass('icon_plus icon_minus');
      $(e.target).prev().addClass('active').find('.icons').toggleClass('icon_plus icon_minus');
    });
    // end function accordion

  });
</script>