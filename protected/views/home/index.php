<?php 
  $dt_steps = array(
             1 => array(
                'name' => 'Step 1',
                'desc' => strtolower($this->setting['journey_step_1_title']),
                ),
              // array(
              //   'name' => 'Step 2',
              //   'desc' => strtolower($this->setting['journey_step_2_title']),
              //   ),
              array(
                'name' => 'Step 2',
                'desc' => strtolower($this->setting['journey_step_3_title']),
                ),
              array(
                'name' => 'Step 3',
                'desc' => strtolower($this->setting['journey_step_4_title']),
                ),
              array(
                'name' => 'Step 4',
                'desc' => strtolower($this->setting['journey_step_5_title']),
                ),
              // array(
              //   'name' => 'Step 6',
              //   'desc' => strtolower($this->setting['journey_step_6_title']),
              //   ),
              array(
                'name' => 'Step 5',
                'desc' => strtolower($this->setting['journey_step_7_title']),
                ),
              // array(
              //   'name' => 'Step 8',
              //   'desc' => strtolower($this->setting['journey_step_8_title']),
              //   ),
              // array(
              //   'name' => 'Step 9',
              //   'desc' => strtolower($this->setting['journey_step_9_title']),
              //   ),
              array(
                'name' => 'Step 6',
                'desc' => strtolower($this->setting['journey_step_10_title']),
                ),
            );

?>

<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('active = 1');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->group = 't.id';
$dataSlide = Slide::model()->findAll($criteria);
?>
<div class="outers_blck_fcs h100per sect_cfcs prelatife">
    <div id="myCarousel" class="carousel fade" data-ride="carousel">
      <div class="bottoms_carouselindic">
        <div class="prelatife container">
            <ol class="carousel-indicators">
              <?php foreach ($dataSlide as $key => $value): ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $key ?>" class="<?php if ($key == 0): ?>active<?php endif ?>"></li>
              <?php endforeach ?>
            </ol>
          </div>
        </div>

        <div class="carousel-inner">
            <?php foreach ($dataSlide as $key => $value): ?>
            <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                <div class="fill" style="background-image:url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1582,632, '/images/slide/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>');"></div>
                <div class="carousel-caption">
                  <div class="blocks_txt_infcs">
                    <div class="prelatife container">
                          <h3><?php echo $value->description->title ?></h3>
                          <div class="clear"></div>
                          <h6><?php echo $value->description->content ?></h6>
                          <div class="clear height-30"></div>
                          <?php if ($value->description->url != ''): ?>
                          <a href="<?php echo $value->description->url ?>" class="btn btn-link btns_vwf_slide"><?php echo $value->description->text_url ?></a>
                          <?php endif ?>
                            <div class="clear"></div>
                          <div class="clear"></div>
                      </div>
                  </div>
                </div>
            </div>
            <?php endforeach ?>
            
        </div>

    </div>

    <div class="back_pattern_purple_bottom_slider">
      <div class="container prelatife text-center">
        <div class="clear height-50"></div>
        <div class="inside_text">
          <!-- <span class="top">Find a Surgeon You Can Trust</span> -->
          <h1>Find a Surgeon You Can Trust</h1>
          <div class="clear height-30"></div>
          <div class="box_filter_center">
            <form class="form-inline" method="get" action="<?php echo CHtml::normalizeUrl(array('/surgeons/list')); ?>">
              <div class="form-group">
                <!-- <label for="exampleInputName2">Procedure</label> -->
                <div class="d-inline selection v-top">
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js"></script>
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css">
                                      <input type="text" name="specialisation" id="specialisation" class="form-control selectpick css-select-moz" placeholder="Procedure" />

                    <script type="text/javascript">
                    var options = {
                        url: "<?php echo CHtml::normalizeUrl(array('/home/specialisationList')); ?>",
                        list: {
                            match: {
                              enabled: true
                            }
                        },
                        getValue: "name",

                        template: {
                          type: "description",
                          fields: {
                            description: "type"
                          }
                        }

                    };

                    $("#specialisation").easyAutocomplete(options);
                    </script>
                </div>
              </div>
              <div class="form-group">
                <input type="text" name="name" id="doctor-auto" class="form-control selectpick css-select-moz" placeholder="Surgeons Name" />
              </div>
              <div class="form-group">
                <!-- <label for="exampleInputName2">Location based on?</label> -->
                <div class="d-inline selection v-top">
                  <input type="text" name="suburb" id="suburb-auto" class="form-control selectpick css-select-moz" placeholder="Location" />
                <script type="text/javascript">
                var options = {
                    url: function(phrase) {
                      return "<?php echo CHtml::normalizeUrl(array('/home/locationList')); ?>";
                    },

                    ajaxSettings: {
                      dataType: "json",
                      method: "POST",
                      data: {
                        dataType: "json"
                      }
                    },
                    preparePostData: function(data) {
                      data.phrase = $("#suburb-auto").val();
                      return data;
                    },
                    // url: "<?php echo CHtml::normalizeUrl(array('/home/locationList')); ?>",
                    // list: {
                    //     match: {
                    //       enabled: true
                    //     }
                    // },

                    requestDelay: 400

                };
                // Try HTML5 geolocation.
                <?php if ($_GET['suburb'] == ''): ?>
                if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function(position) {
                    $.ajax({
                      method: "POST",
                      url: "<?php echo CHtml::normalizeUrl(array('/home/getAddress')); ?>",
                      data: { lat: position.coords.latitude, lng: position.coords.longitude },
                      dataType: 'json'
                    })
                    .done(function( msg ) {
                      $('#suburb-auto').val(msg.address);
                    });

                  }, function() {
                    // alert('Browser doesn\'t support Geolocation');
                  });
                } else {
                  // Browser doesn't support Geolocation
                  // alert('Browser doesn\'t support Geolocation');
                  // handleLocationError(false, infoWindow, map.getCenter());
                }
                <?php endif ?>

                $("#suburb-auto").easyAutocomplete(options);
                </script>

                </div>
              </div>
              <button type="submit" class="btn btn-default btn_submit_custom">SEARCH</button>
            </form>
            <div class="clear height-20"></div>
            <div class="text-center">
              <p>Or <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/list')); ?>">click here</a> to view all Surgeons</p>
            </div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>

<!-- featured section surgeons -->
<section class="section_default back_surgeons_home mh710">
  <div class="prelatife container">
    <div class="clear h65"></div>
    
    <div class="tops margin-bottom-50">
      <h6>FEATURED TRUSTED SURGEONS</h6>
      <div class="clear"></div>
      <h5 class="smalls hide hidden">We connect you with qualified and trained registered professionals</h5>
      <div class="clear height-40"></div>
<?php
$dataCategory = ViewCategory::model()->findAll('language_id = :language_id AND type = :type AND parent_id = 0 ORDER BY sort ASC', array(':language_id'=>$this->languageID, ':type'=>'spesialis'));
?>
      <div class="block_nav">
        <ul class="list-inline">
          <?php foreach ($dataCategory as $key => $value): ?>
          <li <?php if ($key == 0): ?>class="active"<?php endif ?>><a href="#face_<?php echo Slug::create($value->name) ?>" aria-controls="<?php echo Slug::create($value->name) ?>" role="tab" data-toggle="tab"><?php echo $value->name ?></a></li>
          <?php endforeach ?>
        </ul>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear height-2"></div>
    <div class="middles">
      <script type="text/javascript"> 
      $(window).load(function(){
        $('.lists_OwlFeatured_data_surgeons').owlCarousel({
              margin:0,
              nav:false,
              pagination: true,
              items: 4,
              loop: true,
              autoPlay: true,
              autoplayTimeout:1000,
              autoplayHoverPause:true,
              responsive:{
                  0:{
                      items:1
                  },
                  600:{
                      items:2
                  },
                  1120:{
                      items:3
                  },
                  1300:{
                      items:4
                  }
              }
          });
      });
 
      $(document).ready(function(){
        $('.block_nav ul.list-inline li a').live('click', function(){
          var sn_active = $(this).attr('href');
          $(sn_active+' .lists_OwlFeatured_data_surgeons').owlCarousel({
              margin:0,
              nav:false,
              pagination: true,
              items: 4,
              loop: true,
              autoPlay: true,
              autoplayTimeout:1000,
              autoplayHoverPause:true,
              responsive:{
                  0:{
                      items:1
                  },
                  600:{
                      items:2
                  },
                  1120:{
                      items:3
                  },
                  1300:{
                      items:4
                  }
              }
          });
        });
      });

      </script>
       <div class="tab-content">
          <?php foreach ($dataCategory as $key => $value): ?>
          <div role="tabpanel" class="tab-pane <?php if ($key == 0): ?>active<?php endif ?>" id="face_<?php echo Slug::create($value->name) ?>">
            <div class="box_listing_surgeons1">
              <div class="row owl-carousel owl-theme lists_OwlFeatured_data_surgeons">
<?php
$dataFeatured = DoctorFeatured::model()->findAll('type = :type', array(':type'=>'Home | '.$value->name));
?>
                <?php foreach ($dataFeatured as $v): ?>
                <?php if ($v->doctor_id != 0): ?>
                  
<?php
$value = Doctor::model()->with(array('specialication', 'clinics', 'clinic'))->findByPk($v->doctor_id);
?>
                <div class="col-md-12 col-lg-12 item">
                  <div class="paddings">
                    <div class="items">
                      <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'slug'=>$value->slug)); ?>">
                          <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(283,283, '/images/doctor/'.$value->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        </a>
                      </div>
                      <div class="desc">
                        <?php /*<div class="doctor_p d-inline padding-right-15">
                          <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(82,82, '/images/doctor/'.$value->photo , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                        </div>*/ ?>
                        <div class="doctor_info d-inline">
                          <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'slug'=>$value->slug)); ?>">
                          <span class="names"><?php echo strtoupper($value->name) ?></span>
                          </a>
                          <p><?php echo $value->certification ?> <br>
                            <?php if ($value->n_review > 0): ?>
                                <?php echo DoctorReviewCategory::model()->createStar($value->rating) ?> &nbsp; <?php echo $value->n_review ?> reviews <br>
                            <?php endif ?>
                             <?php // echo $value->clinics[0]->suburb ?></p>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <?php endif ?>
                <?php endforeach ?>
              </div>
              <!-- end row -->
              <div class="clear"></div>
            </div>
            <!-- end box listing -->
          </div>
          <?php endforeach ?>
        </div>

      <div class="clear"></div>
    </div>
    <div class="clear"></div>

    <div class="h65"></div>


<?php
$criteria = new CDbCriteria;
$criteria->addCondition('ads_id = :ads_id');
$criteria->params[':ads_id'] = 1;
$criteria->order = 'sort';
// $criteria->group = 't.id';
$dataAds = AdsImage::model()->findAll($criteria);
?>
    <p>
    <?php foreach ($dataAds as $key => $value): ?>
      <a href="<?php echo $value->url ?>">
      <img src="<?php echo Yii::app()->baseUrl.'/images/ads_image/'.$value->image ?>" alt="" class="img-responsive center-block margin-bottom-20">
      </a>
    <?php $value->view = $value->view + 1; $value->save(false);  ?>
    <?php endforeach ?>

    </p>
    <div class="height-30"></div>
      <div class="tops margin-bottom-50">
        <h6>NEWS & MEDIA</h6>
        <div class="clear height-5"></div>
      </div>
      <div class="middles outer-block-news">
        <div class="blocks_news_data">
<?php
    $criteria = new CDbCriteria;

    $criteria->order = 't.date_input DESC';

    $criteria->group = 't.id';

    $dataFeatured = new CActiveDataProvider('DoctorBlog', array(
      'criteria'=>$criteria,
        'pagination'=>array(
            'pageSize'=>2,
        ),
    ));

?>
          <div class="row">
            <?php foreach ($dataFeatured->getData() as $key => $value): ?>
            <div class="col-md-6">
              <div class="items">
                <div class="picture prelatife"><a href="<?php echo CHtml::normalizeUrl(array('/', 'blog'=>$value->slug)); ?>.html"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(614,362, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a></div>
                <div class="info">
                  <div class="row">
                    <div class="col-md-3">
                      <span class="dates"><a href="<?php echo CHtml::normalizeUrl(array('/', 'blog'=>$value->slug)); ?>.html"><?php echo strtoupper(date('d F Y', strtotime($value->date_input))) ?></a></span>
                    </div>
                    <div class="col-md-9 bdr_right">
                      <div class="titles"><a href="<?php echo CHtml::normalizeUrl(array('/', 'blog'=>$value->slug)); ?>.html"><?php echo $value->title ?></a></div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <?php endforeach ?>
          </div>
        </div>
        <div class="clear"></div>
      </div>

      <!-- instagram container -->
      <div class="h65"></div>
      <div class="tops margin-bottom-50">
        <h6>FOLLOW US</h6>
        <div class="clear height-5"></div>
      </div>
      <div class="middles outer-block-feeds">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js"></script>
<?php if (isset($_SERVER['HTTP_USER_AGENT'])): ?>
    <?php $agent = $_SERVER['HTTP_USER_AGENT']; ?>
<?php endif; ?>
<?php /*
<?php if (strlen(strstr($agent, 'Firefox')) > 0): ?>
    <script type="text/javascript">
        var feed = new Instafeed({
            get: 'user', 
            userId: '1712530961',
            // tagName: 'sumantour',
            // https://instagram.com/oauth/authorize/?client_id=3c03a0fd2a354ad59621aea34ceeb3be&redirect_uri=http://sumantour.com&response_type=token&scope=public_content+basic 
            // clientId: '1677ed07ddd54db0a70f14f9b1435579',
            clientId: 'c7615b9ef3344cdaa551eedc8a539e98',
            accessToken: '<?php echo $this->setting['insta_token'] ?>',
            target: 'instafeed',
            sortBy: 'most-recent',
            limit: '100',
            resolution: 'low_resolution',
            //template: '<li><a href="{{link}}" target="_blank"><img src="{{image}}" alt=""></a></li>',
            template: '<div class="item"><div class="col-md-12"><div class="picture prelatife"><a href="{{link}}" target="_blank"><img src="{{image}}" width="300" alt=""></a></div></div></div>',
            after: function(){
                  $('.block_feed.owl-theme').owlCarousel({
                      margin:0,
                      nav:false,
                      pagination: true,
                      items: 4,
                      loop: true,
                      autoPlay: true,
                      autoplayTimeout:1000,
                      autoplayHoverPause:true,
                      responsive:{
                          0:{
                              items:1
                          },
                          600:{
                              items:2
                          },
                          1120:{
                              items:3
                          },
                          1300:{
                              items:4
                          }
                      }
                  });

                },
        });
      feed.run();
    </script>
<?php else: ?>
    <script src="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/js/jquery.lazy.min.js" defer></script>
    <script type="text/javascript">
        var feed = new Instafeed({
            get: 'user', 
            userId: '1712530961',
            // tagName: 'sumantour',
            // https://instagram.com/oauth/authorize/?client_id=3c03a0fd2a354ad59621aea34ceeb3be&redirect_uri=http://sumantour.com&response_type=token&scope=public_content+basic 
            // clientId: '1677ed07ddd54db0a70f14f9b1435579',
            clientId: 'c7615b9ef3344cdaa551eedc8a539e98',
            accessToken: '<?php echo $this->setting['insta_token'] ?>',
            target: 'instafeed',
            sortBy: 'most-recent',
            limit: '100',
            resolution: 'low_resolution',
            //template: '<li><a href="{{link}}" target="_blank"><img src="{{image}}" alt=""></a></li>',
            template: '<div class="item"><div class="col-md-12"><div class="picture prelatife"><a href="{{link}}" target="_blank"><img class="lazy" data-src="{{image}}" width="300" alt=""></a></div></div></div>',
            after: function(){
                  $('.block_feed.owl-theme').owlCarousel({
                      margin:0,
                      nav:false,
                      pagination: true,
                      items: 4,
                      loop: true,
                      autoPlay: true,
                      autoplayTimeout:1000,
                      autoplayHoverPause:true,
                      responsive:{
                          0:{
                              items:1
                          },
                          600:{
                              items:2
                          },
                          1120:{
                              items:3
                          },
                          1300:{
                              items:4
                          }
                      }
                  });

                },
        });
      feed.run();
    </script>    
<?php endif; ?>
*/ ?>
<script src="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/js/jquery.lazy.min.js" defer></script>
<script type="text/javascript">
    var feed = new Instafeed({
        get: 'user', 
        userId: '1712530961',
        // tagName: 'sumantour',
        // https://instagram.com/oauth/authorize/?client_id=3c03a0fd2a354ad59621aea34ceeb3be&redirect_uri=http://sumantour.com&response_type=token&scope=public_content+basic 
        // clientId: '1677ed07ddd54db0a70f14f9b1435579',
        clientId: 'c7615b9ef3344cdaa551eedc8a539e98',
        accessToken: '<?php echo $this->setting['insta_token'] ?>',
        target: 'instafeed',
        sortBy: 'most-recent',
        limit: '100',
        resolution: 'low_resolution',
        //template: '<li><a href="{{link}}" target="_blank"><img src="{{image}}" alt=""></a></li>',
        template: '<div class="item"><div class="col-md-12"><div class="picture prelatife"><a href="{{link}}" target="_blank"><img src="{{image}}" width="300" alt=""></a></div></div></div>',
        after: function(){
              $('.block_feed.owl-theme').owlCarousel({
                  margin:0,
                  nav:false,
                  pagination: true,
                  items: 4,
                  loop: true,
                  autoPlay: true,
                  autoplayTimeout:1000,
                  autoplayHoverPause:true,
                  responsive:{
                      0:{
                          items:1
                      },
                      600:{
                          items:2
                      },
                      1120:{
                          items:3
                      },
                      1300:{
                          items:4
                      }
                  }
              });

            },
    });
  feed.run();
</script>
<script type="text/javascript">
  $(function() {
      $('.lazy').lazy();
  });
</script>
        <div class="block_feed row owl-carousel owl-theme" id="instafeed">
<!-- 1712530961.c7615b9.3740d2ddf82447e9bb2131bdf82bd049 -->
        </div>
        <div class="clear"></div>
      </div>

    <div class="clear height-50"></div>
    <div class="clear"></div>
  </div>
</section>
<!-- End featured section surgeons -->

<div class="clearfix clear"></div>
