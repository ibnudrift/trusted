<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="title_page">BLOGS</h1>
        <div class="clear"></div>
        <h3 class="tagline">Title of Article Blog</h3>
        <div class="clear"></div>
        <div class="row details_cont_articles">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">

                <img src="http://placehold.it/980x425" alt="">

                <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam repudiandae, illo ea inventore illum dolorem dolorum, accusantium tempora totam voluptatibus, fugiat quis! Iusto voluptatem, sit cum assumenda, in magnam sint.</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam repudiandae, illo ea inventore illum dolorem dolorum, accusantium tempora totam voluptatibus, fugiat quis! Iusto voluptatem, sit cum assumenda, in magnam sint.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam repudiandae, illo ea inventore illum dolorem dolorum, accusantium tempora totam voluptatibus, fugiat quis! Iusto voluptatem, sit cum assumenda, in magnam sint.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam repudiandae, illo ea inventore illum dolorem dolorum, accusantium tempora totam voluptatibus, fugiat quis! Iusto voluptatem, sit cum assumenda, in magnam sint.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam repudiandae, illo ea inventore illum dolorem dolorum, accusantium tempora totam voluptatibus, fugiat quis! Iusto voluptatem, sit cum assumenda, in magnam sint.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam repudiandae, illo ea inventore illum dolorem dolorum, accusantium tempora totam voluptatibus, fugiat quis! Iusto voluptatem, sit cum assumenda, in magnam sint.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam repudiandae, illo ea inventore illum dolorem dolorum, accusantium tempora totam voluptatibus, fugiat quis! Iusto voluptatem, sit cum assumenda, in magnam sint.</p>

                <div class="clear height-10"></div>
                <div class="shares-text text-left p_shares_article">
                    <span class="inline-t">SHARE</span>&nbsp; / &nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=#">FACEBOOK</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://plus.google.com/share?url=#">GOOGLE PLUS</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://twitter.com/home?status=#">TWITTER</a>
                </div>

                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">Other Blogs</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                  <li><a href="#">Lorem ipsum dolor sit.</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet.</a></li>
                  <li><a href="#">Lorem ipsum dolor sit.</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet.</a></li>
                  <li><a href="#">Lorem ipsum dolor sit.</a></li>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>
      
      <div class="clear height-20"></div>
      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>