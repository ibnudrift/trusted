<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['journey_step_3_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h2>Step 2</h2>
      <h3><?php echo $this->setting['journey_step_3_title'] ?></h3>
    </div>
  </div>
  <div class="clear"></div>
</div>

<section class="section_default back_cream mh710 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

    <div class="blocks_journey text-center step_1 step_3">
      <div class="tops">
        <h6 class="sub_title">START JOURNEY</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-10"></div>

        <!-- <h1 class="titlepage">Step 2</h1> -->
        <h1 class="c1"><?php echo $this->setting['journey_step_3_title'] ?></h1>
      </div>

      <div class="clear height-50"></div><div class="height-20"></div>

      <div class="middle prelatife">
        <div class="row default">
          <div class="col-md-8 col-lg-9">
            <div class="mw909 content-text">
              
              <h5><?php echo nl2br($this->setting['journey_step_3_subtitle']) ?></h5>
              <div class="landing_hero pict_full">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(909,1000, '/images/static/'.$this->setting['journey_step_3_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
              </div>
              <div class="clear"></div>
              
              <?php echo $this->setting['journey_step_3_content'] ?>
                
                <div class="list_category_step3">
                  <div class="row">
                    <div class="col-md-4 col-sm-4">
                      <div class="items">
                        <div class="pict">
                          <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'id'=>3)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(270,270, '/images/static/'.$this->setting['journey_step_3_menu_1_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a>
                        </div>
                        <div class="clear height-10"></div>
                        <div class="titles">
                          <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'id'=>3)); ?>"><?php echo $this->setting['journey_step_3_menu_1_title'] ?></a>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                      <div class="items">
                        <div class="pict">
                          <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'id'=>1)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(270,270, '/images/static/'.$this->setting['journey_step_3_menu_2_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a>
                        </div>
                        <div class="clear height-10"></div>
                        <div class="titles">
                          <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'id'=>1)); ?>"><?php echo $this->setting['journey_step_3_menu_2_title'] ?></a>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                      <div class="items">
                        <div class="pict">
                          <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'id'=>2)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(270,270, '/images/static/'.$this->setting['journey_step_3_menu_3_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a>
                        </div>
                        <div class="clear height-10"></div>
                        <div class="titles">
                          <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'id'=>2)); ?>"><?php echo $this->setting['journey_step_3_menu_3_title'] ?></a>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>

              <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>
          <div class="col-md-4 col-lg-3">
            <div class="blocks_right_stepmenu_inside">
              <?php echo $this->renderPartial('//layouts/_menu_step', array()); ?>
            </div>
          </div>
        </div>
        <div class="clear height-50"></div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>
    
    <div class="clear"></div>
  </div>

</section>
<?php
$criteria = new CDbCriteria;
$criteria->addCondition('ads_id = :ads_id');
$criteria->params[':ads_id'] = 2;
$criteria->order = 'sort';
// $criteria->group = 't.id';
$dataAds = AdsImage::model()->findAll($criteria);
?>
    <p>
    <?php foreach ($dataAds as $key => $value): ?>
      <a href="<?php echo $value->url ?>">
      <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(728,90, '/images/ads_image/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block margin-bottom-20">
      </a>
    <?php $value->view = $value->view + 1; $value->save(false);  ?>
    <?php endforeach ?>

    </p>
    <div class="height-30"></div>