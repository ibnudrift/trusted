<?php
$session = new CHttpSession;
$session->open();
?>
<style type="text/css">
  header.head{
    border-bottom: 4px solid #7ecdea;
  }
</style>
<!-- outers_page_static -->
<section class=" back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us surgeons_detail_page">
    <div class="prelatife container">
      <div class="clear height-50"></div>
      <div class="clear height-15"></div>
      <div class="pict_full illustration_picture overfl_h">
        <!-- <img src="" alt="" class="img-responsive"> -->
        <?php if ($dataDoctor->package > 0): ?>
        <div class="back_picture" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1600,530, '/images/doctor/'.$dataDoctor->cover , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
        <?php else: ?>
        <div class="back_picture" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1600,530, '/images/doctor/picts_detail_psurgeons.jpg' , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
        <?php endif ?>
          <div class="logo_picDoctor">
            <?php if ($dataDoctor->logo != '' AND $dataDoctor->package > 0): ?>
            <div class="box_logo">
              <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(150,150, '/images/doctor/'.$dataDoctor->logo , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
            </div>
            <?php endif ?>
            <div class="clear height-20"></div>
            <div class="desc_info text-center">
              <h3 class="sub_title"><?php echo (strtoupper($dataDoctor->name)) ?></h3>
              <div class="clear"></div>
              <h6 class="cat_dn hidden-xs"><?php echo $dataDoctor->certification ?></h6>
            </div>
            <div class="surgeons-cover-button">
              <?php if ($dataDoctor->claimed == 0): ?>
                <?php if ($session['login_member'] == null): ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('claim', 'id'=>$dataDoctor->id)); ?>" class="cwbutton_detailsur back_purple_defaults_tl d-inline">CLAIM THIS CLINIC</a>
                  <div class="clear height-10"></div>
                <?php else: ?>
                  <?php
                  $dataClaim = DoctorClaim::model()->find('member_id = :member_id', array(':member_id'=>$session['login_member']['id']));
                  ?>
                  <?php if ($dataClaim === null AND $session['login_member']['type'] == 1): ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('claim', 'id'=>$dataDoctor->id)); ?>" class="cwbutton_detailsur back_purple_defaults_tl d-inline">CLAIM THIS CLINIC</a>
                  <div class="clear height-10"></div>
                  <?php else: ?>
                  <div class="clear height-10"></div>
                  <div class="clear height-10"></div>
                  <div class="clear height-10"></div>
                  <div class="clear height-10"></div>
                  <div class="clear height-10"></div>
                  <?php endif ?>
                <?php endif ?>
              <?php else: ?>
              <?php /*
                <a href="#<?php //echo CHtml::normalizeUrl(array('/contact/index', 'id'=>$dataDoctor->id)); ?>" class="cwbutton_detailsur back_purple_defaults_tl d-inline contact_clinic_now">CONTACT CLINIC</a>
                <div class="clear height-10"></div>
                */ ?>
              <?php endif ?>
                <a href="<?php echo CHtml::normalizeUrl(array('/coaching/index')); ?>" class="cwbutton_detailsur back_purple_defaults_tl d-inline hide hidden">CONSULT TRUSTED SURGEON</a>
                <div class="clear height-10"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="blocks_top_detail_surgeons back-white h80 prelatife">
          <div class="pos_tops">
            <div class="row prelatife">
              <div class="col-md-9">
                <div class="picture_doctor d-inline v-top">
                <?php if ($dataDoctor->package > 0): ?>
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/'.$dataDoctor->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                <?php else: ?>
                  <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/logo-trusted.png' , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                <?php endif ?>
                </div>
                <div class="d-inline v-top padding-left-20 right_conts">
                  <div class="clear height-10"></div>
                  <div class="titles">
                    <?php echo (strtoupper($dataDoctor->name)) ?> <br>
                    <span class="certification_dct"><?php echo $dataDoctor->certification ?></span></div>
                    <div class="clear"></div>
                    <div class="descs">
<?php
$suburbGroup = array();
foreach ($dataDoctor->clinics as $key => $value){
  $suburbGroup[] = $value->suburb;
}
?>
                      <span><?php echo implode(', ', $suburbGroup) ?></span>
                      <div class="clear"></div>
                      <p><?php echo $dataDoctor->clinics[0]->address_1 ?> <?php echo $dataDoctor->clinics[0]->address_2 ?> <?php echo $dataDoctor->clinics[0]->suburb ?> <?php echo $dataDoctor->clinics[0]->state ?></p>
                      <div class="clear"></div>
                      <div class="blocks_social_surgeons">
                        <ul class="list-inline">
                          <?php if ($dataDoctor->clinics[0]->phone != ''): ?>
                          <li><i class="fa fa-phone"></i> &nbsp;<?php echo $dataDoctor->clinics[0]->phone ?></li>
                          <?php endif ?>
                          <?php if ($dataDoctor->clinics[0]->fax != ''): ?>
                          <li><i class="fa fa-fax"></i> &nbsp;<?php echo $dataDoctor->clinics[0]->fax ?></li>
                          <?php endif ?>
                          <?php if ($dataDoctor->clinics[0]->website != ''): ?>
<?php
if (!preg_match("~^(?:f|ht)tps?://~i", $dataDoctor->clinics[0]->website)) {
    $url = "http://" . $dataDoctor->clinics[0]->website;
}
?>
                          <li><a target="_blank" href="<?php echo $url ?>"><i class="fa fa-link"></i> &nbsp;<?php echo $dataDoctor->clinics[0]->website ?></a></li>
                          <?php endif ?>
                          <?php /*if ($dataDoctor->email != ''): ?>
                          <li><a href="emailto:<?php echo $dataDoctor->email ?>" target="_blank"><i class="fa fa-envelope"></i> &nbsp;<?php echo $dataDoctor->email ?></a></li>
                          <?php endif*/ ?>
                        </ul>
                      </div>
                      <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
              </div>
              <div class="col-md-3">
                <div class="clear height-50 hidden-xs hidden-sm"></div><div class="height-20 hidden-xs hidden-sm"></div>
                <div class="blocks_social_surgeons rights_cSoc text-right">
                  <ul class="list-inline text-right">
                    <?php if ($dataDoctor->package > 0): ?>
                    <?php if ($dataDoctor->social_facebook != ''): ?>
                    <li><a href="<?php echo $dataDoctor->social_facebook ?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                    <?php endif ?>
                    <?php if ($dataDoctor->social_instagram != ''): ?>
                    <li><a href="<?php echo $dataDoctor->social_instagram ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <?php endif ?>
                    <?php if ($dataDoctor->social_twitter != ''): ?>
                    <li><a href="<?php echo $dataDoctor->social_twitter ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <?php endif ?>
                    <?php if ($dataDoctor->social_youtube!= ''): ?>
                    <li><a href="<?php echo $dataDoctor->social_youtube?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
                    <?php endif ?>
                    <?php if ($dataDoctor->social_pinterest != ''): ?>
                    <li><a href="<?php echo $dataDoctor->social_pinterest ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    <?php endif ?>
                    <?php if ($dataDoctor->social_linkedin != ''): ?>
                    <li><a href="<?php echo $dataDoctor->social_linkedin ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <?php endif ?>
                    <?php endif ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>

        <div class="clear"></div>
      </div>
    </div>
    <!-- end block white top -->


    <div class="prelatife container">
      <div class="clear height-20"></div>
      <div class="content-text insides_static">
        <div class="row">
          <div class="col-md-9 order-default padding-right-20 ">
            <!-- Start content surgeons -->
        <div class="insides_c_content_surgeons s_detail">
<?php
$criteria = new CDbCriteria;
$criteria->with = array(
  'detail'=>array(
    'select'=>false,
  )
);
$criteria->addCondition('t.status = 1');
$criteria->addCondition('t.doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $dataDoctor->id;

if ($_GET['review_order'] == 'featured') {
  $criteria->order = 'featured DESC, date_input DESC';
} elseif($_GET['review_order'] == 'higest') {
  $criteria->order = '(SUM(detail.value)/COUNT(detail.id)) DESC';
} elseif($_GET['review_order'] == 'lowest') {
  $criteria->order = '(SUM(detail.value)/COUNT(detail.id)) ASC';
} elseif($_GET['review_order'] == 'recent') {
  $criteria->order = 'date_input DESC';
} else {
  $criteria->order = 'date_input DESC';
}

$criteria->group = 't.id';

$dataReview = new CActiveDataProvider('DoctorReview', array(
  'criteria'=>$criteria,
    'pagination'=>array(
        'pageSize'=>10,
    ),
));
?>
<?php
$criteria = new CDbCriteria;
$criteria->select = 'SUM(`value`)/COUNT(`value`) as `value`';
$criteria->addCondition('doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $dataDoctor->id;
$criteria->group = 'doctor_id';
$dataReviewValue = DoctorCategoryReview::model()->find($criteria);

?>
<?php
$jmlTab = 7;
if (trim($dataDoctor->about) == '') {
  $jmlTab--;
}
if ($dataDoctor->member_id == 0) {
  $jmlTab--;
}
?>
            <div class="sBox_surgeons_details no_spec_<?php echo $jmlTab ?>">
              <div>
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="tab_menu_doctor active" id="about_menu"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">Profile</a></li>
                  <li role="presentation" class="tab_menu_doctor <?php if (trim($dataDoctor->about) == ''): ?>active<?php endif ?>" id="reviews_menu"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Reviews</a></li>
                  <?php /*
                  <?php if ($dataDoctor->member_id != 0): ?>
                  <li role="presentation" class="tab_menu_doctor" id="special_menu"><a href="#special" aria-controls="special" role="tab" data-toggle="tab">Specialization</a></li>
                  <?php endif ?>
                  */ ?>
                  <li role="presentation" class="tab_menu_doctor" id="photos_menu"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">Photos</a></li>
                  <li role="presentation" class="tab_menu_doctor" id="videos_menu"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Videos</a></li>
                  <?php /* ?><li role="presentation" class="tab_menu_doctor" id="blogs_menu"><a href="#blogs" aria-controls="blogs" role="tab" data-toggle="tab">BLOGS</a></li><?php */ ?>
                  <li role="presentation" class="tab_menu_doctor" id="maps_menu"><a href="#maps" aria-controls="maps" id="#map_click" role="tab" data-toggle="tab">Clinic</a></li>
                </ul>

                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane abouts_descrip active" id="about">
                    <div class="row">
                      <?php if ($dataDoctor->package > 0): ?>
                      <?php if (trim($dataDoctor->about) != ''): ?>
                      <div class="col-md-6">
                        <h5 class="sub_title">About</h5>
                          <?php echo $dataDoctor->about ?>
                      </div>
                      <?php endif ?>
                      <?php endif ?>
                      <div class="col-md-6">
                        <div class="padding-left-30 clear clearfix margin-bottom-30">
                          <h5 class="sub_title">Specialities</h5>
                          <div class="treatment-area">    
                            <?php
                            $dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
                            $dataSpec = array();
                            foreach ($dataDoctor->spec as $v) {
                              $dataSpec[$v->specialitation_id] = 1;
                            }
                            ?>
                                  <?php foreach ($dataCategory as $va): ?>
                                  <?php
                                  $statusSpec = 0;
                                  if (is_array($va['children']) && count($va['children']) > 0) {
                                    foreach ($va['children'] as $ke => $val) {
                                      if (array_key_exists($val['id'], $dataSpec)) {
                                        $statusSpec = 1;
                                      }
                                    }
                                  }
                                  if ($statusSpec == 1) {
                                  ?>
                                  <?php switch ($va['title']) {
                                    case 'Face':
                                      ?>
                                      <div class="body-area face">
                                        <div class="icon"></div>
                                        Face
                                      </div>
                                      <?php
                                      break;
                                    
                                    case 'Breast':
                                      ?>
                                      <div class="body-area breasts">
                                        <div class="icon"></div>
                                        Breasts
                                      </div>
                                      <?php
                                      break;
                                    
                                    case 'Body':
                                      ?>
                                      <div class="body-area body">
                                        <div class="icon"></div>
                                        Body
                                      </div>
                                      <?php
                                      break;
                                    
                                    case 'Intimates':
                                      ?>
                                      <div class="body-area other">
                                        <div class="icon"></div>
                                        Intimates
                                      </div>
                                      <?php
                                      break;
                                    
                                    default:

                                      break;
                                  }
                                  } ?>
                                        <!-- <div class="clear height-10"></div> -->
                                  <?php endforeach ?>
                                      <div class="clear"></div>
                                  </div>
                        </div>
                        <?php if (trim($dataDoctor->qualification) != ''): ?>
                        <div class="padding-left-30 clear clearfix margin-bottom-30">
                          <h5 class="sub_title">Qualifications</h5>
<?php
$qualification = explode('<br />', nl2br(trim($dataDoctor->qualification)));
?>
                          <ul>
                            <?php foreach ($qualification as $key => $value): ?>
                            <li><?php echo $value ?></li>
                            <?php endforeach ?>
                          </ul>
                        </div>
                        <?php endif ?>
                        <?php if (trim($dataDoctor->profesional) != ''): ?>
                        <div class="padding-left-30 clear clearfix margin-bottom-30">
                          <h5 class="sub_title">Professionals membership & Registrations</h5>
<?php
$profesional = explode('<br />', nl2br(trim($dataDoctor->profesional)));
?>
                          <ul>
                            <?php foreach ($profesional as $key => $value): ?>
                            <li><?php echo $value ?></li>
                            <?php endforeach ?>
                          </ul>
                        </div>
                        <?php endif ?>
                      </div>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane <?php if (trim($dataDoctor->about) == ''): ?>active<?php endif ?>" id="reviews">
                      <div class="blocks_review_surgeonsDetails">

                        <h4>Reviews</h4>
                        <?php if(Yii::app()->user->hasFlash('success')): ?>
                          <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
                          <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css">
                          <script type="text/javascript">
                                  sweetAlert("Success!", "<?php echo Yii::app()->user->getFlash('success') ?>", "success");
                          </script>
                        <?php endif; ?>
                        <div class="row">
<?php
$dataCategoryReview = DoctorReviewCategory::model()->findAll();
?>
                          <?php if ($dataReview->getTotalItemCount() > 0): ?>
                            
                          <div class="col-md-5">
                            <h5>Ratings based on <?php echo $dataReview->getTotalItemCount(); ?> reviews</h5>
                            <div class="ratings_top">

<?php foreach ($dataCategoryReview as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->select = 'SUM(`value`)/COUNT(`value`) as `value`';
$criteria->addCondition('doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $dataDoctor->id;
$criteria->addCondition('review_category_id = :review_category_id');
$criteria->params[':review_category_id'] = $value->id;
$criteria->group = 'doctor_id';
$dataReviewValue = DoctorCategoryReview::model()->find($criteria);
?>
                              <div class="ls">
                                <?php echo DoctorReviewCategory::model()->createStar($dataReviewValue->value) ?> &nbsp;
                                <?php echo $value->name ?>
                              </div>
<?php endforeach ?>
                              <div class="clear"></div>
                            </div>
                            <div class="clear height-20"></div>
                          </div>
                          <?php endif ?>
                          <div class="col-md-7">
                            <h5>Write a review</h5>
                            <p>Are you a patient of <?php echo $dataDoctor->name ?>?</p>

                            <a class="back_purple_defaults_tl d-inline" role="button" data-toggle="collapse" href="#collapse_reviews" aria-expanded="false" aria-controls="collapse_reviews">WRITE A REVIEW</a>
                            <!-- collapse review -->
                             <div class="collapse" id="collapse_reviews">
                                <div class="clear height-15"></div>
                                <div class="well sets_form_conReview">
<?php if ($session['login_member']): ?>
<?php
$modelReview->name = $session['login_member']['first_name'];
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
<?php echo CHtml::errorSummary($modelReview, '', '', array('class'=>'alert alert-danger')); ?>
<?php if ($modelReview->hasErrors()): ?>
<script type="text/javascript">
$(document).ready(function() {

$('.tab-content .tab-pane').removeClass('active');
$('.tab_menu_doctor').removeClass('active');
$('#reviews').addClass('active');
$('#reviews_menu').addClass('active');
$('#collapse_reviews').collapse('show');
})
</script>
<?php endif ?>
<?php /*
                                    <div class="row default">
                                      <div class="col-sm-6">
                                        <div class="form-group">
                                          <label for="">Name</label>
                                          <?php echo $form->textField($modelReview, 'name', array('class'=>'form-control')) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group">
                                          <label for="">Address</label>
                                          <?php echo $form->textField($modelReview, 'address', array('class'=>'form-control')) ?>
                                        </div>
                                      </div>
                                    </div>
*/ ?>
                                    <div class="clear height-15"></div>
                                    <div class="lines-grey h2"></div>
                                    <div class="clear height-5"></div>

                                    <div class="row default">
                                      <div class="col-sm-12">
                                        <div class="form-group">
                                          <label><b>Category Review</b></label>
                                        </div>
                                      </div>
                                    </div>
<?php
$dataCategoryReview = array_chunk($dataCategoryReview, 2);
?>
                                    <?php foreach ($dataCategoryReview as $key => $value): ?>
                                    <div class="row default">
                                      <?php foreach ($value as $k => $v): ?>
                                      <div class="col-sm-6">
                                        <div class="form-group">
                                          <label for=""><?php echo $v->name ?></label>
                                          <div class="clear"></div>
                                          <fieldset class="rating">
                                              <input type="radio" id="star-<?php echo $v->id ?>-5" name="Rating[<?php echo $v->id ?>]" value="5" checked="checked" /><label class = "full" for="star-<?php echo $v->id ?>-5" title="Awesome - 5 stars"></label>
                                              <input type="radio" id="star-<?php echo $v->id ?>-4" name="Rating[<?php echo $v->id ?>]" value="4" /><label class = "full" for="star-<?php echo $v->id ?>-4" title="Pretty good - 4 stars"></label>
                                              <input type="radio" id="star-<?php echo $v->id ?>-3" name="Rating[<?php echo $v->id ?>]" value="3" /><label class = "full" for="star-<?php echo $v->id ?>-3" title="Meh - 3 stars"></label>
                                              <input type="radio" id="star-<?php echo $v->id ?>-2" name="Rating[<?php echo $v->id ?>]" value="2" /><label class = "full" for="star-<?php echo $v->id ?>-2" title="Kinda bad - 2 stars"></label>
                                              <input type="radio" id="star-<?php echo $v->id ?>-1" name="Rating[<?php echo $v->id ?>]" value="1" /><label class = "full" for="star-<?php echo $v->id ?>-1" title="Sucks big time - 1 star"></label>
                                          </fieldset>
                                        </div>
                                      </div>
                                      <?php endforeach ?>
                                    </div>
                                    <?php endforeach ?>

                                    <div class="clear"></div>
                                    <div class="form-group">
                                      <label for="">Message</label>
                                      <?php echo $form->textArea($modelReview, 'comment', array('class'=>'form-control', 'rows'=>4, 'placeholder'=>'We appreciate honest and trustworthy feedback')) ?>
                                    </div>

                                    <button type="submit" class="btn btn-default">Submit</button>
                                    <button type="button" class="btn btn-default reset_reviews_can">Cancel</button>
<?php $this->endWidget(); ?>
<?php else: ?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'action'=>array('/member/index', 'ret'=>urlencode(CHtml::normalizeUrl(array('/surgeons/detail', 'slug'=>$dataDoctor->slug))) ),
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
                                    <div class="row default">
                                      <div class="col-sm-6">
                                        <div class="form-group">
                                          <label for="">Email</label>
                                          <?php echo $form->textField($modelLogin, 'email', array('class'=>'form-control')) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group">
                                          <label for="">Password</label>
                                          <?php echo $form->passwordField($modelLogin, 'pass', array('class'=>'form-control')) ?>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="height-20"></div>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                    <button type="button" class="btn btn-default reset_reviews_can">Cancel</button>
                                    <div class="height-20"></div>
                                    <a href="<?php echo CHtml::normalizeUrl(array('/member/forgot')); ?>">Forgot Password</a>
                                    |
                                    <a href="<?php echo CHtml::normalizeUrl(array('/member/signup', 'ret'=>urlencode(CHtml::normalizeUrl(array('/surgeons/detail', 'slug'=>$dataDoctor->slug))))); ?>">Don't have an account? Sign Up</a> 

<?php $this->endWidget(); ?>
<?php endif ?>

                                  <div class="clear"></div>
                                </div>
                              </div>
                            <!-- end collapse review -->

                            <div class="clear height-20"></div>


<?php if ($dataReview->getTotalItemCount() > 0): ?>
                            <h5>Reviews (<?php echo $dataReview->getTotalItemCount(); ?>)</h5>
                            <div class="filters">
                              <p>
                                Filter by: &nbsp;
                                 <a <?php if ($_GET['review_order'] == 'featured'): ?>class="active"<?php endif ?> href="<?php echo CHtml::normalizeUrl(array('detail', 'slug'=>$dataDoctor->slug, 'review_order'=>'featured')); ?>#reviews">Featured</a>
                                 &nbsp;|&nbsp;
                                 <a <?php if ($_GET['review_order'] == 'higest'): ?>class="active"<?php endif ?> href="<?php echo CHtml::normalizeUrl(array('detail', 'slug'=>$dataDoctor->slug, 'review_order'=>'higest')); ?>#reviews">Highest Rating</a>
                                 &nbsp;|&nbsp;
                                 <a <?php if ($_GET['review_order'] == 'lowest'): ?>class="active"<?php endif ?> href="<?php echo CHtml::normalizeUrl(array('detail', 'slug'=>$dataDoctor->slug, 'review_order'=>'lowest')); ?>#reviews">Lowest Rating</a>
                                 &nbsp;|&nbsp;
                                 <a <?php if ($_GET['review_order'] == 'recent' OR $_GET['review_order'] == ''): ?>class="active"<?php endif ?> href="<?php echo CHtml::normalizeUrl(array('detail', 'slug'=>$dataDoctor->slug, 'review_order'=>'recent')); ?>#reviews">Recent</a>
                              </p>
                            </div>
                            <div class="clear height-20"></div>
                            <div class="listing_default_reviews">
                              <?php foreach ($dataReview->getData() as $key => $value): ?>
                              <div class="items">
<?php
$criteria = new CDbCriteria;
$criteria->select = 'SUM(`value`)/COUNT(`value`) as `value`';
$criteria->addCondition('doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $dataDoctor->id;
$criteria->addCondition('review_id = :review_id');
$criteria->params[':review_id'] = $value->id;
$criteria->group = 'review_id';
$dataReviewValue = DoctorCategoryReview::model()->find($criteria);

?>
                                <span class="top_nstar">
                                  <?php echo DoctorReviewCategory::model()->createStar($dataReviewValue->value) ?> &nbsp;  <?php echo $value->name ?>
                                </span>
                                <div class="clear height-5"></div>
                                <span class="titles_nm"> <?php echo $value->name ?>.  <?php echo $value->address ?></span>
                                <div class="clear height-5"></div>
                                <span class="monts"><?php echo Common::time_elapsed_string($value->date_input); ?></span>
                                <div class="clear height-5"></div>
                                <p><?php echo nl2br($value->comment) ?></p>
                              </div>
                              <?php endforeach ?>
                            </div>
                            <!-- end list reviews user -->
                            <?php $this->widget('MyLinkPager', array(
                                'pages' => $dataReview->getPagination(),
                                'header' => '',
                                'linkHash' => 'reviews',
                                'htmlOptions' => array('class' => 'pagination'),
                            )) ?>
<?php endif ?>
                            <div class="clear"></div>
                          </div>
                        </div>

                        <div class="clear"></div>
                      </div>
                      
                  </div>
                  <?php if ($dataDoctor->member_id != 0): ?>
                  <div role="tabpanel" class="tab-pane" id="special">
                      <h4>Specialization</h4>
<?php
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
$dataSpec = array();
foreach ($dataDoctor->spec as $key => $value) {
  $dataSpec[$value->specialitation_id] = 1;
}
?>
                      <?php foreach ($dataCategory as $key => $value): ?>
                      <?php
                      $statusSpec = 0;
                      if (is_array($value['children']) && count($value['children']) > 0) {
                        foreach ($value['children'] as $k => $v) {
                          if (array_key_exists($v['id'], $dataSpec)) {
                            $statusSpec = 1;
                          }
                        }
                      }
                      ?>
                      <?php if ($statusSpec == 1): ?>
                        
                        <h4><?php echo $value['title'] ?></h4>
                        <ul>
                          <?php foreach ($value['children'] as $k => $v): ?>
                            <?php if (array_key_exists($v['id'], $dataSpec)): ?>
                            <li><?php echo $v['title'] ?></li>
                            <?php endif ?>
                          <?php endforeach ?>
                        </ul>
                      <?php endif ?>
                      <?php endforeach ?>
                      <?php /*
                      <h4>Other</h4>
                      <ul>
                        <?php foreach ($dataDoctor->spec as $key => $value): ?>
                        <li><?php echo PrdCategory::model()->with('description')->find('t.id = :id AND description.language_id = :language_id', array(':id'=>$value->specialitation_id, ':language_id'=>$this->languageID))->description->name ?></li>
                        <?php endforeach ?>
                      </ul>
                      */ ?>
                  </div>
                  <?php endif ?>
                  <div role="tabpanel" class="tab-pane" id="photos">
                      <h4>Photos</h4>
                      <div class="clear"></div>
                      <div class="list_gallery_default">
<?php
$criteria = new CDbCriteria;
$criteria->addCondition('doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $dataDoctor->id;
$dataPhoto = new CActiveDataProvider('DoctorPhoto', array(
  'criteria'=>$criteria,
));
$dataPhotos = $dataPhoto->getData();
?>
                        <?php if (count($dataPhotos) > 0 AND $dataDoctor->package > 0): ?>
                          
                        <div class="row">

                          <?php foreach ($dataPhotos as $key => $value): ?>
                            
                          <div class="col-md-3">
                            <div class="items">
                              <div class="pict">
                                <a href="#">
                                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(210,210, '/images/doctor_photo/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                                </a>
                              </div>
                              <div class="clear height-15"></div>
                              <a href="#"><span class="titles"><?php echo $value->title ?></span></a>
                              <div class="clear"></div>
                            </div>
                          </div>
                          <?php endforeach ?>
                        </div>
                        <?php else: ?>
                          <h3>No Photos</h3>
                        <?php endif ?>
                      </div>
                      <!-- end list gallery -->

                  </div>
                  <div role="tabpanel" class="tab-pane" id="videos">
                      <h4>Videos</h4>

                      <?php if (count($dataDoctor->videos) > 0 AND $dataDoctor->package > 0): ?>
                      <div class="list_gallery_default video">
                        <div class="row default">
                          <?php foreach ($dataDoctor->videos as $key => $value): ?>
                          <div class="col-md-4">
                            <div class="items">
                              <div class="pict">
                                <a href="#">
                                  <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'>
                                  <iframe src='https://www.youtube.com/embed/<?php echo $value->code ?>' frameborder='0' allowfullscreen></iframe></div>
                                </a>
                              </div>
                              <div class="clear height-15"></div>
                              <a href="#"><span class="titles"><?php echo $value->title ?></span></a>
                              <div class="clear"></div>
                            </div>
                          </div>
                          <?php endforeach ?>
                        </div>
                      </div>
                      <!-- end list gallery -->
                      <?php else: ?>
                      <h3>No Videos</h3>
                      <?php endif ?>
                  </div>
                  <?php /*
                  <div role="tabpanel" class="tab-pane" id="blogs">
                      <h4>Blogs</h4>
                      <div class="list_blog_default">
                        <?php
                        $criteria = new CDbCriteria;
                        $criteria->addCondition('doctor_id = :doctor_id');
                        $criteria->params[':doctor_id'] = $dataDoctor->id;
                        $dataBlogs = new CActiveDataProvider('DoctorBlog', array(
                          'criteria'=>$criteria,
                        ));
                        ?>
                        <?php if ($dataBlogs->getTotalItemCount() > 0): ?>
                        <div class="row">

                          <?php foreach ($dataBlogs->getData() as $key => $value): ?>
                          <div class="col-md-12">
                            <div class="items">
                              <div class="row default">
                                <div class="col-md-3">
                                  <div class="pict">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>">
                                    <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(350,210, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                                    </a>
                                  </div>
                                </div>  
                                <div class="col-md-9">
                                  <div class="description">
                                    <span class="titles"><?php echo $value->title ?></span>
                                    <div class="clear height-15"></div>
                                    <p><?php echo substr(strip_tags($value->content), 0, 200) ?>....</p>
                                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>" class="link_more">read more</a>
                                    <div class="clear"></div>
                                  </div>
                                </div>  
                              </div>

                              <div class="clear"></div>
                            </div>
                          </div>
                          <?php endforeach ?>
                        </div>
                        <?php else: ?>
                        <h3>No data blog</h3>
                        <?php endif ?>
                      </div>
                      <!-- end list gallery -->
                  </div>*/ ?>
                  <div role="tabpanel" class="tab-pane" id="maps">
                      <h4>Maps</h4>
                      <div class="row default">
<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnVYV9PU2hDS4GMEJ_TZ2Hy-zy1iXfQX0"></script>
                        <?php foreach ($dataDoctor->clinics as $key => $value): ?>
                        <div class="col-md-6">
<div style="overflow:hidden;height:400px;width:100%;">
  <div id="gmap_canvas<?php echo $key ?>" style="height:400px;width:100%;">
    <style>#gmap_canvas<?php echo $key ?> img{max-width:none!important;background:none!important}</style>
  </div>
</div>
<div class="height-20"></div>
<?php
if (!preg_match("~^(?:f|ht)tps?://~i", $value->website)) {
    $url = "http://" . $value->website;
}
?>

<script type="text/javascript"> 
function initialize<?php echo $key ?>() { 
    var myOptions = {
        zoom: 14,
        center: new google.maps.LatLng(<?php echo $value->latitude ?>, <?php echo $value->longitude ?>),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("gmap_canvas<?php echo $key ?>"), myOptions);
    marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(<?php echo $value->latitude ?>, <?php echo $value->longitude ?>)
    });
    infowindow = new google.maps.InfoWindow({
        content: '<p><?php if ($value->address_1 != ''): ?><?php echo $value->address_1 ?> <br><?php endif ?><?php if ($value->address_2 != ''): ?><?php echo $value->address_2 ?> <br><?php endif ?><?php if ($value->suburb != ''): ?><?php echo $value->suburb ?> <br><?php endif ?><?php if ($value->phone != ''): ?>P: <?php echo $value->phone ?> <br><?php endif ?><?php if ($value->fax != ''): ?>F: <?php echo $value->fax ?> <br><?php endif ?><?php if ($value->website != ''): ?><a href="<?php echo $url ?>" target="_blank"><?php echo $value->website ?></a><?php endif ?></p>'
    });
    google.maps.event.addListener(marker, "click", function() {
        infowindow.open(map, marker);
    });
    infowindow.open(map, marker);
}
// initialize<?php echo $key ?>();
$(window).load(function() {
  initialize<?php echo $key ?>();
})
// $(function() {
//   initialize<?php echo $key ?>();
// })
// $('#map_click').mouseup(function() {
//   initialize<?php echo $key ?>();
// })
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  initialize<?php echo $key ?>();
})
</script>
                        </div>
                        <?php endforeach ?>
                        <div class="col-md-6">
                          <div class="maps_info">
                            <?php foreach ($dataDoctor->clinics as $key => $value): ?>
                            
                              
                              
                              
                              
                              
                              
                              
                              
                            <?php endforeach ?>
                            <div class="clear"></div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>

              </div>

              <div class="clear"></div>
            </div>
            <!-- End detail surgeons -->

          <div class="clear"></div>
        </div>
        <!-- End content surgeons -->
          </div>
          <div class="col-md-3 order-default">
            <div class="padding-left-30">
              <!-- Start right Enquiry -->
              <div class="box_rgh_enquiryBlue">
                <div class="top">
                  <h6 class="text-center">MAKE AN ENQUIRY</h6>
                </div>
                <div class="middles prelatife">
                    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                        // 'type'=>'',
                        'enableAjaxValidation'=>false,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data',
                            'class' => 'enquire-formn'
                        ),
                    )); ?>
                    <?php echo $form->errorSummary($modelEnquire, '', '', array('class'=>'alert alert-danger')); ?>
                    <?php if (($flashMessage = Yii::app()->user->getFlash('success3')) != ''): ?>
                    <div class="alert alert-success">
                      <p><?php echo $flashMessage ?></p>
                    </div>
                    <?php endif ?>
                    <div class="form-group">
                      <?php echo $form->textField($modelEnquire, 'name', array('class'=>'form-control', 'placeholder'=>'Name', 'required'=>'required')); ?>
                    </div>
                    <div class="form-group">
                      <?php echo $form->textField($modelEnquire, 'email', array('class'=>'form-control', 'placeholder'=>'Email', 'required'=>'required')); ?>
                    </div>
                    <div class="form-group">
                      <?php echo $form->textField($modelEnquire, 'phone', array('class'=>'form-control', 'placeholder'=>'Phone', 'required'=>'required')); ?>
                    </div>
                    <div class="form-group">
                      <?php echo $form->textField($modelEnquire, 'type', array('class'=>'form-control', 'placeholder'=>'Type of Enquiry')); ?>
                    </div>
                    <div class="form-group">
                      <?php echo $form->textArea($modelEnquire, 'body', array('class'=>'form-control', 'placeholder'=>'Message', 'rows'=>4, 'required'=>'required')); ?>
                    </div>
                    <div class="form-group">
                      <div id="recaptcha2" class="g-recaptcha"></div>
                    </div>

                    <button type="button" class="btn btn-default btns_submt_frm">SEND</button>
                    <?php $this->endWidget(); ?>
                  <div class="clear"></div>
                </div>
              </div>
              <!-- End right Enquiry -->
              <div class="clear height-15"></div>

              <div class="clear"></div>
            </div>
          </div>
        </div>
        
        

        <div class="clear"></div>
      </div>

      <div class="clear height-20"></div> 
    </div>

    <div class="clear"></div>
  </div>
</section>
<script type="text/javascript">
if (window.location.hash) {
  $('.tab-content .tab-pane').removeClass('active');
  $('.tab_menu_doctor').removeClass('active');

  $(window.location.hash).addClass('active');
  $(window.location.hash+'_menu').addClass('active');
  // $('#collapse_reviews').collapse('show');
};
</script>

<!-- Modal -->
<div class="modal fade" id="Claim_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php if (Yii::app()->user->hasFlash('danger2')): ?>
        <h4 class="modal-title" id="myModalLabel">Alert Danger</h4>
        <?php else: ?>
        <h4 class="modal-title" id="myModalLabel">Request Consultation with Doctor</h4>
        <?php endif ?>
      </div>
      <div class="modal-body">
        <div class="bs_form_claim">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
<?php if (($flashMessage = Yii::app()->user->getFlash('success2')) != ''): ?>
<div class="alert alert-success">
  <p><?php echo $flashMessage ?></p>
</div>
<?php elseif (($flashMessage = Yii::app()->user->getFlash('danger2')) != ''): ?>
<div class="alert alert-danger">
  <p><?php echo $flashMessage ?></p>
</div>
<?php else: ?>
<?php echo CHtml::errorSummary($modelMessage, '', '', array('class'=>'alert alert-danger')); ?>
            <div class="row default">
              <div class="col-sm-6">
                <div class="form-group">
                  <?php echo $form->textField($modelMessage, 'first_name', array('class'=>'form-control', 'placeholder'=>'FIRST NAME *')) ?>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <?php echo $form->textField($modelMessage, 'last_name', array('class'=>'form-control', 'placeholder'=>'LAST NAME')) ?>
                </div>
              </div>
            </div>
             <div class="row default">
              <div class="col-sm-6">
                <div class="form-group">
                  <?php echo $form->textField($modelMessage, 'email', array('class'=>'form-control', 'placeholder'=>'EMAIL')) ?>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <?php echo $form->textField($modelMessage, 'telp', array('class'=>'form-control', 'placeholder'=>'TEL')) ?>
                </div>
              </div>
            </div>
             <div class="row default">
              <div class="col-sm-12">
                <div class="form-group">
                  <?php echo $form->textArea($modelMessage, 'message', array('class'=>'form-control', 'placeholder'=>'MESSAGE *', 'cols'=>4)) ?>
                </div>
              </div>
            </div>
            <div class="row default">
              <div class="col-sm-12">
                <div class="fright">
                  <button type="submit" class="back_purple_defaults_tl pt0">SEND</button>
                </div>
              </div>
            </div>
          <?php endif ?>
          <?php $this->endWidget(); ?>
          <div class="clear"></div>
        </div>

        <div class="clear"></div>
      </div>

    </div>
  </div>
</div>
<div class="container">
  <?php if ($dataDoctor->package > 0): ?>
      <h4>Share this page:</h4>
      <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58f5e4af3ee8fe7e"></script> 
      <div class="addthis_inline_share_toolbox"></div>
  <?php endif ?>
      <div class="height-30"></div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.contact_clinic_now').click(function() {
      $('#Claim_modal').modal();
      return false;
    })
    <?php if ($modelMessage->hasErrors() OR $flashMessage != ''): ?>
          $('#Claim_modal').modal();
    <?php endif ?>
  })
</script>