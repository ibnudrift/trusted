<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      <div class="pict_full illustration_picture"><img src="<?php echo $this->assetBaseurl ?>pict_top_contact.jpg" alt="" class="img-responsive"></div>
      <div class="clear height-50"></div>

      <div class="content-text insides_static page_outers_cart_content">
        <h1 class="title_page">CHECK OUT</h1>
        <div class="clear height-40"></div><div class="height-5"></div>
        <div class="clear"></div>
        
        <!-- start block signup cart -->
        <div class="box_checkoutCart_tab_cont">
          <div>
            <ul class="nav nav-tabs" role="tablist">
              <li role="" class="active"><a href="#retur_customer" aria-controls="retur_customer">FOR RETURNING CUSTOMER</a></li>
              <li role=""><a href="#fr_guest" aria-controls="fr_guest">FOR GUEST</a></li>
            </ul>
            
            <div class="blocks_middle_checkoutsign">
              <div class="row">
                <div class="col-md-6">
                  <div class="panel_box_checkout active">
                    <div class="padding">
                      <div class="mw400 tengah text-left">
                        <h4>Sign in to your account</h4>
                        <div class="clear"></div>
                        <p>If you have an account, please sign in for faster checkout</p>

                        <div class="clear height-25"></div>
                        <div class="box_forms_login">
                          <form action="#" method="post">
                            <div class="form-group">
                              <label for="" class="control-label">Email</label>
                              <div class="clear"></div>
                              <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="" class="control-label">Password</label>
                              <div class="clear"></div>
                              <input type="password" class="form-control">
                            </div>
                            <div class="form-group">
                              <p class="help-block"><a href="#">Forgot Password?</a></p>
                            </div>
                              <button type="submit" class="btn btn-default btn-purples">CONTINUE TO CHECK OUT</button>
                          </form>
                        </div>

                        <div class="clear"></div>
                      </div>
                      
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel_box_checkout">
                    <div class="padding">
                      <div class="mw400 tengah text-left">
                        <h4>Check out without signing in</h4>
                        <div class="clear"></div>
                        <p>You do not need to create an account to place an order.</p>
                        <p>To make future purchases even faster, you will have the option of creating an account during checkout.</p>

                        <div class="clear h150"></div><div class="height-10"></div>
                        <button type="button" class="btn btn-default btn-purples" onclick="javascript:window.location.href='#';">CHECK OUT AS GUEST</button>

                        <div class="clear height-30"></div>
                        <div class="clear"></div>
                      </div>
                      
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="clear"></div>
            </div>
            <!-- end checkout Sign -->
          </div>
          <div class="clear"></div>
        </div>
        <!-- end block signup cart -->

        <div class="clear height-20"></div>
        <!-- end page -->
        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>