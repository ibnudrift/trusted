<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_signup_content">
        <h1 class="title_page">SIGN IN</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row">
          <div class="col-md-6 text-left border-right">
            <div class="mw400 tengah text-left box_sign_default">
              <div class="height-10"></div>
              <div class="clear"></div>
              <div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>picture-sign-1.jpg" alt="" class="img-responsive"></div>
              <div class="clear height-25"></div>
              <div class="b_form_sign">
                <form method="post" action="">
                  <h3 class="sub_title">Patient Sign In</h3>
                  <div class="clear"></div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                  </div>

                  <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" class="form-control" id="" >
                  </div>

                  <button type="submit" class="btn btn-default btns_form_default_submit">SIGN IN</button>
                  <div class="clear height-25"></div>
                  <p class="help-block"><a href="#" target="_blank">Forgot Password</a>?<br />
                  Don&#39;t have an account? <a href="#" target="_blank">Sign Up</a></p>
                </form>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
            <!-- end box --> <div class="clear height-10"></div>

          </div>
          <div class="col-md-6 text-left">
            <div class="mw400 tengah text-left box_sign_default">
              <div class="height-10"></div>
              <div class="clear"></div>
              <div class="pict_full"><img src="<?php echo $this->assetBaseurl ?>picture-sign-2.jpg" alt="" class="img-responsive"></div>
              <div class="clear height-25"></div>
              <div class="b_form_sign">
                <form method="post" action="">
                  <h3 class="sub_title">Doctor Sign In</h3>
                  <div class="clear"></div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                  </div>

                  <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" class="form-control" id="" >
                  </div>

                  <button type="submit" class="btn btn-default btns_form_default_submit">SIGN IN</button>
                  <div class="clear height-25"></div>
                  <p class="help-block"><a href="#" target="_blank">Forgot Password</a>?<br />
                  Don&#39;t have an account? <a href="#" target="_blank">Sign Up</a></p>
                </form>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
            <!-- end box -->

          </div>
          <!-- end col md 6 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>