<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <div class="lefts_cont_member">
              <ul class="list-unstyled">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberE')); ?>">Manage Account</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberClinic')); ?>">Manage Clinic</a></li>
                <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/design/memberReview')); ?>">Manage Reviews</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/design/memberBlog')); ?>">Manage Messages</a></li>
              </ul>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="col-md-9">
            <div class="conts_boxreview_memberm">
              
              <div class="box_listing_surgeons2">
              <div class="row">
                <div class="col-md-12">

                  <div class="items">
                    <div class="picture doctor_p">
                      <a href="#">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/doctor/exam_pict_doctor3.png" alt="" class="img-responsive">
                      </a>
                    </div>
                    <div class="ml102">
                      <div class="desc">
                        <div class="doctor_info">
                          <div class="row">
                            <div class="col-md-12">
                              <a href="/trusted2/surgeons/detail/6">
                              <span class="names">Ashbury Cosmetic Surgery Centre</span>
                              </a>
                              <p> <br>
                                                                        <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o"></i> &nbsp; 1 reviews <br>
                                                                     Level 1 222 Lutwyche Rd <br>
                                  Windsor QLD 4030  <br>
                                                                            Phone: +61 (07) 3857 6188</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- end description -->
                    </div>
                    <div class="bottoms_message_reviewReply back-white">
                      <div class="clear height-20"></div>

                        <div class="row default prelatife blocks_bottom_d">
                          <div class="col-md-10">
                            <div class="text-center">
                                <span class="sub_t center-block"><b>Message</b></span>
                            </div>
                            <div class="clear height-15"></div>

                            <div class="row default">
                              <div class="col-sm-12">
                                <span class="sub_t mb-0"><strong>Name Patient</strong></span>
                                <div class="clear"></div>
                                <span class="dates">Send time: 23/09/2016</span>
                                <div class="clear"></div>
                                <p>Member patient message test.</p>
                              </div>
                              <div class="col-sm-12">
                                <div class="owners text-right">
                                   <span class="sub_t mb-0"><strong>Name Patient</strong></span>
                                    <div class="clear"></div>
                                    <span class="dates">Send time: 23/09/2016</span>
                                    <div class="clear"></div>
                                    <p>Member patient message test.</p>
                                  <div class="clear"></div>
                                </div>
                              </div>
                            </div>
                            <div class="row default">
                              <div class="col-sm-12">
                                <span class="sub_t mb-0"><strong>Name Patient</strong></span>
                                <div class="clear"></div>
                                <span class="dates">Send time: 23/09/2016</span>
                                <div class="clear"></div>
                                <p>Member patient message test.</p>
                              </div>
                            </div>

                            <div class="clear height-25"></div>
                            
                            <div class="collapse" id="collapseExample">
                              <div class="clear height-20"></div>
                                <div class="well">
                                  <form method="post" action="#">
                                    <div class="form-group">
                                      <textarea placeholder="Reply Message" name="" id="" rows="4" class="form-control"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-success">SEND</button>
                                  </form>
                                </div>
                              </div>
                            <!-- end collapse -->
                          </div>
                          <div class="col-md-2 border-left h143">
                            <div class="buttons_d">
                              <a class="btn btn-default btn-link" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-comments"></i>&nbsp;&nbsp;Reply</a>
                            </div>
                            <div class="clear"></div>
                          </div>
                        </div>

                      <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>

                </div>
              </div>

                <?php /*$this->widget('CLinkPager', array(
                    'pages' => $dataDoctor->getPagination(),
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                ))*/ ?>

              <div class="clear"></div>
            </div>
            <!-- end box listing surgeons2 -->

            <div class="rights_cont_member">
              <!-- start review cont -->
              <div class="list_box_default_member_white reviews_manage">

                <div class="items prelatife">
                  <div class="padding">
                    <div class="less_top_memberreview">
                      <div class="pictures"><img src="<?php echo $this->assetBaseurl ?>exam_pict_doctor3.jpg" alt="" class="img-responsive"></div>
                      <div class="info_links">
                        <p>
                          Name: Yanli XIe <br>
                          Phone: 0469865359 <br>
                          Email: yanli@twmg.com.au
                        </p>
                        <div class="clear"></div>
                        <p class="c2">Send time: 23/09/2016</p>
                      </div>
                    </div>
                    <div class="clear height-10"></div>
                  </div>

                   <div class="blocks_bottom_d">
                      <div class="padding h143 pt0">
                        
                        <div class="row default prelatife h143">
                          <div class="col-md-10">
                            <span class="sub_t">Message</span>
                            <div class="clear"></div>
                            <p>Dr Lim is honest and professional. I would highly reommend him to anyone considering surgery.</p>
                            <div class="padding-left-25">
                              <div class="clear height-15"></div>
                              <span class="sub_t mb-0">Reply</span>
                              <div class="clear height-5"></div>
                              <span class="dates">Send time: 23/09/2016</span>
                              <div class="clear"></div>
                              <p>Dr Lim is honest and professional. I would highly reommend him to anyone considering surgery.</p>
                              <div class="clear height-20"></div>
                            </div>
                            
                            <div class="collapse" id="collapseExample">
                              <div class="clear height-20"></div>
                                <div class="well">
                                  <form method="post" action="#">
                                    <div class="form-group">
                                      <textarea name="" id="" rows="4" class="form-control"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-success">SEND</button>
                                  </form>
                                </div>
                              </div>
                            <!-- end collapse -->
                          </div>
                          <div class="col-md-2 border-left h143">
                            <div class="buttons_d">
                              <a class="btn btn-default btn-link" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-comments"></i>&nbsp;&nbsp;Reply</a>
                            </div>
                            <div class="clear"></div>
                          </div>
                        </div>

                        <div class="clear"></div>
                      </div>
                    </div>
                        <div class="clear"></div>
                </div>

                <div class="clear"></div>
              </div>
              <!-- end review cont -->
            </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>