<form action="" method="post">
	<div class="box_default prelatife">
		<h3 class="sub_title">Clinic Contact Information</h3>
                <div class="clear"></div>

                <div class="row memberc">
                	<div class="col-md-6">
                		<div class="form-group">
                			<input type="text" class="form-control" placeholder="Clinic`s Name">
                		</div>
                	</div>
                	<div class="col-md-6">
                		<div class="form-group">
                			<input type="text" class="form-control" placeholder="Website">
                		</div>
                	</div>
                </div>
                <div class="row memberc">
                	<div class="col-md-6">
                		<div class="form-group">
                			<input type="text" class="form-control" placeholder="Phone">
                		</div>
                	</div>
                	<div class="col-md-6">
                		<div class="form-group">
                			<input type="text" class="form-control" placeholder="Fax">
                		</div>
                	</div>
                </div>
	</div>
	<!-- <div class="clear height-35"></div> -->

	<div class="box_default prelatife">
        <div class="row">
        	<div class="col-md-12">
        		<div class="form-group">
        			<input type="text" class="form-control" placeholder="Address line 1">
        		</div>
        	</div>
        	<div class="col-md-12">
        		<div class="form-group">
        			<input type="text" class="form-control" placeholder="Address line 12">
        		</div>
        	</div>
        </div>
        <div class="row memberc">
        	<div class="col-md-4">
        		<div class="form-group">
        			<input type="text" class="form-control" placeholder="Suburb">
        		</div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
        			<input type="text" class="form-control" placeholder="Postcode">
        		</div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
        			<input type="text" class="form-control" placeholder="State">
        		</div>
        	</div>
        </div>
	</div>
        <button class="btn btn-default border-r0" type="button" role="button"><i class="fa fa-plus"></i> &nbsp;Add another clinic</button>

	<div class="clear height-35"></div>
	<div class="box_default prelatife">
		<h3 class="sub_title">Social Links</h3>
        <div class="clear"></div>
        <div class="row memberc">
        	<div class="col-md-4">
        		<div class="form-group">
        			<input type="text" class="form-control" placeholder="Facebook">
        		</div>
        	</div>
        	<div class="clear"></div>
        	<div class="col-md-4">
        		<div class="form-group">
        			<input type="text" class="form-control" placeholder="Twitter">
        		</div>
        	</div>
        	<div class="clear"></div>
        	<div class="col-md-4">
        		<div class="form-group">
        			<input type="text" class="form-control" placeholder="Instagram">
        		</div>
        	</div>
        </div>
	</div>
	<div class="clear height-15"></div>
	<div class="row">
		<div class="col-md-12">
			<button class="btn_purple_member defaults btn btn-default">SUBMIT</button>
		</div>
	</div>
</form>