<form action="" method="post">
    <div class="box_default prelatife">
      <h3 class="sub_title">Update Photo</h3>
          <div class="clear"></div>
          <div class="row memberc">
            <!-- start left -->
            <div class="col-md-3">
              <div class="form-group">
                <div class="bx_imageupload">
                  <input type="file" class="hidden" id="uploadFile"/>
                  <div class="button" id="uploadTrigger">
                    <img src="<?php echo $this->assetBaseurl ?>plus-member-photoUpdate.png" alt="" class="img-responsive">
                  </div>
                </div>
              </div>
            </div>
            <!-- End left -->
            <?php // for ($i=0; $i < 2; $i++) { ?>
            <div class="col-md-3">
              <div class="form-group">
                <div class="picture">
                  <?php /*<img src="<?php echo $this->assetBaseurl ?>t-updatephoto-member.jpg" alt="" class="img-responsive mb-0">*/ ?>
                  <div class="backs_picturem_updatephoto">
                    <div class="in_photo" style="background-image: url('<?php echo $this->assetBaseurl ?>t-updatephoto-member.jpg');"></div>
                  </div>
                </div>
                <div class="clear height-10"></div>
                <input type="text" class="form-control" placeholder="Description">
                <div class="clear height-10"></div>
                <select name="#" id="" class="form-control">
                  <option value="">Select Procedures</option>
                  <option value="">Procedures 1</option>
                  <option value="">Procedures 2</option>
                  <option value="">Procedures 3</option>
                </select>
              </div>
            </div>
            <?php // } ?>

            <!-- end  -->
          </div>
    </div>

    <div class="clear height-15"></div>
    <div class="row">
      <div class="col-md-12">
        <button class="btn_purple_member defaults btn btn-default">SUBMIT</button>
      </div>
    </div>
  </form>

  <script type="text/javascript">
  $(document).ready(function(){
    $("#uploadTrigger").click(function(){
       $("#uploadFile").click();
    });
  })
  </script>