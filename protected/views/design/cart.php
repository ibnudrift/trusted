<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_outers_cart_content">
        <h1 class="title_page">MY BAG</h1>
        <div class="clear height-40"></div> <div class="clear height-5"></div>
        <div class="clear"></div>
        
        <div class="blocks_subtitle">
          <span>ORDER REVIEW</span>
        </div>
        <div class="clear height-5"></div>

        <div class="box_table_cart">
          <table class="table table-hover tables_cart table-striped">
            <thead>
              <tr>
                <th>PRODUCT</th>
                <th>ONLINE AVAILABILITY</th>
                <th>QUALITY</th>
                <th class="text-right">UNIT PRICE</th>
                <th class="text-right">TOTAL PRICE</th>
              </tr>
            </thead>
            <tbody>
              <?php for ($i=1; $i < 3; $i++) { ?>
              <tr>
                <td>
                  <div class="text_cpicture">
                    <div class="pictures fleft">
                      <img src="<?php echo $this->assetBaseurl ?>pict-shop-1.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="descs fleft padding-left-30">
                      <div class="chl mh110">
                        <span class="names">Glasshouse Fragrances Hand Wash - Saigon</span>
                      </div>
                      <a href="#" class="btn btn-default btn-link delete_cart"><i class="fa fa-times-circle"></i> &nbsp;Remove Product</a>
                    </div>
                  </div>
                </td>
                <td>
                  <i class="fa fa-check purple_t"></i>&nbsp; In Stock
                </td>
                <td>
                  <div class="c_input_qty"><input type="number" class="form-control" name="" value="1"></div>
                </td>
                <td class="text-right">
                  $ 24.95
                </td>
                <td class="text-right">
                  <strong>$ 24.95</strong>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <div class="clear"></div>
        </div>

        <div class="clear"></div>
        <div class="fright mw445">
          <div class="promo_code_bx">
            <table class="table table-noborder">
              <tr class="top_inp_promocode">
                <td>
                  <label for=""><b>PROMO CODE</b></label>
                </td>
                <td>
                  <input type="text" class="form-control">
                </td>
                <td>
                  <button class="btn btn-default btns_aplyr">APPLY</button>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="right">
                  <div class="text-right">
                    Order Subtotal
                  </div>
                </td>
                <td>
                  <div class="text-right">$ 24.95</div>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="right">
                  <div class="text-right">
                    Promotion Discount
                  </div>
                </td>
                <td>
                  <div class="text-right">0</div>
                </td>
              </tr>
              <tr class="bb_1">
                <td colspan="2" align="right">
                  <div class="text-right"><label><b>Order Total Excluding Delivery</b></label></div>
                </td>
                <td>
                  <div class="text-right">
                    <span class="price_total">$ 24.95</span>
                  </div>
                </td>
              </tr>
            </table>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <!-- End promo code bottom Shoppin Cart -->
        <div class="clear height-15"></div>
        <div class="height-2"></div>

        <div class="row">
          <div class="col-md-6">
            <div class="text-left">
            <button class="btn btn-default bt_purpleshopdt bt_continue_shop"><i class="fa fa-chevron-left"></i> &nbsp;CONTINUE SHOPPING</button>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-right">
              <button class="btn btn-default bt_purpleshopdt bt_checkout_shop">CHECK OUT SECURELY</button>
            </div>
          </div>
        </div>
        
        <!-- <div class="blocks_subtitle b2">
          <span>DELIVERY ADDRESS</span>
        </div>
        <div class="clear height-35"></div> -->

        <div class="clear height-20"></div>
        <!-- end page -->
        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>