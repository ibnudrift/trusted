<?php 

$tt1 = array(
	'Breast Augmentation',
	'Breast Lift',
	'Breast Reduction',
	'Breast Reconstruction',
	'Nipple Enhancement',
	'Nipple Reduction',
	'Aerola Reduction',
	'Tuberous Breast Correction',
	'Breast Asymmetry correction',
	'Fat grafting for breast contouring'
	);

$tt2 = array(
	'Fat Injection',
	'Face Lift',
	'Brow Lift',
	'Eyelid Surgery'
);

$tt3 = array(
	'Body Lift and Reshaping',
	'Mummy Makeover',
	'Liposuction',
	'Tummy Tuck'
);
?>
<form action="" method="post">
	<div class="box_default prelatife">
		<h3 class="sub_title">Breast</h3>
        <div class="clear"></div>
        <div class="row memberc">
        	<?php foreach ($tt1 as $key => $value): ?>
        	<div class="col-md-4">
        		<div class="form-group">
        			<div class="checkbox">
					    <label>
					      <input type="checkbox"> <?php echo $value; ?>
					    </label>
					  </div>
        		</div>
        	</div>
        	<?php endforeach ?>
        </div>
	</div>

	<div class="clear height-35"></div>
	<div class="box_default prelatife">
		<h3 class="sub_title">Face</h3>
        <div class="clear"></div>
        <div class="row memberc">
        	<?php foreach ($tt2 as $key => $value): ?>
        	<div class="col-md-4">
        		<div class="form-group">
        			<div class="checkbox">
					    <label>
					      <input type="checkbox"> <?php echo $value; ?>
					    </label>
					  </div>
        		</div>
        	</div>
        	<?php endforeach ?>
        </div>
	</div>

	<div class="clear height-35"></div>
	<div class="box_default prelatife">
		<h3 class="sub_title">Body</h3>
        <div class="clear"></div>
        <div class="row memberc">
        	<?php foreach ($tt3 as $key => $value): ?>
        	<div class="col-md-4">
        		<div class="form-group">
        			<div class="checkbox">
					    <label>
					      <input type="checkbox"> <?php echo $value; ?>
					    </label>
					  </div>
        		</div>
        	</div>
        	<?php endforeach ?>
        </div>
	</div>

	<div class="clear height-15"></div>
	<div class="row">
		<div class="col-md-12">
			<button class="btn_purple_member defaults btn btn-default">SUBMIT</button>
		</div>
	</div>
</form>