<section class="section_default back_cream mh500 back_grey_pattern sub_page start_journey">
  <div class="prelatife container z-15">
    <div class="clear height-50"></div>
    <div class="clear height-50"></div>

      <div class="blocks_journey text-center step_1 step_3">
      <div class="tops">
        <h6 class="sub_title">PROCEDURES</h6>
        <div class="clear"></div>
        <div class="lines_purple_journey tengah"></div>
        <div class="clear height-0"></div>

        <p><span>Personalized</span> services from your Trusted Surgeons</p>
      </div>

      <div class="clear height-50"></div><div class="height-20"></div>

      <div class="middle prelatife">
        <div class="row default">
          <div class="col-md-12">
            <div class="tengah content-text">
              <div class="mw909 tengah">
              <p>Your surgeons will have a complied amount of information in relation to your procedure. However, for the purpose of a guide only and not substituting medical advice. Our animations and information is sourced from the American Society of Plastic Surgery and the Australasian Society of Plastic Surgery.</p>
              <p>It is important to know what is available to you, the details, risks and benefits unique to you are to be discussed with your surgeon. There is a good reason why your surgeons time is so valuable and we do not want to encourage you to replace a surgeon many years of education with a google search</p>
              <div class="clear height-50"></div>
              </div>

              <div class="listing_banner_default text-center">
                <div class="row">
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picts">
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'breast')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>banner-1.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <article>
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'breast')); ?>"><h6>BREAST</h6></a>
                        <div class="clear"></div>
                        <div class="desc">
                          <p>Duis accumsan, nunc ut aliquet porta, nunc est pulvinar eros, at porttitor orci metus quis tellus.</p>
                        </div>
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'breast')); ?>" class="btn btn-default btn_learn_more">LEARN MORE</a>  
                      </article>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picts">
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'body')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>banner-2.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <article>
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'body')); ?>"><h6>BODY</h6></a>
                        <div class="clear"></div>
                        <div class="desc">
                          <p>Ut et augue dapibus, fermentum augue eu, aliquet mauris. Nullam fermentum laoreet imperdiet. Pellentesque ante leo, mollis eget tincidunt ac, venenatis ac diam.</p>
                        </div>
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'body')); ?>" class="btn btn-default btn_learn_more">LEARN MORE</a>  
                      </article>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="items">
                      <div class="picts">
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'face')); ?>">
                        <img src="<?php echo $this->assetBaseurl ?>banner-3.jpg" alt="" class="img-responsive">
                        </a>
                      </div>
                      <article>
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'face')); ?>"><h6>FACE</h6></a>
                        <div class="clear"></div>
                        <div class="desc">
                          <p>Nunc a diam eu velit scelerisque lobortis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;  Quisque maximus ex vel magna euismod porta.</p>
                        </div>
                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'face')); ?>" class="btn btn-default btn_learn_more">LEARN MORE</a>  
                      </article>
                    </div>
                  </div>
                </div>
              </div>

              <div class="clear height-50"></div>
              <div class="clear height-20"></div>
              <div class="mw909 tengah">
                <div class="row">
                  <div class="col-md-6">
                    <a href="<?php echo CHtml::normalizeUrl(array('/coaching/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>back-banner1-step-5.png" alt="" class="img-responsive"></a>
                  </div>
                  <div class="col-md-6">
                    <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>back-banner2-step-5.png" alt="" class="img-responsive"></a>
                  </div>
                </div>
              </div>

              <div class="clear"></div>
            </div>

            <div class="clear"></div>
          </div>

        </div>
        <div class="clear height-50"></div>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>

    <div class="clear height-0"></div>

    <div class="clear"></div>
  </div>

</section>