<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static procedures">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      <!-- <div class="pict_full illustration_picture"><img src="<?php // echo $this->assetBaseurl ?>pict_top_procedures.jpg" alt="" class="img-responsive"></div>
      <div class="clear height-50"></div> -->

      <div class="content-text insides_static">
        <h1 class="title_page">PROCEDURES</h1>
        <div class="clear height-50"></div>
        <div class="clear"></div>

        <div class="wrapper-cont-procedures">
          <div class="top-filter">
            <form action="#" class="form-inline">
              <div class="form-group">
                <select name="#" id="" class="form-control">
                  <option value="0">All</option>              <option value="5">Face</option>             <option value="4">Breasts</option>              <option value="3">Body</option>             <option value="2">Other</option>
                </select>
              </div>
            </form>
          </div>
          <div class="clear height-50"></div>
          <div class="height-50"></div>
          <div class="height-30"></div>

          <div class="lists_procedures_nw">
            <div class="items type1">
              <div class="top margin-bottom-50">
                <h3 class="title">FACE</h3>
              </div>
              <div class="middles">
                <div class="row">
                  <div class="col-md-5">
                    <div class="pict"><img src="<?php echo $this->assetBaseurl ?>face.jpg" alt="" class="img-responsive"></div>
                  </div>
                  <div class="col-md-7">
                    <div class="desc">
                          <ul class="list-unstyled">
                              <li><a href="#">Brow Lift</a>
                              </li>
                              <li><a href="#">Cheek Fat Removal</a>
                              </li>
                              <li><a href="#">Ear Pinning</a>
                              </li>
                              <li><a href="#">Eyelid Surgery - Lower</a>
                              </li>
                              <li><a href="#">Eyelid Surgery - Upper</a>
                              </li>
                              <li><a href="#">Face Lift</a>
                              </li>
                              <li><a href="#">Facial Implants - Cheek</a>
                              </li>
                              <li><a href="#">Facial Implants - Chin</a>
                              </li>
                              <li><a href="#">Facial Implants - Nasal</a>
                              </li>
                              <li><a href="#">Facial Scar Revision</a>
                              </li>
                              <li><a href="#">Fat Injection</a>
                              </li>
                              <li><a href="#">Forehead Lift</a>
                              </li>
                              <li><a href="#">Lip Lift</a>
                              </li>
                              <li><a href="#">Neck Lift</a>
                              </li>
                              <li><a href="#">Rhinoplasty</a>
                              </li>
                              <li><a href="#">Scalp Expansion</a>
                              </li>
                              <li><a href="#">Scalp Reduction</a>
                              </li>
                          </ul>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="items type2">
              <div class="top margin-bottom-50">
                <h3 class="title">breasts</h3>
              </div>
              <div class="middles">
                <div class="row">
                  <div class="col-md-7">
                    <div class="desc">
                          <ul class="list-unstyled">
                            <li><a href="#">Breast Asymmetry Correction</a>
                            </li>
                            <li><a href="#">Nipple Enhancement for Inverted Nipples</a>
                            </li>
                            <li><a href="#">Breast Implants with Lift</a>
                            </li>
                            <li><a href="#">Breast Augmentation</a>
                            </li>
                            <li><a href="#">Breast Lift</a>
                            </li>
                            <li><a href="#">Breast Reconstruction</a>
                            </li>
                            <li><a href="#">Breast Revision</a>
                            </li>
                            <li><a href="#">Breast reduction (gynaecomastia)</a>
                            </li>
                            <li><a href="#">Breast Reduction</a>
                            </li>
                            <li><a href="#">Fat Transfer and Implants</a>
                            </li>
                            <li><a href="#">Tuberous Breast Correction</a>
                            </li>
                        </ul>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="pict"><img src="<?php echo $this->assetBaseurl ?>breast.jpg" alt="" class="img-responsive"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="items type1">
              <div class="top margin-bottom-50">
                <h3 class="title">body</h3>
              </div>
              <div class="middles">
                <div class="row">
                  <div class="col-md-5">
                    <div class="pict"><img src="<?php echo $this->assetBaseurl ?>body.jpg" alt="" class="img-responsive"></div>
                  </div>
                  <div class="col-md-7">
                    <div class="desc">
                          <ul class="list-unstyled">
                            <li><a href="#">Triceps Implants</a>
                            </li>
                            <li><a href="#">Bicep Implants</a>
                            </li>
                            <li><a href="#">Calf Implants</a>
                            </li>
                            <li><a href="#">Vaser Liposculpture</a>
                            </li>
                            <li><a href="#">Liposoft (Liquid Liposuction)</a>
                            </li>
                            <li><a href="#">Abdominal Etching</a>
                            </li>
                            <li><a href="#">Arm Lift</a>
                            </li>
                            <li><a href="#">Buttocks Implants</a>
                            </li>
                            <li><a href="#">Chest Implants</a>
                            </li>
                            <li><a href="#">Brazilian Butt Lift</a>
                            </li>
                            <li><a href="#">Fat Transfer</a>
                            </li>
                            <li><a href="#">Hand Surgery</a>
                            </li>
                            <li><a href="#">Lift – Buttocks</a>
                            </li>
                            <li><a href="#">Lift – Thigh</a>
                            </li>
                            <li><a href="#">Lift – Whole Body</a>
                            </li>
                            <li><a href="#">Liposuction</a>
                            </li>
                            <li><a href="#">Mons Reduction</a>
                            </li>
                            <li><a href="#">Mummy Makeover</a>
                            </li>
                            <li><a href="#">Scar Revision</a>
                            </li>
                            <li><a href="#">Skin Cancer</a>
                            </li>
                            <li><a href="#">Tummy Tuck - Full</a>
                            </li>
                            <li><a href="#">Tummy Tuck – Mini</a>
                            </li>
                        </ul>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="items type2">
              <div class="top margin-bottom-50">
                <h3 class="title">Intimates</h3>
              </div>
              <div class="middles">
                <div class="row">
                  <div class="col-md-7">
                    <div class="desc">
                         <ul class="list-unstyled">
                          <li><a href="#">Vaginaplasty</a>
                          </li>
                          <li><a href="#">Circumcision (phimosis)</a>
                          </li>
                          <li><a href="#">Clitorial Reduction</a>
                          </li>
                          <li><a href="#">Excessive Sweating</a>
                          </li>
                          <li><a href="#">Gender Reassignment</a>
                          </li>
                          <li><a href="#">Hymen Reconstruction</a>
                          </li>
                          <li><a href="#">Labiaplasty</a>
                          </li>
                          <li><a href="#">Penis Lengthening</a>
                          </li>
                          <li><a href="#">Penis Thickening</a>
                          </li>
                          <li><a href="#">Testicular Implant Surgery</a>
                          </li>
                          <li><a href="#">Vaginal Tightening</a>
                          </li>
                      </ul>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="pict"><img src="<?php echo $this->assetBaseurl ?>other.jpg" alt="" class="img-responsive"></div>
                  </div>
                </div>
              </div>
            </div>
            <!-- end items -->
          </div>
          <!-- end lists procedures -->

          <div class="clear"></div>
        </div>

        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>
