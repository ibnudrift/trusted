<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['about_why_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h1>CONTACT US</h1>
    </div>
  </div>
  <div class="clear"></div>
</div>
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static contact_page">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-20"></div>
      
      <!-- <div class="pict_full illustration_picture"><img src="<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(1260,1000, '/images/static/'.$this->setting['contact_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
      <div class="clear height-50"></div> -->

      <div class="content-text insides_static inside_contact_page">
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-4">
            <div class="left_content">
                <!-- <p class="mb-0"><span>CUSTOMER SERVICE MANAGER</span></p> -->
                <?php echo $this->setting['contact_customer_services_op'] ?>
                <!-- <div class="clear height-25"></div> -->
                <?php // echo $this->setting['contact_customer_business_op'] ?>

                <?php /*<p class="mb-0"><span>ADDRESS</span>
                <?php // echo nl2br($this->setting['contact_address']) ?></p>
                <?php echo $this->setting['contact_address_op'] ?>*/ ?>
<?php /*
                <p class="mb-0"><span>Working Hours</span></p>
                <?php echo $this->setting['contact_opening'] ?>
*/ ?>
              <div class="clear"></div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="rights_content padding-left-30">
              
              <div class="sBox_contact_form">
              <div>
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">FOR CUSTOMER</a></li>
                  <li role="presentation"><a href="#surgeons" aria-controls="surgeons" role="tab" data-toggle="tab">FOR SURGEONS</a></li>
                </ul>

                <div class="tab-content">
                  <!-- start panel customer -->
                  <div role="tabpanel" class="tab-pane active" id="customer">
                      <?php echo $this->setting['contact_content_patiens'] ?>
                      <div class="contact_form">
                       <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                                // 'type'=>'',
                                'enableAjaxValidation'=>false,
                                'clientOptions'=>array(
                                    'validateOnSubmit'=>false,
                                ),
                                'htmlOptions' => array(
                                    'enctype' => 'multipart/form-data',
                                ),
                            )); ?>

                         <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
                         <?php if( Yii::app()->user->hasFlash('success') ): ?>
                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
                            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css">
                            <script type="text/javascript">
                                    sweetAlert("Success!", "Thank you for contacting us!.", "success");
                            </script>
                          <?php endif; ?>

                          <?php $model->contact_type = 'customer'; ?>
                          <?php echo $form->hiddenField($model, 'contact_type', array()); ?>
                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput1">First Name</label>
                                <?php echo $form->textField($model, 'first_name', array('class'=>'form-control', 'required'=>'required', 'id'=>'exampleInput1')); ?>
                              </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput2">Last Name</label>
                                <?php echo $form->textField($model, 'last_name', array('class'=>'form-control', 'required'=>'required', 'id'=>'exampleInput2')); ?>
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput3">Phone Number</label>
                                <?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'required'=>'required', 'id'=>'exampleInput3')); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                              <label for="exampleInput4">Email Address</label>
                              <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'required'=>'required', 'id'=>'exampleInput4')); ?>
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                              <label for="exampleInput5">Procedure I am interested in</label>
                              <?php $list_proced = array(
                                'Face'=>'Face', 
                                'Breasts'=>'Breasts', 
                                'Body'=>'Body',
                                'Intimates'=>'Intimates'
                                ); ?>
                              <?php echo $form->dropDownList($model, 'procedures', $list_proced, array('class'=>'form-control', 'required'=>'required', 'empty'=> 'Please Select')); ?>
                              </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                              <label for="exampleInput6">Booking a coaching service</label>
                               <div class="checkbox">
                                <label>
                                  <?php $model->coaching = 1; ?>
                                  <?php echo $form->checkBox($model, 'coaching', array()); ?> Yes
                                </label>
                              </div>
                              </div>
                            </div>
                          </div>
                          <div class="row default">
                            <div class="col-sm-12 col-md-12">
                              <div class="form-group">
                              <label for="exampleInput7">Any question for Trusted Surgeons?</label>
                              <?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'rows'=>4, 'required'=>'required')); ?>
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <!-- <div class="g-recaptcha" data-sitekey="6LehEyYTAAAAAHB_k-y4DdfPrdJMwJ9bU11A4_0Q"></div> -->
                              <div id="recaptcha1"></div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group fright">
                                  <button type="submit" class="btn btn-default submits_form"></button>
                              </div>
                            </div>
                          </div>
                          
                        <?php $this->endWidget(); ?>
                        <div class="clear"></div>
                      </div>
                  </div>
                  <!-- start panel surgeons -->
                  <div role="tabpanel" class="tab-pane" id="surgeons">
                      <?php echo $this->setting['contact_content_surgeons'] ?>
                      <div class="contact_form">
                        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                                // 'type'=>'',
                                'enableAjaxValidation'=>false,
                                'clientOptions'=>array(
                                    'validateOnSubmit'=>false,
                                ),
                                'htmlOptions' => array(
                                    'enctype' => 'multipart/form-data',
                                ),
                            )); ?>

                         <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
                          <?php if(Yii::app()->user->hasFlash('success')): ?>
                              <?php $this->widget('bootstrap.widgets.TbAlert', array(
                                  'alerts'=>array('success'),
                              )); ?>
                          <?php endif; ?>

                          <?php $model->contact_type = 'surgeons'; ?>
                          <?php echo $form->hiddenField($model, 'contact_type', array()); ?>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput11">First Name</label>
                                <?php echo $form->textField($model, 'first_name', array('class'=>'form-control', 'required'=>'required', 'id'=>'exampleInput11')); ?>
                              </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput2">Last Name</label>
                                <?php echo $form->textField($model, 'last_name', array('class'=>'form-control', 'required'=>'required')); ?>
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                <label for="exampleInput3">Phone Number</label>
                                <?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'required'=>'required')); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                              <label for="exampleInput4">Email Address</label>
                              <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'required'=>'required')); ?>
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-sm-12 col-md-12">
                              <div class="form-group">
                              <label for="exampleInput6">Any question for Trusted Surgeons?</label>
                              <?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'rows'=>4, 'required'=>'required')); ?>
                              </div>
                            </div>
                          </div>

                          <div class="row default">
                            <div class="col-md-6 col-sm-6">
                              <!-- <div class="g-recaptcha" data-sitekey="6LehEyYTAAAAAHB_k-y4DdfPrdJMwJ9bU11A4_0Q"></div> -->
                              <div id="recaptcha2"></div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <div class="form-group fright">
                                  <button type="submit" class="btn btn-default submits_form"></button>
                              </div>
                            </div>
                          </div>
                          
                        <?php $this->endWidget(); ?>
                        <div class="clear"></div>
                      </div>
                  </div>

                </div>

              </div>

              <div class="clear"></div>
            </div>
            <!-- End detail surgeons -->

              <div class="clear"></div>
            </div>
          </div>
        </div>
        <!-- end inside page contact -->

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

