<div class="outers_pg_banner">
  <div class="page-banner" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(2000,500, '/images/static/'.$this->setting['blog_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="steps-title">
      <h2><?php echo $this->setting['blog_title'] ?></h2>
      <h3><?php echo $dataBlog->title ?></h3>
    </div>
  </div>
  <div class="clear"></div>
</div>
<section class="outers_page_static back_cream mh500 back_grey_pattern Blog_details">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="tagline"><?php echo $dataBlog->title ?></h1>
        <div class="clear"></div>
        <div class="row details_cont_articles">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">

                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(980,1000, '/images/blog/'.$dataBlog->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="">

                <?php echo $dataBlog->content ?>

                <div class="clear height-10"></div>
                <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58f5e4af3ee8fe7e"></script> 
                <div class="addthis_inline_share_toolbox"></div>
<!--                 <div class="shares-text text-left p_shares_article">
                    <span class="inline-t">SHARE</span>&nbsp; / &nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=#">FACEBOOK</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://plus.google.com/share?url=#">GOOGLE PLUS</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://twitter.com/home?status=#">TWITTER</a>
                </div>
 -->
                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">Other Blogs</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                    <?php foreach ($dataBlogs->getData() as $key => $value): ?>
                        
                  <li><a href="<?php echo CHtml::normalizeUrl(array('detail', 'slug'=>$value->slug)); ?>"><?php echo $value->title ?></a></li>
                    <?php endforeach ?>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>
      
      <div class="clear height-20"></div>
      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>