<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static page_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      
      <div class="content-text insides_static outers_page_detail_product">
        <div class="tops">
          <div class="row">
            <div class="col-md-9">
              <div class="n_breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="<?php echo CHtml::normalizeUrl(array('list', 'id'=>$category->id)); ?>">SHOP</a></li>
                  <li class="active"><?php echo $category->description->name ?></li>
                </ol>
                <div class="clear"></div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="backs_btn_product text-right"><a href="<?php echo CHtml::normalizeUrl(array('list', 'id'=>$category->id)); ?>">BACK</a></div>
            </div>
          </div>
        </div>
        <!-- end tops -->
        <div class="middle">
          <div class="details_product_box_content">
            
          <div class="row">
            <div class="col-md-6">

              <div class="picture">
                <div class="ds_table">
                  <div class="ins_table">
                      <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(638,638, '/images/product/'.$data->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="description">
                <h4 class="titles"><?php echo $data->description->name ?></h4>
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                        'alerts'=>array('success'),
                    )); ?>
                    
                    <a href="<?php echo CHtml::normalizeUrl(array('index')); ?>" class="btn btn-default btns_purple_cart back_purple_defaults_tl">CONTINUE SHOPPING</a> &nbsp;&nbsp;
                    <a href="<?php echo CHtml::normalizeUrl(array('/cart/shop')); ?>" class="btn btn-default btns_purple_cart back_purple_defaults_tl">GO TO CART</a>
                <?php else: ?>
                
                <div class="clear"></div>
                <form action="<?php echo CHtml::normalizeUrl(array('addcart')); ?>" method="post">
                  <div class="child_desription_product">
                    <table class="table">
                      <tr>
                        <td>Price </td>
                        <td><b><?php echo Cart::money($data->harga) ?></b></td>
                      </tr>
                      <tr>
                        <td>Stock </td>
                        <td><b><?php echo ($data->out_stock == 1) ? "Not Available" : "Available" ?></b></td>
                      </tr>
                      <tr>
                        <td>Delivery </td>
                        <td><b><?php echo ($data->terlaris == 1) ? "Free" : Cart::money(15) ?></b></td>
                      </tr>
                      <tr>
                        <td>Quantity</td>
                        <td>
                          <input type="number" class="form-control quantity" name="qty" value="1">
                        </td>
                      </tr>
                    </table>
                    <div class="clear"></div>
                  </div>
                  <input type="hidden" name="id" value="<?php echo $data->id ?>">
                  <button type="submit" value="add-cart" class="btn btn-default btns_purple_cart back_purple_defaults_tl">ADD TO CART</button> &nbsp;&nbsp;
                  <button type="submit" value="buy-now" class="btn btn-default btns_purple_cart back_purple_defaults_tl">BUY IT NOW</button>
                  <div class="clear"></div>
                </form>
                <?php endif; ?>
                <div class="clear height-40"></div>
                <?php echo $data->description->desc ?>


                <div class="clear"></div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          </div>
          <!-- end detail product content -->

          <div class="clear"></div>
        </div>
        <!-- end middle -->
        <div class="clear"></div>

        <div class="clear"></div>
      </div>
      <!-- end page detal product -->

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>