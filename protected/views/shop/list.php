<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static page_products">
    
    <!-- <div class="fixd_illustrationtophg">
      <div class="pict_full">
        <img src="<?php // echo Yii::app()->baseUrl.ImageHelper::thumb(1580,415, '/images/category/'.$category->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
      </div>
    </div> -->
    <div class="clear height-5"></div>
    <div class="prelatife container">
      <div class="clear height-50"></div>
      <div class="height-10"></div>
      
      <!-- <div class="clear height-50"></div><div class="height-10"></div> -->
      <div class="content-text insides_static outers_page_listing_product">
        <h1 class="title_page">SHOP</h1>
        <div class="clear height-35"></div>

        <!-- start list product_list -->
        <!-- <h3 class="titles text-center"><?php // echo $category->description->name ?></h3> -->
        <?php
        $listProduct = $data->getData();
        ?>
        <?php if (count($listProduct) > 0): ?>
        <div class="box_listing_products_listdata">
          <div class="row">
            <?php foreach ($listProduct as $key => $value): ?>
              <div class="col-md-4">
                <div class="items">
                  <div class="picture">
                    <a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>">
                      <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(273,273, '/images/product/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive tengah">
                    </a>
                  </div>
                  <div class="desc">
                    <a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>"><span class="title"><?php echo $value->description->name ?></span></a>
                    <div class="clear"></div>
                    <span class="price"><?php echo Cart::money($value->harga) ?></span>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="clear"></div>
        </div>
        <?php else: ?>
          <h3>No Product Available Now</h3>
        <?php endif ?>
        <!-- // end list product_list -->

        <div class="clear height-40"></div>
        <?php $this->widget('CLinkPager', array(
                      'pages' => $data->getPagination(),
                      'header'=>'', 
                      'htmlOptions' => array('class'=>'pagination'),
                  )) ?>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>