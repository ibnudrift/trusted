<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <?php $this->renderPartial('/member/_menu', array(
            )) ?>
          </div>
          <div class="col-md-9">
            <div class="rights_cont_member">
              <?php if ($dataBlog->getTotalItemCount() > 0): ?>
              <!-- start review cont -->
              <div class="list_box_default_member_white blogs">
                <?php foreach ($dataBlog->getData() as $key => $value): ?>
                <div class="items prelatife">
                  <div class="padding">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="picture"><img src="http://placehold.it/195x135" alt="" class="img-responsive"></div>
                      </div>
                      <div class="col-md-9">
                        <h4 class="titles_s"><?php echo $value->title ?></h4>
                          <div class="clear"></div>
                          <div class="blocks_bottom_d">
                            <div class="pt0">
                              <div class="row prelatife">
                                <div class="col-md-9">
                                  <p><?php echo substr(strip_tags($value->content), 0, 200) ?>....</p>
                                  <span class="dates">Posted time: <?php echo date('d/m/Y',strtotime($value->date_input)) ?></span>
                                </div>
                                <div class="col-md-3 border-left h127">
                                  <div class="buttons_d padding-left-15">
                                    <a class="btn btn-default btn-link" href="#" role="button"><i class="fa fa-search"></i>&nbsp;&nbsp;Preview</a>
                                    <div class="clear height-3"></div>
                                    <a class="btn btn-default btn-link" href="<?php echo CHtml::normalizeUrl(array('index', 'update'=>$value->id)); ?>" role="button"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</a>
                                    <div class="clear height-3"></div>
                                    <a class="btn btn-default btn-link" href="<?php echo CHtml::normalizeUrl(array('index', 'delete'=>$value->id)); ?>" role="button"><i class="fa fa-times"></i>&nbsp;&nbsp;Delete</a>
                                  </div>
                                  <div class="clear"></div>
                                </div>
                              </div>

                              <div class="clear"></div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                   
                </div>
                <?php endforeach ?>
                <?php $this->widget('CLinkPager', array(
                    'pages' => $dataBlog->getPagination(),
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                )) ?>
                <?php else: ?>
                <h3>No data review</h3>
                <?php endif ?>

                <div class="clear"></div>
              </div>
              <!-- end review cont -->

              <div class="clear height-30"></div>
              <div class="box_form_reviewc">
                <?php if ($modelBlog->scenario == 'update'): ?>
                <h5 class="s_title">Edit Post</h5>
                <?php else: ?>
                <h5 class="s_title">Add New Post</h5>
                <?php endif; ?>
                <div class="clear height-20"></div>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
  'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
<?php $this->widget('ImperaviRedactorWidget', array(
    'selector' => '.redactor',
    // 'options' => array(
    //     'imageUpload'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'image')),
    //     'clipboardUploadUrl'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'clip')),
    // ),
)); ?>
                <?php echo $form->errorSummary($modelBlog); ?>
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                        'alerts'=>array('success'),
                    )); ?>
                
                <?php endif; ?>  
                <div class="well" style="margin:0;">
                    <div class="form-group">
                      <label for="" class="control-label col-sm-2">Title</label>
                      <div class="col-sm-10">
                        <?php echo $form->textField($modelBlog,'title',array('class'=>'form-control', 'placeholder'=>'Title')); ?>
                      </div>
                    </div>

                    <div class="row default">
                      <div class="col-sm-12 col-md-8">
                        <div class="form-group m-0">
                              <?php echo $form->textArea($modelBlog,'content',array('class'=>'form-control redactor')); ?>
                        </div>
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <div class="form-group m-0">
                          <!-- <label for="" class="control-label col-sm-2 m-0">Image</label> -->
                          <?php echo $form->fileField($modelBlog,'image',array('class'=>'')); ?>
                          <p><b>Note:</b> Image size is 650 x 650px. Larger image will be automatically cropped.</p>
                          <?php if ($modelBlog->scenario == 'update'): ?>
                          <img style="width: 100%;" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(650,650, '/images/blog/'.$modelBlog->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
                          <?php endif; ?>
                        </div>

                      </div>
                    </div>
                  <div class="clear"></div>
                </div>
                <div class="clear height-15"></div>
                <?php if ($modelBlog->scenario == 'update'): ?>
                <button type="submit" class="btn btn-default btn_purple_member defaults">SAVE</button>
                <?php else: ?>
                <button type="submit" class="btn btn-default btn_purple_member defaults">SAVE / PUBLISH</button>
                <?php endif; ?>
<?php $this->endWidget(); ?>

                <div class="clear"></div>
              </div>
              <!-- end box form_review -->

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>
<?php /*
<script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>
*/ ?>
