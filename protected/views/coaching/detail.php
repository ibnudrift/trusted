<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static coaching">
    <div class="fixd_illustrationtophg">
      <div class="pict_full">
        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1580,585, '/images/static/'.$this->setting['coaching_service_'.$id.'_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
      </div>
    </div>

    <div class="prelatife container">
      <!-- <div class="clear height-50"></div><div class="height-10"></div>
      <div class="height-5"></div>

      
        <div class="pict_full"><img src="" alt="" class="img-responsive"></div>
         -->
         <div class="content-text insides_static text-center coaching2_form">
        <div class="clear height-50"></div><div class="height-5"></div>

        <div class="tengah">
          <h1 class="title_page">COACHING</h1>
          <div class="clear"></div>
          <span>
            <?php echo $this->setting['coaching_service_'.$id.'_title'] ?> <br>
            <b><?php echo $this->setting['coaching_service_'.$id.'_rate'] ?></b>
          </span>
          <div class="clear height-40"></div>
          
          <div class="box_form_coaching_c">
            <form method="post" action="#">
              <p class="sub_title">Patient Information</p>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputFirstname">First Name</label>
                      <input type="text" class="form-control" id="exampleInputFirstname">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputlastname">Last Name</label>
                      <input type="text" class="form-control" id="exampleInputlastname">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputemail">Email Address</label>
                      <input type="email" class="form-control" id="exampleInputemail">
                    </div>
                </div>
              </div>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputPhonenumber">Phone Number</label>
                      <input type="text" class="form-control" id="exampleInputPhonenumber">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputmobilenumber">Mobile Number</label>
                      <input type="text" class="form-control" id="exampleInputmobilenumber">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputskype">SKype Number</label>
                      <input type="email" class="form-control" id="exampleInputskype">
                    </div>
                </div>
              </div>

              <div class="clear height-20"></div>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>Consultation Date & Time</b></label>
                      <div class="prelatife">
                      <input type="text" class="form-control datepicker" id="exampleInputconsdate">
                      </div>
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputmobilenumber">&nbsp;</label>
                      <input type="text" class="form-control" id="exampleInputmobilenumber" placeholder="Available Time">
                    </div>
                </div>
                <div class="col-md-4">
                </div>
              </div>

              <div class="clear height-10"></div>
              <p class="sub_title mb5">Procedures</p>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="exampleInputconsdate">Requested Procedures</label>
                      <div class="prelatife">
<?php 
$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
?>

                        <select name="#" id="" class="form-control selectpicker">
                          <?php foreach ($dataCategory as $key => $value): ?>
                            <?php if (count($value['children']) > 0): ?>
                            <optgroup label="<?php echo $value['title'] ?>">
                              <?php foreach ($value['children'] as $k => $v): ?>
                              <option value="<?php echo $v['id'] ?>"><?php echo $v['title'] ?></option>
                              <?php endforeach ?>
                            </optgroup>
                            <?php else: ?>
                            <option value="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></option>
                            <?php endif ?>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                </div>
                <div class="col-md-4">
                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate">Why you want the surgery, your expectations and desired outcome</label>
                      <div class="clear"></div>
                      <textarea name="" class="form-control" id="" rows="5"></textarea>
                    </div>
                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate">Please specify any other requirements</label>
                      <div class="clear"></div>
                      <textarea name="" class="form-control" id="" rows="5"></textarea>
                    </div>
                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>FAMILY MEDICAL CONDITIONS</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Heart Disease
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Diabetes
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Hypertension
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Asthma
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Cancer
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->

                      </div>
                    </div>

                </div>
              </div>
              
              <?php 
              $jpg_im = array(
                'Heart Disease',
                'Diabetes',
                'Hypertension',
                'Deep Vein Thrombosis',
                'Cardiovascular Accidents',
                'Asthma',
                'Bleeding Tendency',
                'Hyperthyriodism',
                'Adrenal Insufficiency',
                'Hepatitis',
                'HIV',
                'Keloid Scarrin',
                'Cancer',
                'Major Operation',
                'Underlying Disease',
                'Drug Allergies',
                'Food Allergies',
                'Current Medication & Dosage',
                'Current Vitamins, Food, Nutritional Support',
                );
              ?>
              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>PATIENT MEDICAL CONDITIONS</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <?php foreach ($jpg_im as $key => $value): ?>
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              <?php echo $value; ?>
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <?php endforeach ?>
                        
                      </div>
                    </div>

                </div>
              </div>
                
              <!-- Start append form -->
              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>PSYCHOLOGICAL CONDITIONS</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Depression
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Anxiety
                            </label>
                          </div>
                        </div>
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Bi-Polar
                            </label>
                          </div>
                        </div>
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Body dysmorphia
                            </label>
                          </div>
                        </div>
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Post Traumatic Stress disorder
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>HABITS</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Smoking
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="">
                              Drinking
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>WOMEN</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <div class="col-xs-12">
                          <label for="">Do you enjoying running and high intensity activity or contact sports?</label>
                        </div>
                        <div class="clear height-15"></div>
                        <div class="col-xs-15">
                          <div class="radio d-inline v-top padding-right-15 mt0">
                            <label>
                              <input type="radio" value="">
                              Yes
                            </label>
                          </div>
                          <div class="radio d-inline v-top mt0">
                            <label>
                              <input type="radio" value="">
                              No
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <div class="row default">
                        <div class="col-xs-12">
                          <label for="">Do you plan on having children in the future?</label>
                        </div>
                        <div class="clear height-15"></div>
                        <div class="col-xs-15">
                          <div class="radio d-inline v-top padding-right-15 mt0">
                            <label>
                              <input type="radio" value="">
                              Yes
                            </label>
                          </div>
                          <div class="radio d-inline v-top mt0">
                            <label>
                              <input type="radio" value="">
                              No
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <div class="row default">
                        <div class="col-xs-12">
                          <label for="">Do you have children and how many?</label>
                        </div>
                        <div class="clear height-15"></div>
                        <div class="col-xs-12">
                          <div class="col-xs-1">
                            <div class="radio d-inline v-top padding-right-15 mt0">
                              <label>
                                <input type="radio" value="">
                                Yes &nbsp;&nbsp;
                              </label>
                            </div>
                          </div>
                          <div class="col-xs-15">
                            <div class="form-group d-inline m-0">
                                <div class="input-group">
                                  <input type="text" class="form-control" id="exampleInputAmount" placeholder="Number">
                                  <div class="input-group-addon">Children</div>
                                </div>
                            </div>
                          </div>
                          <div class="clear"></div>
                          <div class="radio d-inline v-top mt0">
                            <label>
                              <input type="radio" value="">
                              No
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>
              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <div class="row default">
                        <div class="col-xs-12">
                          <label for="">Would you consider yourself a conservative person or do you flaunt your beauty?</label>
                        </div>
                        <div class="clear height-15"></div>
                        <div class="col-xs-15">
                          <div class="radio d-inline v-top padding-right-15 mt0">
                            <label>
                              <input type="radio" value="">
                              Conservative person
                            </label>
                          </div>
                        </div>
                        <div class="col-xs-15">
                          <div class="radio d-inline v-top mt0">
                            <label>
                              <input type="radio" value="">
                              Flaunt my beauty
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <div class="row default">
                        <div class="col-xs-12">
                          <label for=""><b>PLEASE REFER TO PHOTO</b></label>
                        </div>
                        <div class="clear height-15"></div>
                        <div class="col-xs-15">
                          <div class="form-group">
                            <label for="exampleInputFile">Front Photo</label>
                            <input type="file" id="exampleInputFile">
                          </div>
                        </div>
                        <div class="col-xs-15"></div>
                        <div class="col-xs-15">
                          <div class="form-group">
                            <label for="exampleInputFile">Side Photo</label>
                            <input type="file" id="exampleInputFile">
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear"></div>
              <!-- End append form -->

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="exampleInputconsdate"><b>PAYMENT METHOD</b></label>
                      <div class="clear"></div>
                      <div class="row default">
                        <div class="col-xs-15">
                          <div class="radio">
                            <label>
                              <input type="radio" value="">
                              <img src="<?php echo $this->assetBaseurl ?>logo-visa-mastercard-payment.png" alt="" class="img-responsive">
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                        <div class="col-xs-15">
                          <div class="radio">
                            <label>
                              <input type="radio" value="">
                              <img src="<?php echo $this->assetBaseurl ?>logo-paypal-payment.png" alt="" class="img-responsive">
                            </label>
                          </div>
                        </div>
                        <!-- end col xs -->
                      </div>
                    </div>

                </div>
              </div>

              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">Credit Card Number</label>
                      <div class="clear"></div>
                      <input type="text" class="form-control" placeholder="Credit Card Number">
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">Expiration date (MM/YY)</label>
                      <div class="clear"></div>
                      <div class="row">
                        <div class="col-xs-5">
                          <input type="text" class="form-control" placeholder="Month">
                        </div>
                        <div class="col-xs-2 text-center">
                          /
                        </div>
                        <div class="col-xs-5">
                          <input type="text" class="form-control" placeholder="Year">
                        </div>
                      </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">Security Code</label>
                      <div class="clear"></div>
                      <div class="row">
                        <div class="col-xs-5">
                          <input type="text" class="form-control" placeholder="Security Code">
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="text-left">
              <button type="submit" class="btn btn-default backs_btn_submit"></button>
              </div>
            </form>

            <div class="clear"></div>
          </div>
          <!-- end form coaching -->
          <div class="clear height-30"></div>


          <div class="clear"></div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <!-- end middle content back pattern -->

    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {
  $( ".datepicker" ).datepicker({
    showOn: "button",
    buttonImage: "<?php echo $this->assetBaseurl ?>icon-datepicker.png",
    buttonImageOnly: true,
    buttonText: "Select date"
  });
  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>