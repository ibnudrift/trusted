<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>
<?php $this->renderPartial('//mail/_template_header',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border: 1px solid #e7e7e7; border-top: 0px;">
                    <tbody>
                        <tr style="margin:0;padding:0">
                            <td colspan="3" style="margin:0;padding:0">
                            <p style="padding:0px 0px 5px;">
                            <img src="<?php echo $baseUrl ?>/asset/images/edm/pict-thankYou-mail.jpg" alt="picture mail" style="width: 100%;">
                            </p>
                            </td>
                        </tr>
                        <tr style="margin:0;padding:0">
                            <td style="margin:0;padding:0">
                            </td>
                            <td bgcolor="#FFFFFF" style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0;">
                                <!-- padding:30px 15px; border:1px solid #e7e7e7 -->
                                <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto; padding:30px 0px;">
                                    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
                                        <tbody>
                                            <tr style="margin:0;padding:0">
                                                <td style="margin:0;padding:0;font-family: Arial, sans-serif;">
                                                    <p style="font-weight:normal;font-size:18px;line-height:1.6;margin:0 0 20px;padding:0; text-align: center;">
                                                        <strong style="font-size:18px; text-align: center; font-weight: 400; text-transform: uppercase; letter-spacing: 2px; color: #215a8d; font-family: Times New Roman, serif;">Invoice Reference</strong>
                                                    </p>
                                                    <div style="margin-bottom:20px">

                                                        <div style="margin:0;padding:0">
															
                                                            <div style="margin:20px 0 0;padding:0">

                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Name
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->name ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Phone
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->phone ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Email:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->email ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Package
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->package ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Practice Address
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->address ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Website URL
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->website ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <p style="text-align: center; margin-bottom:20px;font-weight:normal;font-size:15px; font-family:arial, sans-serif; line-height:1.9; margin:35px 0 0 0;padding:0px 0 0 0; color:#565f64">Monitor the status of your order on the <a target="_blank" style="margin:0;padding:0;color:#235b8f;text-decoration:none; font-weight: 700;" href="<?php echo $url.CHtml::normalizeUrl(array('/member/order')); ?>">Order</a> page. 
                                                        <br style="margin:0;padding:0"><small style="color:#565f64; font-size: 12px; font-weight: 400px; font-family: arial, sans-serif;">This email is automatically created. Please do not post a reply to this email.</small></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td style="margin:0;padding:0">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
<?php $this->renderPartial('//mail/_template_footer',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
                