<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>
<?php $this->renderPartial('//mail/_template_header',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border: 1px solid #e7e7e7; border-top: 0px;">
                    <tbody>
                        <tr style="margin:0;padding:0">
                            <td colspan="3" style="margin:0;padding:0">
                            <p style="padding:0px 0px 5px;">
                            <img src="<?php echo $baseUrl ?>/asset/images/edm/pict-thankYou-mail.jpg" alt="picture mail" style="width: 100%;">
                            </p>
                            </td>
                        </tr>
                        <tr style="margin:0;padding:0">
                            <td style="margin:0;padding:0">
                            </td>
                            <td bgcolor="#FFFFFF" style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0;">
                                <!-- padding:30px 15px; border:1px solid #e7e7e7 -->
                                <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto; padding:30px 0px;">
                                    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0; font-size: 15px; font-family: arial, sans-serif;font-weight: 400;color:#565f64;">
                                        <tbody>
                                            <tr style="margin:0;padding:0">
                                                <td style="margin:0;padding:0">
                                                    <h5 style="line-height:24px;color:#215a8d; text-transform:uppercase; font-weight:400;font-size:17px;margin:0 0 18px;padding:0;font-family: 'times new roman', sans-serif;">Hello <b><?php echo ucwords($model->first_name).',' ?></b></h5>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 17px;padding:0">Thank you for contacting us, we love hearing from patients!</p>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 17px;padding:0">Please do not hesitate to call us on 1300 883 164.</p>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 17px;padding:0">To keep up to date on all the latest tricks and trends join our facebook page @TrustedSurgeons and our Instagram page @Trusted.Surgeons.<br />
                                                    Please keep in mind we are worth the wait, not all surgeons are Trusted Surgeons. In Australia, any registered medical practitioner can use the title &ldquo;Cosmetic Surgeon&rdquo;.</p>
                                                    
                                                    <p style="margin-bottom:17px;font-weight:normal;font-size:13px;line-height:1.6;margin:30px 0 0 0;padding:10px 0 0 0;border-top:3px solid #dbdbdb">
                                                    <small style="color:#999">Chat soon,<br>
                                                    Team TS xo</small></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td style="margin:0;padding:0">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
<?php $this->renderPartial('//mail/_template_footer',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
