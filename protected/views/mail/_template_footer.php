<table cellspacing="0" cellpadding="5" border="0" width="700" style="border:1px solid #373435">
                    <tbody>
                        <tr>
                            <td valign="middle" style="background-color:#235b8f">
                                <div style="text-align:center; padding: 7px 0;">
                                    <?php if ($this->setting['url_facebook']): ?>
                                    <a href="<?php echo $this->setting['url_facebook'] ?>"><img style="margin:0!important;max-width: 35px;" src="<?php echo $baseUrl ?>/asset/images/ic-facebook.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_twitter']): ?>
                                    <a href="<?php echo $this->setting['url_twitter'] ?>"><img style="margin:0!important;max-width: 35px;" src="<?php echo $baseUrl ?>/asset/images/ic-twitter.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_linkedin']): ?>
                                    <a href="<?php echo $this->setting['url_linkedin'] ?>"><img style="margin:0!important;max-width: 35px;" src="<?php echo $baseUrl ?>/asset/images/ic-linkedin.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_instagram']): ?>
                                    <a href="<?php echo $this->setting['url_instagram'] ?>"><img style="margin:0!important;max-width: 35px;" src="<?php echo $baseUrl ?>/asset/images/ic-instagram.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_pinterest']): ?>
                                    <a href="<?php echo $this->setting['url_pinterest'] ?>"><img style="margin:0!important;max-width: 35px;" src="<?php echo $baseUrl ?>/asset/images/ic-pinterest.png"></a>
                                    <?php endif ?>

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
<table cellspacing="0" cellpadding="0" border="0" width="700" style="border:1px solid #e7e7e7;">
                    <tbody>
                        <tr>
                            <td align="center" valign="top">

                                <table cellspacing="0" cellpadding="10" border="0" width="700">
                                    <tbody>
                                        <tr>
                                            <td valign="top">

                                                <table cellspacing="0" cellpadding="10" border="0" width="100%" style="color:#505050">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" style="font-family:Verdana,helvetica,arial;font-size:13px;padding-left:40px;">
                                                                    <div style="height: 21px;"></div>
                                                                    <b style="color: #235b8f;font-weight:700;">Contact Us</b>
                                                                    <br>
                                                                    <table style="font-family:Verdana,helvetica,arial;font-size:13px;color: #565f64;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td><b>Phone</b></td>
                                                                                <td>&nbsp;</td>
                                                                                <td><a style="color: #565f64;text-decoration:none;" target="_blank" href="mailto:<?php echo $this->setting['contact_phone'] ?>"><?php echo $this->setting['contact_phone'] ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><b>Email</b></td>
                                                                                <td>&nbsp;</td>
                                                                                <td><a style="color: #565f64;text-decoration:none;" target="_blank" href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></td>
                                                                            </tr>
                                                                            <!-- <tr>
                                                                                <td style="width:150px">CS For Patients</td>
                                                                                <td style="width:10px">:</td>
                                                                                <td><?php echo $this->setting['contact_phone_patiens'] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width:150px">CS For Surgeons</td>
                                                                                <td style="width:10px">:</td>
                                                                                <td><?php echo $this->setting['contact_phone_surgeons'] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email For Patients</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['contact_email_patiens'] ?>"><?php echo $this->setting['contact_email_patiens'] ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email For Surgeons</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['contact_email_surgeons'] ?>"><?php echo $this->setting['contact_email_surgeons'] ?></a></td>
                                                                            </tr> -->
                                                                        </tbody>
                                                                    </table>
                                                                    
                                                                </div>
                                                            </td>
                                                            <td valign="top" style="font-family:Verdana,helvetica,arial;font-size:12px;float: right;">
                                                                <table style="font-family:Verdana,helvetica,arial;font-size:12px">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="float: right; text-align: center;padding-right:25px;">
                                                                                    <a href="<?php echo $url ?>">
                                                                                        <img src="<?php echo $baseUrl ?>/asset/images/edm/logo_footersn_newsn_mail.jpg">
                                                                                    </a>
                                                                                    <?php /*
                                                                                    <p style="color: #565f64;">
                                                                                        <strong>TRUSTED SURGEONS</strong> <br>
                                                                                        <?php // echo nl2br($this->setting['contact_address']) ?>
                                                                                    </p>*/
                                                                                     ?>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>
                    </tbody>
                </table>


                <br>
            </td>
        </tr>
    </tbody>
</table>