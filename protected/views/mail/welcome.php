<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>

<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
    <tbody>
        <tr>
            <td align="center" valign="top">

                <table cellspacing="0" cellpadding="5" border="0" width="700" style="border:1px solid #7ecdea">
                    <tbody>
                        <tr>
                            <td valign="top" style="background-color:#ffffff; text-align:center">
                                <img src="<?php echo $baseUrl ?>/asset/images/logo_header_trusted_surgeons.jpg">
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border:1px solid #666">
                    <tbody>
                        <tr style="margin:0;padding:0">
                            <td style="margin:0;padding:0">
                            </td>
                            <td bgcolor="#FFFFFF" style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">

                                <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;padding:20px 10px;">
                                    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
                                        <tbody>
                                            <tr style="margin:0;padding:0">
                                                <td style="margin:0;padding:0">
                                                    <h5 style="line-height:24px;color:#000;font-weight:900;font-size:17px;margin:0 0 20px;padding:0">Dear <?php echo ucwords($model->first_name).' '.ucwords($model->last_name).',' ?></h5>

                                                    <p style="font-weight:normal;font-size:13px;line-height:1.6;margin:0 0 20px;padding:0">Thank you for joining Trusted Surgeons. Your account has been activated and you can login in our member area by using the following details.</p>

                                                    <p style="font-weight:normal;font-size:13px;line-height:1.6;margin:0 0 20px;padding:0">
                                                    <?php echo ucwords($model->first_name).' '.ucwords($model->last_name); ?><br>
                                                    <?php echo $model->email; ?>
                                                    </p>

                                                    <p style="font-weight:normal;font-size:13px;line-height:1.6;margin:0 0 20px;padding:0">Trusted Surgeons only list qualified plastic surgeons who have the experience and qualifications you deserve. Not all surgeons are qualified to perform cosmetic surgery but within Australia this is legal. </p>
                                                    <p style="font-weight:normal;font-size:13px;line-height:1.6;margin:0 0 20px;padding:0">Having surgery can be daunting, please do not hesitate to call us on 1300 883 164 or write to us at info@trustedsurgeons.com.au</p>
                                                    <p style="font-weight:normal;font-size:13px;line-height:1.6;margin:0 0 20px;padding:0">In the meantime, please find our closed group on facebook @trustedsurgeonssupport and our pages @trustedsurgeons and @boobiesupplies. On Instgram we are @trusted.surgeons and #trustedsurgeons and #trustinus</p>

                                                    

                                                    <p style="margin-bottom:20px;font-weight:normal;font-size:14px;line-height:1.6;margin:30px 0 0 0;padding:10px 0 0 0;border-top:3px solid #d0d0d0">
                                                    Welcome to our community!<br><small style="color:#999">Warmest regards,<br>
                                                    Team TS xo</small></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td style="margin:0;padding:0">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" cellpadding="5" border="0" width="700" style="border:1px solid #7ecdea">
                    <tbody>
                        <tr>
                            <td valign="middle" style="background-color:#225b8f">
                                <div style="text-align:center">
                                    <?php if ($this->setting['url_facebook']): ?>
                                    <a href="<?php echo $this->setting['url_facebook'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-facebook.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_twitter']): ?>
                                    <a href="<?php echo $this->setting['url_twitter'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-twitter.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_linkedin']): ?>
                                    <a href="<?php echo $this->setting['url_linkedin'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-linkedin.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_instagram']): ?>
                                    <a href="<?php echo $this->setting['url_instagram'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-instagram.png"></a>
                                    <?php endif ?>
                                    <?php if ($this->setting['url_pinterest']): ?>
                                    <a href="<?php echo $this->setting['url_pinterest'] ?>"><img style="margin:0!important" src="<?php echo $baseUrl ?>/asset/images/ic-pinterest.png"></a>
                                    <?php endif ?>

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border:1px solid #666">
                    <tbody>
                        <tr>
                            <td align="center" valign="top">

                                <table cellspacing="0" cellpadding="10" border="0" width="700">
                                    <tbody>
                                        <tr>
                                            <td valign="top">

                                                <table cellspacing="0" cellpadding="10" border="0" width="100%" style="color:#505050">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" style="font-family:Verdana,helvetica,arial;font-size:12px">
                                                                    <b style="color: #f58634;">Customer Service:</b>
                                                                    <br>
                                                                    <table style="font-family:Verdana,helvetica,arial;font-size:12px">
                                                                        <tbody>
                                                                            <!-- <tr>
                                                                                <td style="width:150px">CS For Patients</td>
                                                                                <td style="width:10px">:</td>
                                                                                <td><?php echo $this->setting['contact_phone_patiens'] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width:150px">CS For Surgeons</td>
                                                                                <td style="width:10px">:</td>
                                                                                <td><?php echo $this->setting['contact_phone_surgeons'] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email For Patients</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['contact_email_patiens'] ?>"><?php echo $this->setting['contact_email_patiens'] ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email For Surgeons</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['contact_email_surgeons'] ?>"><?php echo $this->setting['contact_email_surgeons'] ?></a></td>
                                                                            </tr> -->
                                                                            <tr>
                                                                                <td>Phone</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['contact_phone'] ?>"><?php echo $this->setting['contact_phone'] ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email</td>
                                                                                <td>:</td>
                                                                                <td><a target="_blank" href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <br>
                                                                    <table style="font-family:Verdana,helvetica,arial;font-size:12px">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="width:200px">
                                                                                    <a href="<?php echo $url ?>">
                                                                                        <img src="<?php echo $baseUrl ?>/asset/images/logo_header_trusted_surgeons.jpg">
                                                                                    </a>
                                                                                </td>
                                                                                <td style="text-align:right" width="100%">
                                                                                    <p>
                                                                                        <strong>TRUSTED SURGEONS</strong> <br>

                                                                                        <?php echo nl2br($this->setting['contact_address']) ?>
                                                                                    </p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>
                    </tbody>
                </table>


                <br>
            </td>
        </tr>
    </tbody>
</table>