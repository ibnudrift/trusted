<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>
<?php $this->renderPartial('//mail/_template_header',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border: 1px solid #e7e7e7; border-top: 0px;">
                    <tbody>
                        <tr style="margin:0;padding:0">
                            <td colspan="3" style="margin:0;padding:0">
                            <p style="padding:0px 0px 5px;">
                            <img src="<?php echo $baseUrl ?>/asset/images/edm/pict-thankYou-mail.jpg" alt="picture mail" style="width: 100%;">
                            </p>
                            </td>
                        </tr>
                        <tr style="margin:0;padding:0">
                            <td style="margin:0;padding:0">
                            </td>
                            <td bgcolor="#FFFFFF" style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0;">
                                <!-- padding:30px 15px; border:1px solid #e7e7e7 -->
                                <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto; padding:30px 0px;">
                                    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0; font-size: 15px; font-family: arial, sans-serif;font-weight: 400;color:#565f64;">
                                        <tbody>
                                            <tr style="margin:0;padding:0">
                                                <td style="margin:0;padding:0">
                                                    <h5 style="line-height:24px;color:#215a8d; text-transform:uppercase; font-weight:400;font-size:17px;margin:0 0 18px;padding:0;font-family: 'times new roman', sans-serif;">Dear <b><?php echo ucwords($model->name).',' ?></b></h5>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 17px;padding:0">Thank you for your enquiry!</p>
                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 17px;padding:0">We would love to have you as part of our Trusted Surgeon community. Please keep in mind we only list Specialist Plastic Surgeons who are members of;</p>
                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 17px;padding:0">The Australian Society of Plastic Surgery</p>
                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 17px;padding:0">And the Australasian Society of Aesthetic Plastic Surgery.<br />
                                                    We look forward to discussing with you, what we do for the industry and what we can do for your practice.</p>

                                                    <p style="margin-bottom:17px;font-weight:normal;font-size:13px;line-height:1.6;margin:30px 0 0 0;padding:10px 0 0 0;border-top:3px solid #dbdbdb"><small style="color:#999">Warmest Regards<br>
                                                    The Trusted Surgeons Team</small></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td style="margin:0;padding:0">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
<?php $this->renderPartial('//mail/_template_footer',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
