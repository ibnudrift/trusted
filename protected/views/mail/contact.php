<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>
<?php $this->renderPartial('//mail/_template_header',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border: 1px solid #e7e7e7; border-top: 0px;">
                    <tbody>
                        <tr style="margin:0;padding:0">
                            <td colspan="3" style="margin:0;padding:0">
                            <p style="padding:0px 0px 5px;">
                            <img src="<?php echo $baseUrl ?>/asset/images/edm/pict-thankYou-mail.jpg" alt="picture mail" style="width: 100%;">
                            </p>
                            </td>
                        </tr>
                        <tr style="margin:0;padding:0">
                            <td style="margin:0;padding:0">
                            </td>
                            <td bgcolor="#FFFFFF" style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0;">
                                <!-- padding:30px 15px; border:1px solid #e7e7e7 -->
                                <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto; padding:30px 0px;">
                                    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0">
                                        <tbody>
                                            <tr style="margin:0;padding:0">
                                                <td style="margin:0;padding:0;font-family: Arial, sans-serif;">
                                                    <p style="font-weight:normal;font-size:18px;line-height:1.6;margin:0 0 20px;padding:0; text-align: center;">
                                                        <strong style="font-size:18px; text-align: center; font-weight: 400; text-transform: uppercase; letter-spacing: 2px; color: #215a8d; font-family: Times New Roman, serif;">Detail Contact</strong>
                                                    </p>
                                                    <div style="margin-bottom:20px">

                                                        <div style="margin:0;padding:0">
															
                                                            <div style="margin:20px 0 0;padding:0">

                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">First Name:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->first_name; ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Last Name:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->last_name; ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Phone Number:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->phone; ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Email:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->email ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                                <?php if ($model->procedures): ?>
                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Procedure:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->procedures; ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <?php endif; ?>

                                                                <?php if ($model->coaching): ?>
                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Coaching:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo $model->coaching; ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <?php endif; ?>

                                                                <div style="border-bottom-width:1px;border-bottom-color:#dbdbdb;border-bottom-style:solid;margin:0;padding:0">
                                                                </div>
                                                                <table bgcolor="transparent" style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0; color:#565f64;">
                                                                    <tbody style="margin:0;padding:0">
                                                                        <tr style="margin:0;padding:0">
                                                                            <td valign="top" style="width:50%;font-weight:700;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0">Message:
                                                                            </td>
                                                                            <td align="right" valign="top" style="text-align:right;font-size:14px;vertical-align:top;line-height:22px;margin:0;padding:0 10px 0 0;font-weight: 700;"><?php echo nl2br($model->body); ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>     

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p>&nbsp;</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td style="margin:0;padding:0">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
<?php $this->renderPartial('//mail/_template_footer',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
