<?php
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
$url = Yii::app()->request->hostInfo;
?>
<?php $this->renderPartial('//mail/_template_header',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>
                <table cellspacing="0" cellpadding="0" border="0" width="700" style="border: 1px solid #e7e7e7; border-top: 0px;">
                    <tbody>
                        <tr style="margin:0;padding:0">
                            <td colspan="3" style="margin:0;padding:0">
                            <p style="padding:0px 0px 5px;">
                            <img src="<?php echo $baseUrl ?>/asset/images/edm/pict-thankYou-mail.jpg" alt="picture mail" style="width: 100%;">
                            </p>
                            </td>
                        </tr>
                        <tr style="margin:0;padding:0">
                            <td style="margin:0;padding:0">
                            </td>
                            <td bgcolor="#FFFFFF" style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0;">
                                <!-- padding:30px 15px; border:1px solid #e7e7e7 -->
                                <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto; padding:30px 0px;">
                                    <table bgcolor="transparent" style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0; font-size: 15px; font-family: arial, sans-serif;font-weight: 400;color:#565f64;">
                                        <tbody>
                                            <tr style="margin:0;padding:0">
                                                <td style="margin:0;padding:0">
                                                    <h5 style="line-height:24px;color:#215a8d; text-transform:uppercase; font-weight:400;font-size:17px;margin:0 0 18px;padding:0;font-family: 'times new roman', sans-serif;">Congratulations! Not only are you a Trusted Surgeon but you have a sought-after position. Our <b>Platinum Packages</b> are very limited!</h5>
                                                    
                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">We only offer 2 positions per state per category (Face, Breast, Body).</p>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">You certainly will not be short on exposure, we will promote you on all our social platforms reaching over 70 000 followers. We design all our posts to emulate your branding and clinic, whilst keeping up to date with latest trends. We will utilise the hashtag #trustindr(and your surname) along with trending industry hashtags.</p>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">Whilst we ensure you will reach a minimum of 1000 targeted users per month we will look at ways we can help improve conversions. Adding us to your consult form pre-surgery is a great way to gain insight on patient referrals and encouraging patients to share their story with us is fantastic free PR.</p>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">Let&rsquo;s get started to promote your presence online as a Trusted Surgeon! &nbsp;</p>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">Once your Ezidebit payment has been successful. We will design a few banner options for you, set up a profile and assist any media uploading and account access to edit your profile.</p>

                                                    <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">Thank you for your support and helping us on our mission.</p>


                                                    <p style="margin-bottom:20px;font-weight:normal;font-size:13px;line-height:1.6;margin:30px 0 0 0;padding:10px 0 0 0;border-top:3px solid #dbdbdb"><small style="color:#999">Warmest regards<br>The Trusted Surgeons Team</small></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td style="margin:0;padding:0">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
<?php $this->renderPartial('//mail/_template_footer',array('baseUrl'=>$baseUrl, 'url'=>$url)); ?>