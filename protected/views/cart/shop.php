<?php
$session = new CHttpSession;
$session->open();
$order = $session['order'];

// print_r($order);
// exit;
?>
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_outers_cart_content">
        <h1 class="title_page">MY BAG</h1>
        <div class="clear height-40"></div> <div class="clear height-5"></div>
        <div class="clear"></div>
        
    <div class="product-list-warp">
        <div id="cart-shop">
<?php if (count($data)>0): ?>
        <div class="blocks_subtitle">
          <span>ORDER REVIEW</span>
        </div>
        <div class="clear height-5"></div>
        <div class="box_table_cart">
          <table class="table table-hover tables_cart table-striped">
            <thead>
              <tr>
                <th>PRODUCT</th>
                <th>ONLINE AVAILABILITY</th>
                <th>QUANTITY</th>
                <th class="text-right">UNIT PRICE</th>
                <th class="text-right">TOTAL PRICE</th>
              </tr>
            </thead>
            <tbody>
            <?php $total = 0 ?>
            <?php $weight = 0 ?>
            <?php foreach ($data as $key => $value): ?>
            <?php
                    if ($value['option'] != '') {
                        $option = PrdProductAttributes::model()->find('id_str = :id_str', array(':id_str'=>$value['option']));
                        $value['price'] = $option->price;
                    }
                    $weightItem = PrdProduct::model()->findByPk($value['id'])->berat;
            ?>
              <tr>
                <td>
                  <div class="text_cpicture">
                    <div class="pictures fleft">
                      <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(150,150, '/images/product/'.$value['image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                    </div>
                    <div class="descs fleft padding-left-30">
                      <div class="chl mh110">
                        <span class="names"><?php echo $value['name'] ?></span>
                      </div>
                      <a href="#" class="btn btn-default btn-link delete_cart btn-delete-cart"><i class="fa fa-times-circle"></i> &nbsp;Remove Product</a>
                    </div>
                  </div>
                </td>
                <td>
                  <i class="fa fa-check purple_t"></i>&nbsp; In Stock
                </td>
                <td>
                    <form action="<?php echo CHtml::normalizeUrl(array('/product/edit')); ?>" method="post"> 
                        <input type="hidden" value="<?php echo $value['id'] ?>" name="id"> 
                        <?php if (count($value['optional']) > 0 AND $value['optional'] != ''): ?> 
                            <?php foreach ($value['optional'] as $k => $v): ?> 
                            <input type="hidden" value="<?php echo $v ?>" name="option[<?php echo $k ?>]"> 
                            <?php endforeach ?> 
                        <?php endif ?> 
                        <!-- <span class="quantity"><?php echo $value['qty'] ?> Item(s) <a href="#" class="btn-edit-cart">Edit</a></span>  -->
                        <div class="c_input_qty"><input type="number" class="form-control select_qty" name="qty" value="<?php echo $value['qty'] ?>"></div>
                    </form>
                </td>
                <td class="text-right">
                  <?php echo Cart::money($value['price']) ?>
                </td>
                <td class="text-right">
                  <strong><?php echo Cart::money($subTotal = $value['price'] * $value['qty']) ?></strong>
                </td>
              </tr>
            <?php $total = $total + $subTotal ?>
            <?php $weight = ($weightItem*$value['qty']) + $weight ?>
            <?php endforeach ?>
            </tbody>
          </table>
          <div class="clear"></div>
        

        <div class="clear"></div>
        <div class="fright mw445">
          <div class="promo_code_bx">
            <form action="<?php echo CHtml::normalizeUrl(array('discount')); ?>" id="form-promo">
            <?php if(Yii::app()->user->hasFlash('success')): ?>
            
                <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'alerts'=>array('success'),
                )); ?>
            
            <?php endif; ?>
            <?php if(Yii::app()->user->hasFlash('danger')): ?>
            
                <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'alerts'=>array('danger'),
                )); ?>
            
            <?php endif; ?>
            <table class="table table-noborder">
              <tr class="top_inp_promocode">
                <td>
                  <label for=""><b>PROMO CODE</b></label>
                </td>
                <td>
                  <input type="text" class="form-control" id="discount" name="discount" value="<?php echo $order['promo_kode'] ?>">
                </td>
                <td>
                  <button class="btn btn-default btns_aplyr btn-discount">APPLY</button>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="right">
                  <div class="text-right">
                    Order Subtotal
                  </div>
                </td>
                <td>
                  <div class="text-right"><?php echo Cart::money($total) ?></div>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="right">
                  <div class="text-right">
                    Promotion Discount
                  </div>
                </td>
                <td>
                  <?php if ($order['discount_type'] == 0 ): ?>
                  <div class="text-right"><?php echo Cart::money($order['discount']) ?></div>
                  <?php $total = $total - $order['discount']; ?>
                  <?php elseif($order['discount_type'] == 1): ?>
                  <div class="text-right"><?php echo Cart::money(($order['discount']/100)*$total) ?></div>
                  <?php $total = $total - ($order['discount']/100)*$total; ?>
                  <?php endif ?>
                </td>
              </tr>
              <tr class="bb_1">
                <td colspan="2" align="right">
                  <div class="text-right"><label><b>Order Total Excluding Delivery</b></label></div>
                </td>
                <td>
                  <div class="text-right">
                    <span class="price_total"><?php echo Cart::money($total) ?></span>
                  </div>
                </td>
              </tr>
            </table>
            </form>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
        </div>
        </div>
<?php else: ?>
<div class="padding-32">
    <h3>Shopping cart is empty</h3>
    <div class="height-50"></div>
    <div class="height-50"></div>
    <div class="height-50"></div>
    <div class="height-50"></div>
    <div class="clear height-30"></div>
</div>
<?php endif ?>
        </div>
    </div>
        <div class="clear"></div>
        <!-- End promo code bottom Shoppin Cart -->
        <div class="clear height-15"></div>
        <div class="height-2"></div>

        <div class="row">
          <div class="col-md-6">
            <div class="text-left">
            <button onclick="window.location.href = '<?php echo CHtml::normalizeUrl(array('/shop/index')); ?>'" class="btn btn-default bt_purpleshopdt bt_continue_shop"><i class="fa fa-chevron-left"></i> &nbsp;CONTINUE SHOPPING</button>
            </div>
          </div>
          <div class="col-md-6">
            <?php if (count($data)>0): ?>
            <?php if (isset($session['login_member'])): ?>
              
            <div class="text-right">
              <button onclick="window.location.href = '<?php echo CHtml::normalizeUrl(array('/cart/payment')); ?>'" class="btn btn-default bt_purpleshopdt bt_checkout_shop">CHECK OUT SECURELY</button>
            </div>
            <?php else: ?>
            <div class="text-right">
              <button onclick="window.location.href = '<?php echo CHtml::normalizeUrl(array('/cart/checkout')); ?>'" class="btn btn-default bt_purpleshopdt bt_checkout_shop">CHECK OUT SECURELY</button>
            </div>
            <?php endif ?>
            <?php endif ?>
          </div>
        </div>
        
        <!-- <div class="blocks_subtitle b2">
          <span>DELIVERY ADDRESS</span>
        </div>
        <div class="clear height-35"></div> -->

        <div class="clear height-20"></div>
        <!-- end page -->
        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>
<script type="text/javascript">
    // $('.btn-edit-cart').live('click', function() {
    //     obj = $(this).parent().parent();
    //     obj.find('.quantity').html(''+
    //     '<select name="qty" class="span1 select_qty">'+
    //     '   <option value="">Quantity</option>'+
    //     '   <?php for ($i=1; $i <= 20; $i++) { ?>'+
    //     '   <option value="<?php echo $i ?>"><?php echo $i ?></option>'+
    //     '   <?php } ?>'+
    //     '</select>');
    //     return false;
    // })
    $('.btn-delete-cart').live('click', function() {
        var data = $(this).parent().parent().parent().parent().find('form').serialize();
        // console.log(data);
        // return false;
        $.ajax({
            url: url_edit_cart_action,
            data: data+'&ajax=ajax&qty=0',
            dataType: 'html',
            type: 'post',
            success: function(msg){
                $( ".product-list-warp" ).load( baseurl+"/cart/shop #cart-shop" );
            },
            error: function(msg){
                alert('sending data error, cek your connection');
                console.log(msg);
            }
        });
        return false;
    })
    $('.select_qty').live('change', function() {
        var data = $(this).parent().parent().parent().find('form').serialize();
        $.ajax({
            url: url_edit_cart_action,
            data: data+'&ajax=ajax',
            dataType: 'html',
            type: 'post',
            success: function(msg){
                $( ".product-list-warp" ).load( baseurl+"/cart/shop #cart-shop" );
            },
            error: function(msg){
                alert('sending data error, cek your connection');
                console.log(msg);
            }
        });
        return false;
    })
</script>
<script type="text/javascript">
    $('.btn-discount').live('click', function() {
        $.ajax({
            url: $('#form-promo').attr('action'),
            data: {'discount': $('#discount').val()},
            dataType: 'html',
            type: 'post',
            success: function(msg){
                $( ".product-list-warp" ).load( baseurl+"/cart/shop #cart-shop" );
            },
            error: function(msg){
                alert('sending data error, cek your connection');
                console.log(msg);
            }
        });
        return false;
    })

</script>