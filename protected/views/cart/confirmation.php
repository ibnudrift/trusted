<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_outers_cart_content">
        <h1 class="title_page">Confirmation</h1>
        <div class="clear height-40"></div> <div class="clear height-5"></div>
        <div class="clear"></div>


					<div class="inside-content">
						
						<!-- /. Start Content About -->
						<div class="m-ins-content detail-shopcart content-text">
							
								
							<?php if ($modelOrder->payment_method_id == 2): ?>
							<p><b>Pay With</b> <br>
								<a href="<?php echo CHtml::normalizeUrl(array('/paypal/pay', 'id'=>$modelOrder->id)); ?>">
									<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/paypal.jpg" alt="">
								</a>
							</p>
							<?php endif ?>
							<h4>Order ID: <?php echo $modelOrder->invoice_prefix ?>-<?php echo $modelOrder->invoice_no ?></h4>
							<div class="lines-green"></div>
							<div class="row">
								<div class="col-md-6">
									<h1 class="title-inside-page">Confirmation</h1>
									<p>Thank you for placing your order with Trusted Surgeons! <br>
									We will send out a confirmation email as soon as your payment will be received.</p>
								</div>
								<div class="col-md-6">
									<b>Shipping address</b><br>
									<?php echo $modelOrder->shipping_first_name ?> <?php echo $modelOrder->shipping_last_name ?><br>
									<?php echo $modelOrder->shipping_address_1 ?><br>
									<?php echo $modelOrder->shipping_city ?>,
									<?php echo $modelOrder->shipping_zone ?>, <?php echo $modelOrder->shipping_postcode ?><br>
									Mobile phone : <?php echo $modelOrder->phone ?><br>
								</div>
							</div>
							<div class="height-10"></div>

					        <div class="blocks_subtitle">
					          <span>ORDER REVIEW</span>
					        </div>
							<div class="box_table_cart">
							    <table class="table table-hover tables_cart table-striped">
							    	<thead>
							    		<tr>
							    			<td>PRODUCT</td>
							    			<!-- <td>Option</td> -->
							    			<td width="15%">Quantity</td>
							    			<td width="15%"><b>Total</b></td>
							    		</tr>
							    	</thead>
							    	<tbody>
							    		<?php $total = 0 ?>
							    		<?php foreach ($data as $key => $value): ?>
							    		<tr>
							    			<td>
							                  <div class="text_cpicture">
							                    <div class="pictures fleft">
							                      <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(150,150, '/images/product/'.$value['image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive">
							                    </div>
							                    <div class="descs fleft padding-left-30">
							                      <div class="chl mh110">
							                        <span class="names"><?php echo $value['name'] ?></span>
							                      </div>
							                    </div>
							                  </div>
							    			</td>
							    			<?php /*
							    			<td>
												<?php
												$totalOption = 0;
												$value['option'] = unserialize($value['option']);
												?>
												<?php if (count($value['option']) > 0 AND $value['option'] != ''): ?>
													<?php foreach ($value['option'] as $k => $v): ?>
													<?php
													$dataOption = explode('|', $v);
													?>
													<span class="varian"><?php echo $dataOption[1] ?> $<?php echo number_format($dataOption[2]) ?></span><br>
													<?php $totalOption = $totalOption + $dataOption[2]; ?>
													<?php endforeach ?>
												<?php endif ?>
							    			</td>
							    			*/ ?>
							    			<td>
							    				<form action="<?php echo CHtml::normalizeUrl(array('/product/edit')); ?>" method="post">
							    					<input type="hidden" value="<?php echo $value['id'] ?>" name="product_id">
							    					<?php /*
							    					<input type="hidden" value="<?php echo $value['option'] ?>" name="option">
							    					*/ ?>
								    				<span class="quantity"><?php echo $value['qty'] ?> Item(s)</span>
							    				</form>
							    			</td>
							    			<td>
							    				<b><?php echo Cart::money($subTotal = ($value['price']+$totalOption) * $value['qty']) ?></b>
							    			</td>
							    		</tr>
							    		<?php $total = $total + $subTotal ?>
							    		<?php endforeach ?>
							    	</tbody>
								</table>

						        <div class="clear"></div>
						        <div class="fright mw445">
						          <div class="promo_code_bx">
						            <table class="table table-noborder">
						              <tr>
						                <td colspan="2" align="right">
						                  <div class="text-right">
						                    Order Subtotal
						                  </div>
						                </td>
						                <td>
						                  <div class="text-right"><?php echo Cart::money($order['total']) ?></div>
						                </td>
						              </tr>
						              <tr>
						                <td colspan="2" align="right">
						                  <div class="text-right">
						                    Promotion Discount
						                  </div>
						                </td>
						                <td>
						                  <?php if ($order['discount_type'] == 0 ): ?>
						                  <div class="text-right"><?php echo Cart::money($order['discount']) ?></div>
						                  <?php $total = $total - $order['discount']; ?>
						                  <?php elseif($order['discount_type'] == 1): ?>
						                  <div class="text-right"><?php echo Cart::money(($order['discount']/100)*$total) ?></div>
						                  <?php $total = $total - ($order['discount']/100)*$total; ?>
						                  <?php endif ?>
						                </td>
						              </tr>
						              <tr class="bb_1">
						                <td colspan="2" align="right">
						                  <div class="text-right"><label><b>Order Total Excluding Delivery</b></label></div>
						                </td>
						                <td>
						                  <div class="text-right">
						                    <span class="price_total"><?php echo Cart::money($order['grand_total']) ?></span>
						                  </div>
						                </td>
						              </tr>
						            </table>
						            <div class="clear"></div>
						          </div>
						          <div class="clear"></div>
						        </div>





							</div>
							<div class="clear"></div>
					        <div class="row">
					          <div class="col-md-6">
					            <div class="text-left">
					            <button onclick="window.location.href = '<?php echo CHtml::normalizeUrl(array('/member/index')); ?>'" class="btn btn-default bt_purpleshopdt bt_continue_shop"><i class="fa fa-chevron-left"></i> &nbsp;view My Account</button>
					            </div>
					          </div>
					          <div class="col-md-6">
					          </div>
					        </div>


							<div class="clear height-25"></div>
							<div class="clear"></div>
						</div>
						<!-- /. End Content About -->

						<div class="clear height-15"></div>


					<div class="clear"></div>
					</div>
					<div class="clear"></div>



        <div class="clear height-20"></div>
        <!-- end page -->
        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>