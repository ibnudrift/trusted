<section class="top-content-inside about">
    <div class="container">
        <div class="titlepage-Inside">
            <h1>e-STORE</h1>
        </div>
    </div>
    <div class="celar"></div>
</section>
<section class="middle-content">
    <div class="prelatife container">
        
        <div class="clear height-20"></div>
        <div class="height-3"></div>
        <div class="prelatife">
            <div class="box-featured-latestproduct">
                <div class="box-title">
                    <div class="titlebox-featured" alt="title-product"><?php echo $data->description->name ?></div>
                    <div class="fright back-button-productyellow"><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$data->category_id)); ?>" onclick="window.history.back()"></a></div>
                    <div class="clear"></div>
                </div>
                <div class="box-product-detailg">
                    <div class="clear height-25"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-picture-big">
                                <div class="in">
                                    <img class="img-main" style="display: inline-block;" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(513,513, '/images/product/'.$data->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="<?php echo $data->description->name ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 descriptions-product">
                            <form action="<?php echo CHtml::normalizeUrl(array('/product/addcart')); ?>" method="post">
                            <div class="padding-left-10 padding-right-20 border-product-detail">
                                <div class="clear height-10"></div>
                                <div class="height-50"></div>
                                <div class="height-50"></div>
                                <div class="height-25"></div>
                                <b>Price</b> <br>
                                <span class="price"><?php echo Cart::money($data->harga) ?></span>
                                <?php if ($data->harga < $data->harga_coret): ?>
                                <span class="price-coret"><?php echo Cart::money($data->harga_coret) ?></span>
                                <?php endif ?>
                                <br><br>
                                <?php if ($data->out_stock == 0): ?>
                                <b>Quantity</b> <br>
                                <select name="qty" id="" class="form-control product-detail">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                </select>
                                <span class="help-inline">pc(s)</span> <br><br>
                                <?php endif ?>

                                <div class="status-item">
                                    <?php if ($data->out_stock == 1): ?>
                                      <button type="button" class="btn btn-primary btn-xs btn-danger">Out of Stock</button>
                                    <?php endif ?>
                                    <?php if ($data->terbaru == 1): ?>
                                      <button type="button" class="btn btn-primary btn-xs btn-success">Special</button>
                                    <?php endif ?>
                                    <?php if ($data->terlaris == 1): ?>
                                      <button type="button" class="btn btn-primary btn-xs btn-warning">On-order Only</button>
                                    <?php endif ?>
                                </div>
                                <br>
                                <?php if ($data->out_stock == 0): ?>
                                <b>Product Variations</b> <br>
<?php
$option = PrdProductAttributes::model()->findAll('product_id = :product_id', array(':product_id'=>$data->id));
?>
                                <select name="option" id="" class="form-control product-detail2">
                                    <option value="">Default</option>
                                    <?php foreach ($option as $key => $value): ?>
                                    <option value="<?php echo $value->id ?>"><?php echo $value->attribute ?> (<?php echo Cart::money($value->price) ?>)</option>
                                    <?php endforeach ?>
                                </select> <br><br>
                                <!-- <input type="number" value="1" min="1" name="qty" class="form-control product-detail" style="padding-right: 5px;"> -->
                                <input type="hidden" value="<?php echo $data->id ?>" name="id" class="form-control">
                                <button type="submit" class="btn btn-default-blue">ADD TO CART</button>
                                <?php else: ?>
                                <b>Item out of stock, <a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact us</a> for order </b>
                                <?php endif ?>
                                <?php /*
                                <div class="row">
                                    <div class="col-sm-3"><b>Price</b></div>
                                    <div class="col-sm-9">
                                    <span class="price"><?php echo Cart::money($data->harga) ?></span>
                                    <?php if ($data->harga < $data->harga_coret): ?>
                                    <span class="price-coret"><?php echo Cart::money($data->harga_coret) ?></span>
                                    <?php endif ?>
                                    </div>
                                </div>
                                
                                <div class="clear height-20"></div>
                                <div class="row">
                                    <div class="col-md-3"><b>Quantity</b></div>
                                    <div class="col-md-9">
                                        <div class="quantity">
                                            <input type="text" value="1" name="qty" class="form-control">
                                            <input type="hidden" value="<?php echo $data->id ?>" name="id" class="form-control">
                                            <span class="help-inline">pcs</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear height-15"></div>
                                <div class="height-3"></div>
                                <div class="row">
                                    <div class="col-md-3">&nbsp;</div>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-default-blue">ADD TO CART</button>
                                    </div>
                                </div>
                                */ ?>
                                <div class="clear"></div>
                            </div>
                            </form>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="padding-25">
                                <div class="content-text">
                                    <?php echo $data->description->desc ?>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="clear height-35"></div>
                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="back-shadow-bottom-featproduct"></div>
        <div class="clear"></div>
        </div>

        <div class="clear height-35"></div>
        <div class="clearfix"></div>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54092b87219ecbb4" async="async"></script>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_native_toolbox"></div>
        <div class="clear height-35"></div>
    </div>
    <div class="clearfix"></div>
</section>
