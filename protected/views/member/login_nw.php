<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div>
      

      <div class="content-text insides_static page_member_signup_content">
      	
      	<div class="box_frm_loginNew">
      		<img src="<?php echo $this->assetBaseurl ?>lgo_signIn_news.jpg" alt="" class="img-responsive center-block">
      		<div class="clear height-30"></div>
      		<div class="lines-blue tengah"></div>
      		<div class="clear height-30"></div>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
            <?php echo CHtml::errorSummary($model2, '', '', array('class'=>'alert alert-danger')); ?>
      			<div class="form-group">
              <?php echo CHtml::textField('MeMember2[email]', $model2->email, array('class'=>'form-control', 'placeholder'=>'Enter Username')) ?>
      			</div>
      			<div class="form-group">
              <?php echo CHtml::passwordField('MeMember2[pass]', $model2->pass, array('class'=>'form-control', 'placeholder'=>'Enter Password')) ?>
      			</div>
      			<div class="checkbox">
			    <label>
			      <input type="checkbox"> Keep me signed in
			    </label>
			  </div>
        <div id="recaptcha2"></div>
        <div class="height-20"></div>
			  <button type="submit" class="btn btn-default">SEND</button>
        <div class="height-20"></div>
        <p class="help-block"><a href="<?php echo CHtml::normalizeUrl(array('/member/forgot')); ?>" target="_blank">Forgot Password</a>?<br />
        Don&#39;t have an account? <a href="<?php echo CHtml::normalizeUrl(array('/member/signup', 'ret'=>$_GET['ret'])); ?>" target="_blank">Sign Up</a></p>
<?php $this->endWidget(); ?>

      		<div class="clear"></div>
      	</div>

       <div class="clear"></div>
      </div>
      <!-- End content Signup -->

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>
