<?php
$session = new CHttpSession;
$session->open();
?>
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="inside  s sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static page_member_s_content">
        <h1 class="title_page">MEMBER AREA</h1>
        <div class="clear height-50"></div> <div class="clear height-5"></div>
        <div class="clear"></div>

        <div class="row default box_outers_dashboardmember">
          <div class="col-md-3">
            <?php $this->renderPartial('/member/_menu', array(
            )) ?>
          </div>
          <div class="col-md-9">

            <div class="conts_boxreview_memberm">
              <?php if(Yii::app()->user->hasFlash('success')): ?>
                  <?php $this->widget('bootstrap.widgets.TbAlert', array(
                      'alerts'=>array('success'),
                  )); ?>
              <?php endif; ?>
              <?php echo CHtml::errorSummary($modelMessage, '', '', array('class'=>'alert alert-danger')); ?>

<?php if ($this->memberData['type'] == 1): ?>
              <?php if ($dataMessage->getTotalItemCount() > 0): ?>
              <div class="box_listing_surgeons2">
                <?php foreach ($dataMessage->getData() as $key => $value): ?>
              <div class="row">
                <div class="col-md-12">

                  <div class="items">
                 
                    <div class="bottoms_message_reviewReply back-white">
                      <div class="row">
                        <div class="col-md-9">
                          <div class="desc">
                            <div class="doctor_info">
                              <div class="row">
                                <div class="col-md-12">
                                  <span class="names"><?php echo $value->first_name ?> <?php echo $value->last_name ?></span>
                                  <p>
                                      Phone: <?php echo $value->telp ?> <br>
                                      Email: <?php echo $value->email ?>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                         
                      <div class="clear height-20"></div>

                        <div class="row default prelatife blocks_bottom_d">
                          <div class="col-md-10">
                            <div class="text-center">
                                <span class="sub_t center-block"><b>Message</b></span>
                            </div>
                            <div class="clear height-15"></div>

                            <div class="row default">
                              <div class="col-sm-12">
                                <div class="owners text-right">
                                  <span class="sub_t mb-0"><strong><?php echo $value->first_name ?> <?php echo $value->last_name ?></strong></span>
                                  <div class="clear"></div>
                                  <span class="dates">Send time: <?php echo date('d/m/Y',strtotime($value->date_input)) ?></span>
                                  <div class="clear"></div>
                                  <p><?php echo nl2br($value->message) ?></p>
                                </div>
                              </div>
<?php
$dataMessage2 = DoctorMessage::model()->findAll('parent_id = :parent ORDER BY date_input ASC', array(':parent'=>$value->id));
?>
                              <?php foreach ($dataMessage2 as $k => $v): ?>
                              <?php if ($v->doctor_id != $value->doctor_id): ?>
                              <div class="col-sm-12">
                                <div class="owners text-right">
                                   <span class="sub_t mb-0"><strong><?php echo $v->first_name ?> <?php echo $v->last_name ?></strong></span>
                                    <div class="clear"></div>
                                    <span class="dates">Send time: <?php echo date('d/m/Y',strtotime($v->date_input)) ?></span>
                                    <div class="clear"></div>
                                    <p><?php echo nl2br($v->message) ?></p>
                                  <div class="clear"></div>
                                </div>
                              </div>
                              <?php else: ?>
                              <div class="col-sm-12">
                                <span class="sub_t mb-0"><strong><?php echo $v->first_name ?> <?php echo $v->last_name ?></strong></span>
                                <div class="clear"></div>
                                <span class="dates">Send time: <?php echo date('d/m/Y',strtotime($v->date_input)) ?></span>
                                <div class="clear"></div>
                                <p><?php echo nl2br($v->message) ?></p>
                              </div>
                              <?php endif ?>
                              <?php endforeach ?>
                            </div>

                            <div class="clear height-25"></div>
                            
                            <div class="collapse" id="collapseExample_<?php echo $key ?>">
                              <div class="clear height-20"></div>
                                <div class="well">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
                                    <div class="form-group">
                                      <?php
                                      $modelMessage->parent_id = $value->id;
                                      ?>
                                      <?php echo $form->hiddenField($modelMessage, 'parent_id') ?>
                                      <?php echo $form->textArea($modelMessage, 'message', array('class'=>'form-control', 'placeholder'=>'MESSAGE *', 'cols'=>4)) ?>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-success">SEND</button>
<?php $this->endWidget(); ?>
                                </div>
                              </div>
                            <!-- end collapse -->
                          </div>
                          <div class="col-md-2 border-left h143">
                            <div class="buttons_d">
                              <?php if ($value->member_id > 0): ?>
                              <a class="btn btn-default btn-link" role="button" data-toggle="collapse" href="#collapseExample_<?php echo $key ?>" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-comments"></i>&nbsp;&nbsp;Reply</a>
                              <?php endif ?>
                            </div>
                            <div class="clear"></div>
                          </div>
                        </div>

                      <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>

                </div>
              </div>
              <?php endforeach ?>

                <?php $this->widget('CLinkPager', array(
                    'pages' => $dataMessage->getPagination(),
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                )) ?>

              <div class="clear"></div>
            </div>
            <!-- end box listing surgeons2 -->
            <?php else: ?>
            <h3>No data message</h3>
            <?php endif ?>
<?php else: ?>
<!-- Member Versi -->
              <?php if ($dataMessage->getTotalItemCount() > 0): ?>
              <div class="box_listing_surgeons2">
                <?php foreach ($dataMessage->getData() as $key => $value): ?>
              <div class="row">
                <div class="col-md-12">

                  <div class="items">
                 
                    <div class="bottoms_message_reviewReply back-white">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="items">
                            <div class="picture doctor_p">
                              <a href="#">
                                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(127,127, '/images/doctor/'.$value->doctor->photo , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <div class="desc">
                            <div class="doctor_info">
                              <div class="row">
                                <div class="col-md-12">
                                  <a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'id'=>$value->doctor_id)); ?>">
                                  <span class="names"><?php echo $value->doctor->name ?></span>
                                  </a>
                                  <p><?php echo $value->doctor->certification ?> <br>
                                      <?php if ($value->doctor->n_review > 0): ?>
                                          <?php echo DoctorReviewCategory::model()->createStar($value->doctor->rating) ?> &nbsp; <?php echo $value->doctor->n_review ?> reviews <br>
                                      <?php endif ?>
                                      <?php echo $value->doctor->clinics[0]->address_1 ?> <?php echo $value->doctor->clinics[0]->address_2 ?> <br>
                                      <?php echo $value->doctor->clinics[0]->suburb ?> <?php echo $value->doctor->clinics[0]->state ?> <br>
                                      <?php if ($value->doctor->clinics[0]->phone != ''): ?>
                                              Phone: <?php echo $value->doctor->clinics[0]->phone ?>
                                      <?php endif ?>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                         
                      <div class="clear height-20"></div>

                        <div class="row default prelatife blocks_bottom_d">
                          <div class="col-md-10">
                            <div class="text-center">
                                <span class="sub_t center-block"><b>Message</b></span>
                            </div>
                            <div class="clear height-15"></div>

                            <div class="row default">
                              <div class="col-sm-12">
                                <span class="sub_t mb-0"><strong><?php echo $value->first_name ?> <?php echo $value->last_name ?></strong></span>
                                <div class="clear"></div>
                                <span class="dates">Send time: <?php echo date('d/m/Y',strtotime($value->date_input)) ?></span>
                                <div class="clear"></div>
                                <p><?php echo nl2br($value->message) ?></p>
                              </div>
<?php
$dataMessage2 = DoctorMessage::model()->findAll('parent_id = :parent ORDER BY date_input ASC', array(':parent'=>$value->id));
?>
                              <?php foreach ($dataMessage2 as $k => $v): ?>
                              <?php if ($v->member_id != $session['login_member']['id']): ?>
                              <div class="col-sm-12">
                                <div class="owners text-right">
                                   <span class="sub_t mb-0"><strong><?php echo $v->first_name ?> <?php echo $v->last_name ?></strong></span>
                                    <div class="clear"></div>
                                    <span class="dates">Send time: <?php echo date('d/m/Y',strtotime($v->date_input)) ?></span>
                                    <div class="clear"></div>
                                    <p><?php echo nl2br($v->message) ?></p>
                                  <div class="clear"></div>
                                </div>
                              </div>
                              <?php else: ?>
                              <div class="col-sm-12">
                                <span class="sub_t mb-0"><strong><?php echo $v->first_name ?> <?php echo $v->last_name ?></strong></span>
                                <div class="clear"></div>
                                <span class="dates">Send time: <?php echo date('d/m/Y',strtotime($v->date_input)) ?></span>
                                <div class="clear"></div>
                                <p><?php echo nl2br($v->message) ?></p>
                              </div>
                              <?php endif ?>
                              <?php endforeach ?>
                            </div>

                            <div class="clear height-25"></div>
                            
                            <div class="collapse" id="collapseExample_<?php echo $key ?>">
                              <div class="clear height-20"></div>
                                <div class="well">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
                                    <div class="form-group">
                                      <?php
                                      $modelMessage->parent_id = $value->id;
                                      ?>
                                      <?php echo $form->hiddenField($modelMessage, 'parent_id') ?>
                                      <?php echo $form->textArea($modelMessage, 'message', array('class'=>'form-control', 'placeholder'=>'MESSAGE *', 'cols'=>4)) ?>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-success">SEND</button>
<?php $this->endWidget(); ?>
                                </div>
                              </div>
                            <!-- end collapse -->
                          </div>
                          <div class="col-md-2 border-left h143">
                            <div class="buttons_d">
                              <a class="btn btn-default btn-link" role="button" data-toggle="collapse" href="#collapseExample_<?php echo $key ?>" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-comments"></i>&nbsp;&nbsp;Reply</a>
                            </div>
                            <div class="clear"></div>
                          </div>
                        </div>

                      <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>

                </div>
              </div>
              <?php endforeach ?>

                <?php $this->widget('CLinkPager', array(
                    'pages' => $dataMessage->getPagination(),
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                )) ?>

              <div class="clear"></div>
            </div>
            <!-- end box listing surgeons2 -->
            <?php else: ?>
            <h3>No data message</h3>
            <?php endif ?>


<?php endif ?>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
          <!-- end col md 12 -->
        </div>
        <div class="clear height-20"></div>

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<script>
$( function() {

  $('.selectpicker').selectpicker({
    // style: 'btn-info',
    size: 4
  });

} );
</script>