<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>
      

      <div class="content-text insides_static">
        <h1 class="title_page">EDUCATION - <?php echo strtoupper($names); ?></h1>
        <div class="clear height-50"></div>
        <div class="clear"></div>
        <div class="row">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">
                <?php
                  $urls = '';
                  $name_proc = isset($_GET['name'])? $_GET['name'] : '';
                  // echo $name_proc; exit;
                  if ($name_proc == 'face') {
                    echo "<h4>".nl2br($this->setting['procedures_face_title'])."</h4>";
                  }elseif ($name_proc == 'body') {
                    echo "<h4>".nl2br($this->setting['procedures_body_title'])."</h4>";
                  } else {
                    echo "<h4>".nl2br($this->setting['procedures_breast_title'])."</h4>";
                  }
                  
                ?>
                <div class="clear height-10"></div>

               <?php
                  $urls = '';
                  $name_proc = isset($_GET['name'])? $_GET['name'] : '';
                  // echo $name_proc; exit;
                  if ($name_proc == 'face') {
                    $urls = 'https://content.understand.com/tsaustralia.menu?CatalogID=4d07e4b8-dfd0-4bf2-bea8-3aaf1af34237&SingleCatalog=1';
                  }elseif ($name_proc == 'body') {
                    $urls = 'https://content.understand.com/tsaustralia.menu?CatalogID=980e7aef-30f9-44ef-aba8-6a8d719fe8fc&SingleCatalog=1';
                  } else {
                    $urls = 'https://content.understand.com/tsaustralia.menu?CatalogID=b4d9716a-d5ff-4ec9-8d1a-931ad6997c9e&SingleCatalog=1';
                  }
                  
                ?>
                <div class="box_slider_step6">
                    <div class="pict_full">
                        <style>
                        /*.embed-container { position: relative; padding-bottom: 61.45%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }*/
                        </style>
                        <!-- <div class='embed-container'><iframe src='https://content.understand.com/tsaustralia.menu?CatalogID=b4d9716a-d5ff-4ec9-8d1a-931ad6997c9e&SingleCatalog=1' style='border:0'></iframe></div> -->
                        <iframe src="<?php echo $urls; ?>" border="0" frameborder="0" scrolling="no" width="909" style="min-width:100%; width:100%;" height="557"></iframe>
                    </div>
                  <div class="clear"></div>
                </div>
                <div class="clear height-50"></div>
                
                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">EDUCATION</span>
              </div>
              <div class="clear"></div>
              <?php echo $this->renderPartial('//education/_right_menu', array('active'=>'education')); ?>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>

      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>