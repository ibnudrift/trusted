<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<body class="">

			<div class="full_hmes h100per">
				<?php echo $this->renderPartial('//layouts/_header', array()); ?>
				<?php echo $content ?>
				<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
				<div class="clear"></div>
			</div>
    <script type="text/javascript">
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
          window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
    </script>			
</body>
<?php $this->endContent(); ?>