<?php 
$active_gstep = ( isset($_REQUEST['step']) AND $_REQUEST['step'] != '' )? 'step-'.$_REQUEST['step'] : '';
?>
<div class="block_default_rightmenustatic">
      <ul class="list-unstyled">
        <li data-id="1" class="c1 <?php if ($active_gstep == 'step-1'): ?>active<?php endif ?>">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'1')); ?>">
            <span class="step">Step 1</span>
            <span class="name"><?php echo strtolower($this->setting['journey_step_1_title']) ?></span>
          </a>
        </li>
        <?php /*<!-- <li data-id="1" class="c2 <?php if ($active_gstep == 'step-2'): ?>active<?php endif ?>">
          <a href="<?php // echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'2')); ?>">
            <span class="step">Step 2</span>
            <span class="name"><?php // echo strtolower($this->setting['journey_step_2_title']) ?></span>
          </a>
        </li> -->*/ ?>

        <li data-id="2" class="c2 <?php if ($active_gstep == 'step-3'): ?>active<?php endif ?>">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'3')); ?>">
            <span class="step">Step 2</span>
            <span class="name"><?php echo strtolower($this->setting['journey_step_3_title']) ?></span>
          </a>
        </li>

        <li data-id="3" class="c3 <?php if ($active_gstep == 'step-5'): ?>active<?php endif ?>">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'5')); ?>">
            <span class="step">Step 3</span>
            <span class="name"><?php echo strtolower($this->setting['journey_step_5_title']) ?></span>
          </a>
        </li>

        <li data-id="4" class="c4 <?php if ($active_gstep == 'step-4'): ?>active<?php endif ?>">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'4')); ?>">
            <span class="step">Step 4</span>
            <span class="name"><?php echo strtolower($this->setting['journey_step_4_title']) ?></span>
          </a>
        </li>

        <?php /*<!-- <li data-id="5" class="c6 <?php if ($active_gstep == 'step-6'): ?>active<?php endif ?>">
          <a href="<?php // echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'6')); ?>">
            <span class="step">Step 6</span>
            <span class="name"><?php // echo strtolower($this->setting['journey_step_6_title']) ?></span>
          </a>
        </li> -->*/ ?>
        <li data-id="5" class="c5 <?php if ($active_gstep == 'step-7'): ?>active<?php endif ?>">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'7')); ?>">
            <span class="step">Step 5</span>
            <span class="name"><?php echo strtolower($this->setting['journey_step_7_title']) ?></span>
          </a>
        </li>
       <li data-id="7" class="c8 <?php if ($active_gstep == 'step-8'): ?>active<?php endif ?>">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'8')); ?>">
            <span class="step">Step 6</span>
            <span class="name"><?php echo strtolower($this->setting['journey_step_8_title']) ?></span>
          </a>
        </li>
        <li data-id="8" class="c9 <?php if ($active_gstep == 'step-9'): ?>active<?php endif ?>">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'9')); ?>">
            <span class="step">Step 7</span>
            <span class="name"><?php echo strtolower($this->setting['journey_step_9_title']) ?></span>
          </a>
        </li>
        
        <?php /*<!--  <li data-id="6" class="c6 <?php if ($active_gstep == 'step-10'): ?>active<?php endif ?>">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/journey/', 'step'=>'10')); ?>">
            <span class="step">Step 6</span>
            <span class="name"><?php echo strtolower($this->setting['journey_step_10_title']) ?></span>
          </a>
        </li> -->*/ ?>

      </ul>
    </div>
<?php
$criteria = new CDbCriteria;
$criteria->addCondition('ads_id = :ads_id');
$criteria->params[':ads_id'] = 3;
$criteria->order = 'sort';
// $criteria->group = 't.id';
$dataAds = AdsImage::model()->findAll($criteria);
?>
    <div class="height-20"></div>
    <div class="adds_rightCont">
    <p>
    <?php foreach ($dataAds as $key => $value): ?>
      <a href="<?php echo $value->url ?>">
      <img src="<?php echo Yii::app()->baseUrl.'/images/ads_image/'.$value->image ?>" alt="" class="img-responsive center-block margin-bottom-20">
      </a>
    <?php $value->view = $value->view + 1; $value->save(false);  ?>
    <?php endforeach ?>
    </div>

    </p>
    <div class="height-30"></div>


