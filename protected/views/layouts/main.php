<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="language" content="<?php echo Yii::app()->language ?>" />

	<meta name="keywords" content="<?php echo CHtml::encode($this->metaKey); ?>">
	<meta name="description" content="<?php echo CHtml::encode($this->metaDesc); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/google-optimized.css" />
    <?php
    /*
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/screen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/comon.css" />
    */
    ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php echo $this->metaOg; ?>

    <!-- Bootstrap -->
    <?php /*<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/js/bootstrap-3/css/bootstrap.min.css" />*/ ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/js/bootstrap-3/js/bootstrap.min.js" defer></script>

    <link rel="Shortcut Icon" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/images/favicon.png" />
    <link rel="icon" type="image/ico" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/images/favicon.png" />
    <link rel="icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/images/favicon.png" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php //Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php //Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <?php 
    // $this->widget('application.extensions.fancyapps.EFancyApps', array(
    //         'target'=>'',
    //         'config'=>array(),
    //         )
    //     );
    ?>
    <script src="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/js/jquery.min.js"></script>
    <!-- All JS -->
    <script type="text/javascript">
        var baseurl = "<?php echo CHtml::normalizeUrl(array('/')); ?>";
        var url_add_cart_action = "<?php echo CHtml::normalizeUrl(array('/product/addCart')); ?>";
        var url_edit_cart_action = "<?php echo CHtml::normalizeUrl(array('/product/edit')); ?>";
    </script>
    <script src="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/js/all.min.js" defer></script>

    <?php echo $this->setting['google_tools_webmaster']; ?>
    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
        <?php echo $this->setting['google_tools_analytic']; ?>
    <?php endif; ?>
    
    <?php if ($this->setting['purechat_status'] == '1'): ?>
    <?php echo $this->setting['purechat_code'] ?>
    <?php endif ?>

    <!-- Css -->
    <?php /*<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/styles.css" />*/ ?>
    <?php
    /*
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/pager.css" />
    */
    ?>
    <?php
    /*
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    */
    ?>
    <!-- Latest compiled and minified CSS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js" defer></script>

    <?php if ($this->set_parallax == true): ?>
    <!-- Paralax -->
    <script src="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/js/parallax/jquery.parallax-1.1.3.js" defer></script>
    <script type="text/javascript" defer>
    $(function(){
        $('#intro_step').parallax("50%", 0.3);
    });    
    </script>
    <?php endif ?>
    <noscript id="deferred-styles">
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/font-awesome-4.2.0/css/font-awesome.min.css" />  
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/media.style.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/animate.min.css">  
      <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/bootstrap-select.min.css">  
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/pager.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.8.23/themes/base/minified/jquery-ui.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.8.23/themes/base/minified/jquery.ui.theme.min.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.Yii::app()->theme->baseUrl; ?>/asset/css/last_news.css">  
    </noscript>
</head>
    <?php echo $content ?>

    <script type="text/javascript">
    $(function(){
            // $('.selectpick').selectpicker();

            $('#back-top a').click(function () {
                $('body, html').animate({
                    scrollTop: 0
                }, 1200);
                return false;
            });

            $(".scrollToS").click(function() {
                var t_secttion = $(this).attr('data-id');
                if (t_secttion == 'home'){
                        $('body, html').animate({
                            scrollTop: 0
                        }, 1200);
                        return false;
                }else{
                    $('html, body').animate({
                        scrollTop: $('#'+t_secttion).offset().top
                    }, 1200);
                }
            });

            // detect scroll top
            $(window).scroll(function(){
                var St_heighTop = $(window).scrollTop();
                if (St_heighTop > 750){
                    $('.back_top_menu_sett').addClass('Fixedm');
                }else{
                    $('.back_top_menu_sett').removeClass('Fixedm');
                }
                
            });
    });
</script>

<div id="back-top" class="t-backtop">
    <div class="clear height-5"></div>
        <a href="#top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <script type="text/javascript">
        $(window).load(function(){
        $('.t-backtop').hide();
        });
        $(function(){
            $('.t-backtop').click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });

            var $win = $(window);
                     $win.scroll(function () {
                         if ($win.scrollTop() == 0)
                         $('.t-backtop').hide();
                         else if ($win.height() + $win.scrollTop() != $(document).height() || $win.height() + $win.scrollTop() > 500) {
                         $('.t-backtop').show();
                         }
                     });
        });             
    </script>
    
    <?php if ($this->view_js_light): ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.15/css/lightgallery.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.15/js/lightgallery-all.min.js"></script>
        <script>
             $(document).ready(function() {
                $(".light-gallery").lightGallery({
                    thumbnail:false,
                    animateThumb: false,
                    showThumbByDefault: false
                });

                $('a.call_test_hasil').on('click', function() {
                    var imgs = $(this).attr('data-img');

                    $(this).lightGallery({
                        dynamic: true,
                        dynamicEl: [{
                            "src": imgs,
                            // 'thumb': '../static/img/thumb-1.jpg',
                            'subHtml': ''
                        }]
                    })
                 
                });
            });
        </script>
    <?php endif ?>
</html>