<?php
// secret key recaptcha
// 6LcUECkTAAAAAPVdNP-sfV7zcDDCC80AN4PrJSfe
$model = new JoinForm;
$model->scenario = 'insert';
if(isset($_POST['JoinForm']))
{
    $model->attributes=$_POST['JoinForm'];

    $status = true;
    // filter captcha
    $secret_key = "6LejaCwUAAAAAHfvHbHHxpwCGxNY8X3fijoPytUl";
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
    $response = json_decode($response);
    if($response->success==false)
    {
        $model->addError('verifyCode','Make sure you have finished Captcha.');
        $status = false;
    }

    // 
    if($status AND $model->validate() )
    {

        // config email
        $messaged = $this->renderPartial('//mail/join_community',array(
            'model'=>$model,
        ),TRUE);

        $config = array(
            'to'=>array($model->email, $this->setting['email']),
            'subject'=>'Congratulations you have joined trusted surgeons',
            'message'=>$messaged,
        );
        
        // print_r($config); exit;

        Common::mail($config);

        $dataArray = [
            'email'     => $model->email,
            'status'    => 'subscribed',
            'firstname' => $model->name,
            'lastname'  => ''
        ];
        $http = Common::addMailchimp($dataArray, '74f1492e18');

        Yii::app()->user->setFlash('success','Thank you for joining us');
        $this->refresh();
        // end contact customer
    }

}
?>
<?php 
$pages_nactive = $this->id.'/'.$this->action->id;
?>
<div class="social-media cons_sett <?php echo ($pages_nactive == 'home/index' OR $pages_nactive == 'contact/index')? '':'hide hidden'; ?>">
    <?php if ($this->setting['url_facebook']): ?>
    <a href="<?php echo $this->setting['url_facebook'] ?>" class="fb" target="_blank"><i class="fa fa-facebook"></i></a>
    <?php endif ?>
    <?php if ($this->setting['url_youtube']): ?>
    <a href="<?php echo $this->setting['url_youtube'] ?>" class="yt" target="_blank"><i class="fa fa-youtube"></i></a>
    <?php endif ?>
    <?php if ($this->setting['url_gplus']): ?>
    <a href="<?php echo $this->setting['url_gplus'] ?>" class="gp" target="_blank"><i class="fa fa-google-plus"></i></a>
    <?php endif ?>
    <?php if ($this->setting['url_twitter']): ?>
    <a href="<?php echo $this->setting['url_twitter'] ?>" class="tt" target="_blank"><i class="fa fa-twitter"></i></a>
    <?php endif ?>
    <?php if ($this->setting['url_instagram']): ?>
    <a href="<?php echo $this->setting['url_instagram'] ?>" class="ig" target="_blank"><i class="fa fa-instagram"></i></a>
    <?php endif ?>
    <?php if ($this->setting['url_linkedin']): ?>
    <a href="<?php echo $this->setting['url_linkedin'] ?>" class="li" target="_blank"><i class="fa fa-linkedin"></i></a>
    <?php endif ?>
    <?php if ($this->setting['url_pinterest']): ?>
    <a href="<?php echo $this->setting['url_pinterest'] ?>" class="li" target="_blank"><i class="fa fa-pinterest-p"></i></a>
    <?php endif ?>
    <?php if ($this->setting['url_snapchat']): ?>
    <a href="<?php echo $this->setting['url_snapchat'] ?>" class="li" target="_blank"><i class="fa fa-snapchat-ghost"></i></a>
    <?php endif ?>
</div>

<div class="pulls_footers_fx">
<div class="site-footer">
<div class="newsletter newsletter-subscription">
    <div class="wrapper">
        <h3>Join the Community</h3>
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                // 'type'=>'',
                'enableAjaxValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>false,
                ),
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                ),
            )); ?>

         <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
          <?php if(Yii::app()->user->hasFlash('success')): ?>
              <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
              <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css">
              <script type="text/javascript">
                      sweetAlert("Success!", "<?php echo Yii::app()->user->getFlash('success') ?>", "success");
              </script>

          <?php endif; ?>
            <div class="formWrap">
                <span class="formElement name">
                    <?php echo $form->textField($model, 'name', array('class'=>'newsletter-firstname', 'required'=>'required', 'placeholder'=>'Name')); ?>
                </span>
                <span class="formElement email">
                    <?php echo $form->textField($model, 'email', array('class'=>'newsletter-firstname', 'required'=>'required', 'placeholder'=>'Email')); ?>
                </span>
                <div class="clear"></div>
            </div>
            <div class="formWrap captchann">
                <div id="recaptcha3"></div>
            </div>
            <div class="formSubmit">
                <input class="newsletter-submit" type="submit" value="Join">
            </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
</div>

<footer class="foot">
    <div class="prelatife container">
        <div class="row">
            <div class="col-md-3">
                <div class="lgo_footer">
                    <a href="#"><img src="<?php echo $this->assetBaseurl ?>logo-footer.png" alt="" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="t-footers">
                    <a href="tel:<?php echo $this->setting['contact_phone'] ?>"><?php echo $this->setting['contact_phone'] ?></a>
                    <i class="xs-hidden">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i><br class="visible-xs">
                    <a href="mailto:info@trustedsurgeons.com.au"><span>INFO</span>@TRUSTEDSURGEONS.COM.AU</a>
                    <div class="clear height-15"></div>
                    <div class="menu_footer">
                        <ul class="list-inline">
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/privacy')); ?>">Privacy Policies</a></li>
                            <li class="separator"></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/tos')); ?>">Terms and Conditions</a></li>
                            <li class="separator"></li>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/advertise2')); ?>">Advertise With Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="social-media">
                    <?php if ($this->setting['url_facebook']): ?>
                    <a href="<?php echo $this->setting['url_facebook'] ?>" class="fb" target="_blank"><i class="fa fa-facebook"></i></a>
                    <?php endif ?>
                    <?php if ($this->setting['url_youtube']): ?>
                    <a href="<?php echo $this->setting['url_youtube'] ?>" class="yt" target="_blank"><i class="fa fa-youtube"></i></a>
                    <?php endif ?>
                    <?php if ($this->setting['url_gplus']): ?>
                    <a href="<?php echo $this->setting['url_gplus'] ?>" class="gp" target="_blank"><i class="fa fa-google-plus"></i></a>
                    <?php endif ?>
                    <?php if ($this->setting['url_twitter']): ?>
                    <a href="<?php echo $this->setting['url_twitter'] ?>" class="tt" target="_blank"><i class="fa fa-twitter"></i></a>
                    <?php endif ?>
                    <?php if ($this->setting['url_instagram']): ?>
                    <a href="<?php echo $this->setting['url_instagram'] ?>" class="ig" target="_blank"><i class="fa fa-instagram"></i></a>
                    <?php endif ?>
                    <?php if ($this->setting['url_linkedin']): ?>
                    <a href="<?php echo $this->setting['url_linkedin'] ?>" class="li" target="_blank"><i class="fa fa-linkedin"></i></a>
                    <?php endif ?>
                    <?php if ($this->setting['url_pinterest']): ?>
                    <a href="<?php echo $this->setting['url_pinterest'] ?>" class="li" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                    <?php endif ?>
                    <?php if ($this->setting['url_snapchat']): ?>
                    <a href="<?php echo $this->setting['url_snapchat'] ?>" class="li" target="_blank"><i class="fa fa-snapchat-ghost"></i></a>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</footer>
</div>

<?php 
$active_id = $this->id;
$active_menu_pg = $this->id .'/' . $this->action->id;
?>

<script src='https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit' async defer></script>

<?php if ($active_menu_pg == 'contact/index' || $active_menu_pg == 'member/signup'): ?>   
<script>
var recaptcha1;
var recaptcha2;
var recaptcha3;
var myCallBack = function() {
  //Render the recaptcha1 on the element with ID "recaptcha1"
  recaptcha1 = grecaptcha.render('recaptcha1', {
    'sitekey' : '6LejaCwUAAAAAH8GDoaYgNo0h4WsHzaSQXC1lejA', //Replace this with your Site key
    'theme' : 'light'
  });
  
  //Render the recaptcha2 on the element with ID "recaptcha2"
  recaptcha2 = grecaptcha.render('recaptcha2', {
    'sitekey' : '6LejaCwUAAAAAH8GDoaYgNo0h4WsHzaSQXC1lejA', //Replace this with your Site key
    'theme' : 'light'
  });

  //Render the recaptcha3 on the element with ID "recaptcha3"
  recaptcha3 = grecaptcha.render('recaptcha3', {
    'sitekey' : '6LejaCwUAAAAAH8GDoaYgNo0h4WsHzaSQXC1lejA', //Replace this with your Site key
    'theme' : 'light'
  });
  
};
</script>
<?php elseif ($active_menu_pg == 'surgeon/advertiseenquire' || $active_menu_pg == 'surgeon/enquire' || $active_menu_pg == 'member/index'): ?>
<script type="text/javascript">
    var recaptcha2;
    var recaptcha3;
    var myCallBack = function() {      
      //Render the recaptcha2 on the element with ID "recaptcha2"
      recaptcha2 = grecaptcha.render('recaptcha2', {
        'sitekey' : '6LejaCwUAAAAAH8GDoaYgNo0h4WsHzaSQXC1lejA', //Replace this with your Site key
        'theme' : 'light'
      });

      //Render the recaptcha3 on the element with ID "recaptcha3"
      recaptcha3 = grecaptcha.render('recaptcha3', {
        'sitekey' : '6LejaCwUAAAAAH8GDoaYgNo0h4WsHzaSQXC1lejA', //Replace this with your Site key
        'theme' : 'light'
      });
      
    };
</script>
<?php else: ?>

<script type="text/javascript">
var recaptcha2;
var recaptcha3;
var myCallBack = function() {
  recaptcha3 = grecaptcha.render('recaptcha3', {
    'sitekey' : '6LejaCwUAAAAAH8GDoaYgNo0h4WsHzaSQXC1lejA', //Replace this with your Site key
    'theme' : 'light'
  });
    
<?php if ($active_menu_pg == 'surgeons/detail'): ?>

    recaptcha2 = grecaptcha.render('recaptcha2', {
        'sitekey' : '6LejaCwUAAAAAH8GDoaYgNo0h4WsHzaSQXC1lejA', //Replace this with your Site key
        'theme' : 'light'
      });

    // var object = $(".g-recaptcha");
    // recaptcha2 = grecaptcha.render($(".g-recaptcha"), {
    //     'sitekey' : '6LejaCwUAAAAAH8GDoaYgNo0h4WsHzaSQXC1lejA', //Replace this with your Site key
    //     'size'    : 'invisible',
    //     "theme"   : "light",
    //     'badge'   : 'inline',
    //     'callback' : function(token) {
    //         object.parents('form.enquire-formn').find(".g-recaptcha-response").val(token);
    //         object.parents('form.enquire-formn').submit();
    //     },
    //   });
<?php endif ?>
};

</script>

<?php endif ?>