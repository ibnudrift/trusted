<?php // echo $this->action->id; ?>
<header class="head">
    <div class="backs_respons prelatife">
        <div class="visible-lg">
            <div class="logo_header_web">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                    <img src="<?php echo $this->assetBaseurl ?>logo_header_trusted_surgeons.jpg" alt="" class="img-responsive">
                </a>
            </div>
            <div class="d-inline v-top right_header">
                <div class="top_header">
                    <div class="pl-24">
                        <div class="clear h8"></div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-6">
                                <div class="padding-right-35 text-right sMenu_header_right">
                                    <ul class="list-inline">
                                        <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeons/list')); ?>"><span class="fa fa-search"></span> &nbsp;Find a Trusted Surgeon</a></li>
                                        <?php
                                        $session = new CHttpSession;
                                        $session->open();
                                        if ( ! isset($session['login_member'])):
                                        ?>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>"><span class="icon-profile-h"></span> &nbsp;Sign In</a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/member/signup')); ?>"><span class="icon-pencil-h"></span> &nbsp;Sign Up</a></li>
                                        <?php else: ?>
                                        <li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>"><i class="fa fa-user fa-flip-horizontal" aria-hidden="true"></i> &nbsp;My Dash</a></li>
                                        <li><a href="<?php echo CHtml::normalizeUrl(array('/member/logout')); ?>"><i class="fa fa-sign-out fa-flip-horizontal" aria-hidden="true"></i> &nbsp;Log Out</a></li>
                                        <?php endif ?>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="bottom_header">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="top-menu">
                                <ul class="list-inline">
                                    <li class="<?php if ($this->id == 'about'): ?>active<?php endif ?> dropdown">
                                        <a href="<?php echo CHtml::normalizeUrl(array('/about/index')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ALCL</a>
                                        <ul class="dropdown-menu">
                                            <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/education/index')); ?>">3D Surgery Animation</a></li> -->
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/about/index')); ?>"><?php echo $this->setting['about_who_title'] ?></a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/about/why')); ?>"><?php echo $this->setting['about_why_title'] ?></a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/about/me')); ?>"><?php echo $this->setting['about_me_title'] ?></a></li>
                                          </ul>
                                    </li>
                                    <li class="<?php if ($this->action->id == 'journey'): ?>active<?php endif ?> dropdown">
                                        <a href="<?php echo CHtml::normalizeUrl(array('/home/journey')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Get Started</a>
                                        <ul class="dropdown-menu">
                                            <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/education/index')); ?>">3D Surgery Animation</a></li> -->
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/journey', 'step'=>1)); ?>">Patient journey</a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/gettingstarted/mentoring')); ?>"><?php echo $this->setting['mentoring_title'] ?></a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/education/faq')); ?>">FAQ</a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/gettingstarted/help')); ?>"><?php echo $this->setting['help_title'] ?></a></li>
                                          </ul>
                                    </li>
                                    <li <?php if ($this->id == 'procedures'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index')); ?>">PROCEDURES</a></li>
                                    <?php /*
                                    <li <?php if ($this->id == 'surgeons'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/surgeons/index')); ?>">Surgeons</a></li>
                                    <li <?php if ($this->id == 'coaching'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/coaching/index')); ?>">COACHING</a></li>

                                    <li <?php if ($this->id == 'shop'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/shop/list')); ?>">Shop</a></li>
                                    <li class="<?php if ($this->id == 'education'): ?>active<?php endif ?> dropdown">
                                        <a href="<?php echo CHtml::normalizeUrl(array('/education/index')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROCEDURES</a>
                                        <ul class="dropdown-menu">
                                            <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/education/index')); ?>">3D Surgery Animation</a></li> -->
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'breast')); ?>">Procedures</a></li>
                                            <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/education/faq')); ?>">FAQ</a></li> -->
                                          </ul>
                                    </li>
                                    */ ?>
                                    <li class="<?php if ($this->id == 'professional'): ?>active<?php endif ?> dropdown">
                                        <a href="<?php echo CHtml::normalizeUrl(array('/education/index')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ABOUT US</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/whyClinic/')); ?>"><?php echo $this->setting['why_clinic_title'] ?></a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/industryblog/index/')); ?>"><?php echo $this->setting['blog_prof_title'] ?></a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/clinic_faq/')); ?>"><?php echo $this->setting['clinic_faq_title'] ?></a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/advertise/')); ?>"><?php echo $this->setting['advertise_title'] ?></a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/advertise2/')); ?>">Advertise with Us</a></li>
                                          </ul>
                                    </li>
                                    <li class="<?php if ($this->id == 'blog'): ?>active<?php endif ?> dropdown">
                                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NEWS & MEDIA</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/youtube/index/')); ?>">Video Library</a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index/')); ?>">News</a></li>
                                          </ul>
                                    </li>

                                    <li <?php if ($this->id == 'contact'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/contact/index')); ?>">CONTACT US</a></li>
                                    
                                    <?php /*<li class="dropdown">
                                        <a href="<?php echo CHtml::normalizeUrl(array('/procedures/index')); ?>" class="dropdown-toggle disabled" z-toggle="dropdown">Procedures</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'face' )); ?>">FACE</a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'breast' )); ?>">BREAST</a></li>
                                            <li><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'body' )); ?>">BODY</a></li>
                                          </ul>
                                    </li>*/ ?>
                                    
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div id="searchs_bar">
                                  <form action="<?php echo CHtml::normalizeUrl(array('/surgeons/list')); ?>" class="search-form">
                                    <div class="form-group has-feedback">
                                        <label for="search" class="sr-only">Search</label> 
                                        <input type="text" class="form-control" name="q" id="search" placeholder="Search ...">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </form>
                            </div>
                            <div class="text-right sMenu_header_right pulls_sticky" style="display: none;">
                                <ul class="list-inline">
                                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeons/list')); ?>">Find a Trusted Surgeon</a></li> -->
                                    <?php
                                    $session = new CHttpSession;
                                    $session->open();
                                    if ( ! isset($session['login_member'])):
                                    ?>
                                        <li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>"><span class="icon-profile-h"></span> &nbsp;Sign In</a></li>
                                        <li><a href="<?php echo CHtml::normalizeUrl(array('/member/signup')); ?>"><span class="icon-pencil-h"></span> &nbsp;Sign Up</a></li>
                                    <?php else: ?>
                                    <li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>"><i class="fa fa-user fa-flip-horizontal" aria-hidden="true"></i> &nbsp;My Dash</a></li>
                                    <li><a href="<?php echo CHtml::normalizeUrl(array('/member/logout')); ?>"><i class="fa fa-sign-out fa-flip-horizontal" aria-hidden="true"></i> &nbsp;Log Out</a></li>
                                    <?php endif ?>
                                </ul>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <!-- end visible lg -->
        <div class="hidden-lg">
            <div class="row default">
                <div class="col-md-4 col-sm-4 hidden-xs">
                    <div class="block top_left_header2 text-left">
                        <div class="d-inline v-top">
                            <a href="javascript:;" class="showmenu_barresponsive2"><i class="fa fa-user"></i></a>
                        </div>
                        <?php /*
                        <div class="d-inline v-top">
                            <a href="#"><i class="fa fa-shopping-cart"></i></a>
                        </div>
                        */ ?>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="logo_header_web">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                            <img src="<?php echo $this->assetBaseurl ?>logo_header_trusted_surgeons.jpg" alt="" class="img-responsive center-block">
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 visible-xs">
                    <div class="block top_left_header2 text-left">
                        <div class="d-inline v-top">
                            <a href="javascript:;" class="showmenu_barresponsive2"><i class="fa fa-user"></i></a>
                        </div>
                        <?php /*
                        <div class="d-inline v-top">
                            <a href="#"><i class="fa fa-shopping-cart"></i></a>
                        </div>
                        */ ?>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="block top_right_header2 text-right">
                        <a href="javascript:;" class="showmenu_barresponsive">MENU &nbsp;<i class="fa fa-bars"></i></a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear clearfix"></div>
    </div>

<div id="show_mn1" class="outer-blok-black-menuresponss-hides">
    <div class="prelatife container">
        <div class="clear height-40"></div>
        <div class="fright">
            <div class="hidesmenu-frightd">
                <a href="javacript:;" class="closemrespobtn">
                    <i class="fa fa-times-circle"></i>
                </a>
            </div>
        </div>
        <div class="clear height-50"></div><div class="height-15"></div>
        <div class="menu-sheader-datals">
            <ul class="list-unstyled">
                <li class="<?php if ($this->id == 'about'): ?>active<?php endif ?> dropdown">
                    <a href="<?php echo CHtml::normalizeUrl(array('/about/index')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ALCL</a>
                    <ul class="dropdown-menu">
                        <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/education/index')); ?>">3D Surgery Animation</a></li> -->
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/about/index')); ?>"><?php echo $this->setting['about_who_title'] ?></a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/about/why')); ?>"><?php echo $this->setting['about_why_title'] ?></a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/about/me')); ?>"><?php echo $this->setting['about_me_title'] ?></a></li>
                      </ul>
                </li>
                <li class="<?php if ($this->action->id == 'journey'): ?>active<?php endif ?> dropdown">
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/journey')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Get Started</a>
                    <ul class="dropdown-menu">
                        <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/education/index')); ?>">3D Surgery Animation</a></li> -->
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/journey', 'step'=>1)); ?>">Patient journey</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/gettingstarted/mentoring')); ?>"><?php echo $this->setting['mentoring_title'] ?></a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/education/faq')); ?>">FAQ</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/gettingstarted/help')); ?>"><?php echo $this->setting['help_title'] ?></a></li>
                      </ul>
                </li>
                <li <?php if ($this->id == 'procedures'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index')); ?>">PROCEDURES</a></li>
                <?php /*
                <li <?php if ($this->id == 'surgeons'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/surgeons/index')); ?>">Surgeons</a></li>
                <li <?php if ($this->id == 'coaching'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/coaching/index')); ?>">COACHING</a></li>

                <li <?php if ($this->id == 'shop'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/shop/list')); ?>">Shop</a></li>
                <li class="<?php if ($this->id == 'education'): ?>active<?php endif ?> dropdown">
                    <a href="<?php echo CHtml::normalizeUrl(array('/education/index')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROCEDURES</a>
                    <ul class="dropdown-menu">
                        <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/education/index')); ?>">3D Surgery Animation</a></li> -->
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/procedures/index', 'name' => 'breast')); ?>">Procedures</a></li>
                        <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/education/faq')); ?>">FAQ</a></li> -->
                      </ul>
                </li>
                */ ?>
                <li class="<?php if ($this->id == 'professional'): ?>active<?php endif ?> dropdown">
                    <a href="<?php echo CHtml::normalizeUrl(array('/education/index')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ABOUT US</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/whyClinic/')); ?>"><?php echo $this->setting['why_clinic_title'] ?></a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/industryblog/index/')); ?>"><?php echo $this->setting['blog_prof_title'] ?></a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/clinic_faq/')); ?>"><?php echo $this->setting['clinic_faq_title'] ?></a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/advertise/')); ?>"><?php echo $this->setting['advertise_title'] ?></a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/surgeon/advertise2/')); ?>">Advertise with Us</a></li>
                      </ul>
                </li>
                <li class="<?php if ($this->id == 'blog'): ?>active<?php endif ?> dropdown">
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NEWS & MEDIA</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/youtube/index/')); ?>">Video Library</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index/')); ?>">News</a></li>
                      </ul>
                </li>

                <li <?php if ($this->id == 'contact'): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/contact/index')); ?>">CONTACT US</a></li>
                
            </ul>
        </div>
        <div class="clear height-20"></div>
        <div class="lines-white height-3"></div>
        <div class="clear height-20"></div>
        <div class="text-center center-block information_headerphone">
            <i class="fa fa-phone flip-horizontal"></i> &nbsp;<span>PHONE.</span> <a href="tel:<?php echo $this->setting['contact_phone'] ?>"><?php echo $this->setting['contact_phone'] ?></a>
        </div>
        <div class="clear height-15"></div>
        <div class="lines-white height-3"></div>
        <div class="clear height-20"></div>
        <div class="blocks_social_responheader information_social_net text-center">
            <?php if ($this->setting['url_facebook']): ?>
            <a href="<?php echo $this->setting['url_facebook'] ?>" class="fb" target="_blank"><i class="fa fa-facebook"></i></a>
            <?php endif ?>
            <?php if ($this->setting['url_youtube']): ?>
            <a href="<?php echo $this->setting['url_youtube'] ?>" class="yt" target="_blank"><i class="fa fa-youtube"></i></a>
            <?php endif ?>
            <?php if ($this->setting['url_gplus']): ?>
            <a href="<?php echo $this->setting['url_gplus'] ?>" class="gp" target="_blank"><i class="fa fa-google-plus"></i></a>
            <?php endif ?>
            <?php if ($this->setting['url_twitter']): ?>
            <a href="<?php echo $this->setting['url_twitter'] ?>" class="tt" target="_blank"><i class="fa fa-twitter"></i></a>
            <?php endif ?>
            <?php if ($this->setting['url_instagram']): ?>
            <a href="<?php echo $this->setting['url_instagram'] ?>" class="ig" target="_blank"><i class="fa fa-instagram"></i></a>
            <?php endif ?>
            <?php if ($this->setting['url_linkedin']): ?>
            <a href="<?php echo $this->setting['url_linkedin'] ?>" class="li" target="_blank"><i class="fa fa-linkedin"></i></a>
            <?php endif ?>
            <?php if ($this->setting['url_pinterest']): ?>
            <a href="<?php echo $this->setting['url_pinterest'] ?>" class="li" target="_blank"><i class="fa fa-pinterest-p"></i></a>
            <?php endif ?>
            <?php if ($this->setting['url_snapchat']): ?>
            <a href="<?php echo $this->setting['url_snapchat'] ?>" class="li" target="_blank"><i class="fa fa-snapchat-ghost"></i></a>
            <?php endif ?>
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>

<div id="show_mn2" class="outer-blok-black-menuresponss-hides">
    <div class="prelatife container">
        <div class="clear height-40"></div>
        <div class="fright">
            <div class="hidesmenu-frightd">
                <a href="javacript:;" class="closemrespobtn2">
                    <i class="fa fa-times-circle"></i>
                </a>
            </div>
        </div>
        <div class="clear height-50"></div><div class="height-30"></div>
        <div class="menu-sheader-datals">
            <ul class="list-unstyled">
                <li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">Find a Trusted Surgeon</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/member/index')); ?>">SIGN IN</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/member/signup')); ?>">SIGN UP</a></li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    $(function(){
        // show and hide menu responsive
        // menu 1
        $('a.showmenu_barresponsive').live('click', function() {
            $('#show_mn1.outer-blok-black-menuresponss-hides').slideToggle('slow');
            return false;
        });
        $('a.closemrespobtn').live('click', function() {
            $('#show_mn1.outer-blok-black-menuresponss-hides').slideUp('slow');
            return false;
        });

        // menu 1
        $('a.showmenu_barresponsive2').live('click', function() {
            $('#show_mn2.outer-blok-black-menuresponss-hides').slideToggle('slow');
            return false;
        });
        $('a.closemrespobtn2').live('click', function() {
            $('#show_mn2.outer-blok-black-menuresponss-hides').slideUp('slow');
            return false;
        });

        // searching header
        $('.search-form .form-group input').live('click', function() {
            $(this).parent().addClass('fullw'); 
        });
    });
</script>
</header>