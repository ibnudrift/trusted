<?php
class MyLinkPager extends CLinkPager
{

    public $linkHash = '';

    protected function createPageUrl($page)
    {
        $url = $this->getPages()->createPageUrl($this->getController(),$page);

        if($this->linkHash)
            $url = $url.'#'.$this->linkHash;

        return $url;
    }
}