<?php

/**
 * This is the model class for table "me_member".
 *
 * The followings are the available columns in table 'me_member':
 * @property integer $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $pass
 * @property string $login_terakhir
 * @property integer $aktivasi
 * @property integer $aktif
 * @property string $image
 * @property string $phone
 * @property string $billing_address_line_1
 * @property string $billing_suburb
 * @property string $billing_state
 * @property string $billing_postcode
 */
class MeMember extends CActiveRecord
{
	public $pass2;
	public $passold;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MeMember the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'me_member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('email', 'unique', 'className' => 'MeMember', 'attributeName' => 'email', 'message'=>'This Email is already in use', 'on'=>'createMember'),
			array('pass, email, first_name, last_name', 'required', 'on'=>'createMember'),
			array('pass', 'compare', 'compareAttribute'=>'pass2', 'on'=>array('updatePass')),
			array('pass', 'compare', 'compareAttribute'=>'pass2', 'on'=>array('changePass')),

			array('email', 'unique', 'className' => 'MeMember', 'attributeName' => 'email', 'message'=>'This Email is already in use', 'on'=>array('createMember', 'insert')),

			array('first_name', 'required', 'on'=>'update'),

			array('pass, pass2, passold', 'required', 'on'=>'updatePass'),
			array('pass, pass2', 'required', 'on'=>'changePass'),

			// array('email, first_name, last_name, pass, login_terakhir, aktivasi, aktif, image, phone, billing_address_line_1, billing_suburb, billing_state, billing_postcode', 'required'),
			array('aktivasi, aktif', 'numerical', 'integerOnly'=>true),
			array('email, first_name, last_name, image', 'length', 'max'=>200),
			array('pass', 'length', 'max'=>100),
			array('phone, billing_suburb, billing_state, delivery_suburb, delivery_state', 'length', 'max'=>50),
			array('billing_postcode, delivery_postcode', 'length', 'max'=>5),
			array('billing_postcode, delivery_postcode', 'length', 'min'=>3),
			// array('phone', 'length', 'max'=>11),
			// array('phone', 'length', 'min'=>9),
			array('email', 'email'),
			array('billing_address_line_1, billing_address_line_2, delivery_address_line_1, delivery_address_line_2, pass2, passold, type', 'safe'),

			// array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>FALSE, 'on'=>'insert', 'except'=>array('createTemp', 'copy')),
			// array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>TRUE, 'on'=>'update', 'except'=>array('createTemp', 'copy')),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, first_name, last_name, pass, login_terakhir, aktivasi, aktif, image, phone, billing_address_line_1, billing_address_line_2, billing_suburb, billing_state, billing_postcode, delivery_address_line_1, delivery_address_line_2, delivery_suburb, delivery_state, delivery_postcode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'passold' => 'Old Password',
			'pass' => 'Password',
			'pass2' => 'Confirm Password',
			'login_terakhir' => 'Last Login',
			'aktivasi' => 'Aktivasi',
			'aktif' => 'Active',
			'image' => 'Image',
			'phone' => 'Phone',
			'billing_address_line_1' => 'Address Line 1',
			'billing_address_line_2' => 'Address Line 2',
			'billing_suburb' => 'Suburb',
			'billing_state' => 'State',
			'billing_postcode' => 'Postcode',
			'delivery_address_line_1' => 'Address Line 1',
			'delivery_address_line_2' => 'Address Line 2',
			'delivery_suburb' => 'Suburb',
			'delivery_state' => 'State',
			'delivery_postcode' => 'Postcode',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('login_terakhir',$this->login_terakhir,true);
		$criteria->compare('aktivasi',$this->aktivasi);
		$criteria->compare('aktif',$this->aktif);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('billing_address_line_1',$this->billing_address_line_1,true);
		$criteria->compare('billing_address_line_2',$this->billing_address_line_2,true);
		$criteria->compare('billing_suburb',$this->billing_suburb,true);
		$criteria->compare('billing_state',$this->billing_state,true);
		$criteria->compare('billing_postcode',$this->billing_postcode,true);
		$criteria->compare('delivery_address_line_1',$this->delivery_address_line_1,true);
		$criteria->compare('delivery_address_line_2',$this->delivery_address_line_2,true);
		$criteria->compare('delivery_suburb',$this->delivery_suburb,true);
		$criteria->compare('delivery_state',$this->delivery_state,true);
		$criteria->compare('delivery_postcode',$this->delivery_postcode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}