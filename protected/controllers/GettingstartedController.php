<?php

class GettingstartedController extends Controller
{

	public function actionMentoring()
	{
		// $this->pageTitle = 'Why your Clinic should join - '.$this->pageTitle;
		$metaType = 'mentoring';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('mentoring', array(
		));
	}
	public function actionHelp()
	{
		// $this->pageTitle = 'Why your Clinic should join - '.$this->pageTitle;
		$metaType = 'help';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('help', array(
		));
	}

}

