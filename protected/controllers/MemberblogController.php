<?php

class MemberblogController extends ControllerMember
{
	public function actionIndex()
	{
		// https://www.youtube.com/watch?v=mC2q2SGy2e0
		// $data = file_get_contents("http://youtube.com/get_video_info?video_id=".'mC2q2SGy2e0');
		// parse_str($data, $output);

		$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));

		$criteria = new CDbCriteria;
		$criteria->addCondition('t.doctor_id = :doctor_id');
		$criteria->params[':doctor_id'] = $model->id;

		if ($_GET['review_order'] == 'featured') {
		  $criteria->order = 'featured DESC, date_input DESC';
		} elseif($_GET['review_order'] == 'higest') {
		  $criteria->order = 'date_input ASC';
		} elseif($_GET['review_order'] == 'lowest') {
		  $criteria->order = 'date_input DESC';
		} elseif($_GET['review_order'] == 'recent') {
		} else {
		  $criteria->order = 'date_input DESC';
		}

		$dataBlog = new CActiveDataProvider('DoctorBlog', array(
		  'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		));

		if ($_GET['update']) {
		$modelBlog = DoctorBlog::model()->findByPk($_GET['update']);
		}else{
		$modelBlog = new DoctorBlog;
		}

		if ($_POST['DoctorBlog']) {
			$modelBlog->attributes=$_POST['DoctorBlog'];
			$image = CUploadedFile::getInstance($modelBlog,'image');
			if ($image->name != '') {
				$modelBlog->image = substr(md5(time()),0,5).'-'.$image->name;
			}
			if($modelBlog->validate()){

				if ($image->name != '') {
					$image->saveAs(Yii::getPathOfAlias('webroot').'/images/blog/'.$modelBlog->image);
				}

				$modelBlog->doctor_id = $model->id;
				$modelBlog->member_id = $this->memberData['id'];
				$modelBlog->status = 1;
				$modelBlog->date_input = date('Y-m-d H:i:s');
				$modelBlog->save();
				$this->redirect(array('index'));
			}
		}

		$this->render('index', array(
			'model'=>$model,
			'modelBlog'=>$modelBlog,
			'dataBlog'=>$dataBlog,
		));	
	}

	// public function actionApprove($id='')
	// {
	// 	$data = DoctorReview::model()->findByPk($id);
	// 	$data->status = 1;
	// 	$data->save(false);
	// 	$this->redirect(array('index'));
	// }

	// public function actionHide($id='')
	// {
	// 	$data = DoctorReview::model()->findByPk($id);
	// 	$data->status = 0;
	// 	$data->save(false);
	// 	$this->redirect(array('index'));
	// }

	// public function actionFeatured($id='')
	// {
	// 	$data = DoctorReview::model()->findByPk($id);
	// 	$data->featured = 1;
	// 	$data->save(false);
	// 	$this->redirect(array('index'));
	// }

	// public function actionDel_featured($id='')
	// {
	// 	$data = DoctorReview::model()->findByPk($id);
	// 	$data->featured = 0;
	// 	$data->save(false);
	// 	$this->redirect(array('index'));
	// }

	public function actionDelete($id='')
	{
		$data = DoctorBlog::model()->findByPk($id);
		$data->status = 2;
		$data->save(false);
		$this->redirect(array('index'));
	}

}