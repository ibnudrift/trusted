<?php

class ProceduresController extends Controller
{

	// public function actionIndex()
	// {
	// 	$this->render('index', array(
	// 		// 'procedure'=>$procedure,
	// 	));
	// }

	public function actionDetail($slug)
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('description');

		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		$criteria->addCondition('t.slug = :slug');
		$criteria->params[':slug'] = $slug;

		$data = Listprocedure::model()->find($criteria);

		$this->pageTitle = $data->description->meta_title;
		$this->metaDesc = $data->description->meta_description;
		$this->metaKey = $data->description->meta_keyword;
		$this->metaOg = $data->description->open_graph;

		$this->render('detail', array(
			'data'=>$data,
			// 'procedure'=>$procedure,
		));
	}

	public function actionIndex()
	{
		// $id = 0;
		// if ($_GET['name'] != '') {
		// 	if ($_GET['name']== 'face') {
		// 		$id = 3;
		// 	}elseif ($_GET['name'] == 'body') {
		// 		$id = 2;
		// 	}elseif ($_GET['name'] == 'breast') {
		// 		$id = 1;
		// 	} else {
		// 		$id = 0;
		// 	}
		// }

		$metaType = 'procedures';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		// $procedure = array();
		// if ($id != 0 AND $id != '') {

			$this->render('index', array(
				'procedure'=>$procedure,
			));
		// }else{
		// 	$this->render('landing', array(
		// 	));
		// }
		// End Procedure
	}
	public function actionAturslug()
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('description');

		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$data = Listprocedure::model()->findAll($criteria);
		foreach ($data as $key => $value) {
			$value->slug = Slug::create($value->description->question);
			$value->save(false);
		}
	}
}