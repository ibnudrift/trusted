<?php

class MemberreviewController extends ControllerMember
{
	public function actionIndex()
	{
		// https://www.youtube.com/watch?v=mC2q2SGy2e0
		// $data = file_get_contents("http://youtube.com/get_video_info?video_id=".'mC2q2SGy2e0');
		// parse_str($data, $output);
		$criteria = new CDbCriteria;
		if ($this->memberData['type'] == 1) {
			$model = Doctor::model()->find('member_id = :member_id', array(':member_id'=>$this->memberData['id']));
			$criteria->addCondition('t.doctor_id = :doctor_id');
			$criteria->params[':doctor_id'] = $model->id;
		} else {
			$criteria->addCondition('t.member_id = :member_id');
			$criteria->params[':member_id'] = $this->memberData['id'];
		}
		
		if ($_GET['review_order'] == 'featured') {
		  $criteria->order = 'featured DESC, date_input DESC';
		} elseif($_GET['review_order'] == 'higest') {
		  $criteria->order = 'date_input ASC';
		} elseif($_GET['review_order'] == 'lowest') {
		  $criteria->order = 'date_input DESC';
		} elseif($_GET['review_order'] == 'recent') {
		} else {
		  $criteria->order = 'date_input DESC';
		}

		$dataReview = new CActiveDataProvider('DoctorReview', array(
		  'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		));

		$this->render('index', array(
			'model'=>$model,
			'dataReview'=>$dataReview,
		));	
	}

	public function actionApprove($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$data->status = 1;
		$data->save(false);
		
		$dataDoctor = Doctor::model()->findByPk($data->doctor_id);
		
		//rating keseluruhan
		$dataReviewValue = Yii::app()->db->createCommand()
		    ->select('SUM(`doctor_category_review`.`value`)/COUNT(`doctor_category_review`.`value`) as `value`')
		    // ->select('*')
		    ->from('doctor_review')
		    ->join('doctor_category_review', 'doctor_review.id = doctor_category_review.review_id')
		    ->where('doctor_review.doctor_id=:doctor_id', array(':doctor_id'=>$dataDoctor->id))
		    ->group('doctor_review.doctor_id')
		    ->queryRow();
		$dataDoctor->rating = $dataReviewValue['value'];

		//jumlah yang mereview
		$criteria = new CDbCriteria;
		$criteria->addCondition('doctor_id = :doctor_id');
		$criteria->addCondition('status = 1');
		$criteria->params[':doctor_id'] = $dataDoctor->id;
		$jmlReview = DoctorReview::model()->count($criteria);

		$dataDoctor->n_review = $jmlReview;
		$dataDoctor->save(false);

		Yii::app()->user->setFlash('success','You have approved the review');

		$this->redirect(array('index'));
	}

	public function actionHide($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$data->status = 0;
		$data->save(false);
		$dataDoctor = Doctor::model()->findByPk($data->doctor_id);
		
		//rating keseluruhan
		$dataReviewValue = Yii::app()->db->createCommand()
		    ->select('SUM(`doctor_category_review`.`value`)/COUNT(`doctor_category_review`.`value`) as `value`')
		    // ->select('*')
		    ->from('doctor_review')
		    ->join('doctor_category_review', 'doctor_review.id = doctor_category_review.review_id')
		    ->where('doctor_review.doctor_id=:doctor_id', array(':doctor_id'=>$dataDoctor->id))
		    ->group('doctor_review.doctor_id')
		    ->queryRow();
		$dataDoctor->rating = $dataReviewValue['value'];

		//jumlah yang mereview
		$criteria = new CDbCriteria;
		$criteria->addCondition('doctor_id = :doctor_id');
		$criteria->addCondition('status = 1');
		$criteria->params[':doctor_id'] = $dataDoctor->id;
		$jmlReview = DoctorReview::model()->count($criteria);

		$dataDoctor->n_review = $jmlReview;
		$dataDoctor->save(false);

		Yii::app()->user->setFlash('success','You have to hide a review');

		$this->redirect(array('index'));
	}

	public function actionFeatured($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$data->featured = 1;
		$data->save(false);
		Yii::app()->user->setFlash('success','You have featured the review');
		$this->redirect(array('index'));
	}

	public function actionDel_featured($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$data->featured = 0;
		$data->save(false);
		Yii::app()->user->setFlash('success','You have delete featured review');
		$this->redirect(array('index'));
	}

	public function actionDelete($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$dataDoctor = Doctor::model()->findByPk($data->doctor_id);
		$data->delete();

		//rating keseluruhan
		$dataReviewValue = Yii::app()->db->createCommand()
		    ->select('SUM(`doctor_category_review`.`value`)/COUNT(`doctor_category_review`.`value`) as `value`')
		    // ->select('*')
		    ->from('doctor_review')
		    ->join('doctor_category_review', 'doctor_review.id = doctor_category_review.review_id')
		    ->where('doctor_review.doctor_id=:doctor_id', array(':doctor_id'=>$dataDoctor->id))
		    ->group('doctor_review.doctor_id')
		    ->queryRow();
		$dataDoctor->rating = $dataReviewValue['value'];

		//jumlah yang mereview
		$criteria = new CDbCriteria;
		$criteria->addCondition('doctor_id = :doctor_id');
		$criteria->addCondition('status = 1');
		$criteria->params[':doctor_id'] = $dataDoctor->id;
		$jmlReview = DoctorReview::model()->count($criteria);

		$dataDoctor->n_review = $jmlReview;
		$dataDoctor->save(false);

		Yii::app()->user->setFlash('success','You have deleted the review');
		$this->redirect(array('index'));
	}

}