<?php

class AboutController extends Controller
{

	public function actionIndex()
	{
		$metaType = 'about_who';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('index', array(
		));
	}
	public function actionMission()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;

		$this->render('mission', array(
		));
	}
	public function actionWhy()
	{
		// $this->pageTitle = 'About Us - '.$this->pageTitle;
		$metaType = 'about_why';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('why', array(
		));
	}
	public function actionMe()
	{
		// $this->pageTitle = 'About Us - '.$this->pageTitle;
		$metaType = 'about_me';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('me', array(
		));
	}
}

