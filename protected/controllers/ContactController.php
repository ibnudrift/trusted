<?php

class ContactController extends Controller
{

	public function actionIndex()
	{

		$this->pageTitle = 'Contact Us - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if ($_GET['id']) {
			$dataDoctor = Doctor::model()->with(array('clinics', 'spec', 'videos'))->findByPk($_GET['id']);
			$dataMember = MeMember::model()->findByPk($dataDoctor->id);
		}

		// secret key recaptcha
		// 6LejaCwUAAAAAHfvHbHHxpwCGxNY8X3fijoPytUl
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			$status = true;
	        $secret_key = "6LejaCwUAAAAAHfvHbHHxpwCGxNY8X3fijoPytUl";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	        	$model->addError('verifyCode','Make sure you have finished Captcha.');
	        	$status = false;
	        }

	        // 
			if($status AND $model->validate() )
			{
				// contact customer
				if ($model->contact_type == 'customer') {
					if ($model->coaching == 1) {
						$model->coaching = 'Booking a coaching service';	
					}else{
						$model->coaching = 'Not Booking a coaching service';
					}

					// config email to client
					$messaged = $this->renderPartial('//mail/contact_customer',array(
						'model'=>$model,
					),TRUE);

					if ($_GET['id']) {
						$config = array(
							'to'=>array($model->email, $dataMember->email),
							'subject'=>'Hi, '.$dataMember->first_name.' Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
							'message'=>$messaged,
						);
					}else{
						$config = array(
							'to'=>array($model->email, $this->setting['email']),
							'subject'=>'Hi, Trusted Surgeons Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
							'message'=>$messaged,
						);
					}
					Common::mail($config);


					// config email to Admin Surgeons
					$messaged2 = $this->renderPartial('//mail/contact',array(
						'model'=>$model,
					),TRUE);
					if ($_GET['id']) {
						$config2 = array(
							'to'=>array($this->setting['email']),
							'subject'=>'Hi, '.$dataMember->first_name.' Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
							'message'=>$messaged2,
						);
					}else{
						$config2 = array(
							'to'=>array($this->setting['email']),
							'subject'=>'Hi, Trusted Surgeons Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
							'message'=>$messaged2,
						);
					}
					Common::mail($config2);

					Yii::app()->user->setFlash('success','Thank you for contacting us.');
					if ($_GET['id']) {
						$this->redirect(array('/surgeons/detail', 'id'=>$GET['id']));
					}else{
						$this->refresh();
					}
					// end contact customer
				}else{
					// contact surgeons
					$messaged = $this->renderPartial('//mail/contact_surgeons',array(
						'model'=>$model,
					),TRUE);
					$config = array(
						'to'=>array($model->email, $this->setting['email']),
						'subject'=>'Hi, Trusted Surgeons Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
						'message'=>$messaged,
					);
					Common::mail($config);

					// config email to Admin Surgeons
					$messaged3 = $this->renderPartial('//mail/contact',array(
						'model'=>$model,
					),TRUE);
					$config3 = array(
						'to'=>array( $this->setting['email'] ),
						'subject'=>'Hi, Trusted Surgeons Contact for '.strtoupper( ($model->contact_type != '')? $model->contact_type:'' ),
						'message'=>$messaged3,
					);
					Common::mail($config3);


					Yii::app()->user->setFlash('success','Thank you for contacting us.');
					$this->refresh();
				}
				// end contact surgeons
			}

		}

		$metaType = 'contact';
		$this->pageTitle = ( $this->setting[$metaType.'_meta_title'] != '' )? $this->setting[$metaType.'_meta_title'] : 'Contact Us - '.$this->pageTitle;
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];
				
		$this->render('index', array(
			'model'=>$model,
		));
	}

}