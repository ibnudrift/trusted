<?php

class DesignController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionSignUp()
	{
		$this->pageTitle = 'Sign Up - '.$this->pageTitle;

		$this->render('signup', array(
		));
	}

	public function actionSignIn()
	{
		$this->pageTitle = 'Sign In - '.$this->pageTitle;

		$this->render('signin', array(
		));
	}

	public function actionMemberE()
	{
		$this->pageTitle = 'Member - '.$this->pageTitle;

		$this->render('member_acedit', array(
		));
	}

	public function actionMemberReview()
	{
		$this->pageTitle = 'Member - '.$this->pageTitle;

		$this->render('member_review', array(
		));
	}

	public function actionMemberBlog()
	{
		$this->pageTitle = 'Member - '.$this->pageTitle;

		$this->render('member_blog', array(
		));
	}

	public function actionMemberClinic()
	{
		$this->pageTitle = 'Member - '.$this->pageTitle;

		$this->render('member_clinic', array(
		));
	}

	public function actionCart()
	{
		$this->pageTitle = 'My Bag - '.$this->pageTitle;

		$this->render('cart', array(
		));
	}

	public function actionCart2()
	{
		$this->pageTitle = 'My Bag - '.$this->pageTitle;

		$this->render('cart2', array(
		));
	}

	public function actionCheckout()
	{
		$this->pageTitle = 'Checkout - '.$this->pageTitle;

		$this->render('checkout', array(
		));
	}

}