<?php

class CoachingController extends Controller
{

	public function actionIndex()
	{

		$this->pageTitle = 'Coaching - ' . $this->pageTitle;
		$this->render('index', array());
	}

	public function actionDetail($id)
	{

		$this->pageTitle = 'Coaching - ' . $this->pageTitle;
		$this->render('detail', array(
			'id'=>$id,
		));
	}

	// public function actionDetail($id)
	// {
	// 	$this->layout='//layouts/column1';

	// 	$detail = Artikel::model()->getData($id, $this->languageID);
		
	// 	$dataSub = Artikel::model()->getAllData(5, false, $this->languageID);

	// 	$dataFooter = Artikel::model()->getAllData(3, $id, $this->languageID);

	// 	$this->pageTitle = $detail->title . ' - ' . $this->pageTitle;
	// 	$this->render('detail', array(
	// 		'detail' => $detail,
	// 		'dataSub' => $dataSub,
	// 		'dataFooter' => $dataFooter,
	// 	));
	// }

}