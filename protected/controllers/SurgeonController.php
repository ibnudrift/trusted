<?php

class SurgeonController extends Controller
{

	public function actionWhyClinic()
	{
		// $this->pageTitle = 'Why your Clinic should join - '.$this->pageTitle;
		$metaType = 'why_clinic';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('why_clinic', array(
		));
	}

	public function actionSurgeons_blog()
	{
		// $this->pageTitle = 'Surgeons Blog - '.$this->pageTitle;

		$this->render('surgeon_blog', array(
		));
	}

	public function actionClinic_faq()
	{
		// $this->pageTitle = 'Clinic FAQ - '.$this->pageTitle;
		$metaType = 'clinic_faq';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('clinic_faq', array(
		));
	}

	public function actionAdvertise()
	{
		// $this->pageTitle = 'Clinic FAQ - '.$this->pageTitle;
		$metaType = 'advertise';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('advertise', array(
		));
	}

	public function actionAdvertise2()
	{
		$metaType = 'download';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('advertise2', array(
		));
	}

	public function actionEnquire()
	{
		$this->pageTitle = 'Enquire Form - '.$this->pageTitle;
		$model = new EnquireForm;
		$model->scenario = 'insert';

		if(isset($_POST['EnquireForm']))
		{
			$model->attributes=$_POST['EnquireForm'];

			$status = true;
		    // filter captcha
		    $secret_key = "6LejaCwUAAAAAHfvHbHHxpwCGxNY8X3fijoPytUl";
		    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
		    $response = json_decode($response);
		    if($response->success==false)
		    {
		        $model->addError('verifyCode','Make sure you have finished Captcha.');
		        $status = false;
		    }

		    // 
		    if($status AND $model->validate() )
			{
				// Config email ke Admin
				$messaged = $this->renderPartial('//mail/enquire',array(
					'model'=>$model,
				),TRUE);

				$invoice = 'TS-'.date("Ymd").'-'.rand(1000,9999);
				$config = array(
					'to'=>array($model->email, $this->setting['email']),
					'subject'=>'Trusted Surgeons Enquire '.$model->email,
					'message'=>$messaged,
					'invoice'=>$invoice,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				Common::mail($config);
				
				// Sent email package Gold
			    $dataArray = array(
			        'email'     => $model->email,
			        'status'    => 'subscribed',
			        'firstname' => $model->name,
			        'lastname'  => ''
			    );
				if ($model->package == 'Gold') {
					$messaged_gold = $this->renderPartial('//mail/enquire_gold',array(
					'model'=>$model,
					),TRUE);

					$config_gold = array(
						'to'=>array($model->email),
						'subject'=>'Trusted Surgeons Enquire '.$model->email,
						'message'=>$messaged_gold,
						'invoice'=>$invoice,
					);
					if ($this->setting['contact_cc']) {
						$config_gold['cc'] = array($this->setting['contact_cc']);
					}
					if ($this->setting['contact_bcc']) {
						$config_gold['bcc'] = array($this->setting['contact_bcc']);
					}
					Common::mail($config_gold);
					$http = Common::addMailchimp($dataArray, '5710083e67');
				}

				// sent email package silver
				if ($model->package == 'Silver'){
					$messaged_silver = $this->renderPartial('//mail/enquire_silver',array(
					'model'=>$model,
					),TRUE);

					$config_silver = array(
						'to'=>array($model->email),
						'subject'=>'Trusted Surgeons Enquire '.$model->email,
						'message'=>$messaged_silver,
						'invoice'=>$invoice,
					);
					if ($this->setting['contact_cc']) {
						$config_silver['cc'] = array($this->setting['contact_cc']);
					}
					if ($this->setting['contact_bcc']) {
						$config_silver['bcc'] = array($this->setting['contact_bcc']);
					}
					Common::mail($config_silver);
					$http = Common::addMailchimp($dataArray, '4523dd0987');
				}

				// sent email package platinum
				if ($model->package == 'Platinum'){
					$messaged_platinum = $this->renderPartial('//mail/enquire_platinum',array(
					'model'=>$model,
					),TRUE);
					
					$config_platinum = array(
						'to'=>array($model->email),
						'subject'=>'Trusted Surgeons Enquire '.$model->email,
						'message'=>$messaged_platinum,
						'invoice'=>$invoice,
					);
					if ($this->setting['contact_cc']) {
						$config_platinum['cc'] = array($this->setting['contact_cc']);
					}
					if ($this->setting['contact_bcc']) {
						$config_platinum['bcc'] = array($this->setting['contact_bcc']);
					}
					Common::mail($config_platinum);
					$http = Common::addMailchimp($dataArray, 'e444167ce2');
				}

			    

				Yii::app()->user->setFlash('success','Thank you for contacting us. We will respond to you as soon as possible.');
				if ($model->package == 'Gold') {
					$this->redirect('https://secure.ezidebit.com.au/webddr/Request.aspx?a=9FDA8861-7363-46DD-FA00-613899BD9D5B&freq=4&rDate=0&rAmount='.$this->setting['advertise_gold_price'].'.00&dur=1&businessOrPerson=1&nouref=1&debits=2&nosms=1&PaymentReference='.$invoice);
				} elseif ($model->package == 'Platinum') {
					$this->redirect('https://secure.ezidebit.com.au/webddr/Request.aspx?a=9FDA8861-7363-46DD-FA00-613899BD9D5B&freq=4&rDate=0&rAmount='.$this->setting['advertise_platinum_price'].'.00&dur=1&businessOrPerson=1&nouref=1&debits=2&nosms=1&PaymentReference='.$invoice);
				}
				
				$this->refresh();
			}

		}

		if ($model->package == '') {
			$model->package = ucfirst($_GET['package']);
		}

		$this->render('enquire', array(
			'model'=>$model,
		));
	}

	public function actionAdvertiseenquire()
	{
		$this->pageTitle = 'Advertise Download Form - '.$this->pageTitle;
		$model = new AdvertiseEnquireForm;
		$model->scenario = 'insert';

		if(isset($_POST['AdvertiseEnquireForm']))
		{
			$model->attributes=$_POST['AdvertiseEnquireForm'];

			$status = true;
		    // filter captcha
		    $secret_key = "6LejaCwUAAAAAHfvHbHHxpwCGxNY8X3fijoPytUl";
		    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
		    $response = json_decode($response);
		    if($response->success==false)
		    {
		        $model->addError('verifyCode','Make sure you have finished Captcha.');
		        $status = false;
		    }

			if($status && $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/advertise_enquire',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email']),
					'subject'=>'[Trusted Surgeons] Download Media Guide for '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				Common::mail($config);


				$messaged2 = $this->renderPartial('//mail/advertise_enquire_client',array(
					'model'=>$model,
				),TRUE);
				$config2 = array(
					'to'=>array($model->email, $this->setting['email']),
					'subject'=>'[Trusted Surgeons] Download Media Guide for '.$model->email,
					'message'=>$messaged2,
				);
				if ($this->setting['contact_cc']) {
					$config2['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config2['bcc'] = array($this->setting['contact_bcc']);
				}
				Common::mail($config2);

				Yii::app()->user->setFlash('success','Thank you for filling out the form. Please check your email to download the media guide.');
				$this->refresh();
			}

		}

		$this->render('advertise_enquire', array(
			'model'=>$model,
		));
	}

	public function actionPrivacy()
	{
		// $this->pageTitle = 'Clinic FAQ - '.$this->pageTitle;
		$metaType = 'privacy';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('privacy', array(
		));
	}

	public function actionTos()
	{
		// $this->pageTitle = 'Clinic FAQ - '.$this->pageTitle;
		$metaType = 'tos';
		$this->pageTitle = $this->setting[$metaType.'_meta_title'];
		$this->metaDesc = $this->setting[$metaType.'_meta_description'];
		$this->metaKey = $this->setting[$metaType.'_meta_keyword'];
		$this->metaOg = $this->setting[$metaType.'_open_graph'];

		$this->render('tos', array(
		));
	}


}

