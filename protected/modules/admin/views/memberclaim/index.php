<?php
$this->breadcrumbs=array(
	'Manage Reviews',
);
$this->pageHeader=array(
	'icon'=>'fa fa-life-ring',
	'title'=>'Manage Reviews',
	'subtitle'=>'Manage Reviews Data',
);

$this->menu=array(
	array('label'=>'Add Manage Reviews', 'icon'=>'icon-plus-sign','url'=>array('create')),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<div class="row-fluid">
	<div class="span12">
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Manage Reviews Data</h4>
		    </div>
		    <div class="widgetcontent">
		    	
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'search_name',array('class'=>'span12','maxlength'=>200, 'placeholder'=>'Search Doctor Name')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'search_date_from',array('class'=>'span12 datepick','maxlength'=>200, 'placeholder'=>'Search Date From')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'search_date_to',array('class'=>'span12 datepick','maxlength'=>200, 'placeholder'=>'Search Date To')); ?>
		</div>
		<div class="span3">
			<?php echo $form->dropDownListRow($model,'status',array(
				'1'=>'Managed',
				'2'=>'Unclaimed',
			),array('class'=>'span12','maxlength'=>200, 'empty'=>'All Status')); ?>
		</div>
	</div>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>'Search',
	)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		// 'buttonType'=>'button',
		'type'=>'primary',
		'label'=>'Reset',
		'url'=>Yii::app()->createUrl($this->route),
	)); ?>

<?php $this->endWidget(); ?>

				<hr>
				<?php
				$data = $dataReview->getData();
				?>
				<?php $this->widget('CLinkPager', array(
				    'pages' => $dataReview->getPagination(),
				)) ?>
				<hr>
				<?php foreach ($data as $key => $value): ?>
				<div class="row-fluid">
					<div class="span6">
						<h3 class="title-product">
							<?php if ($value->doctor->n_review > 0): ?>
							<?php echo DoctorReviewCategory::model()->createStar($value->doctor->rating) ?>
                      		&nbsp; 
							<?php endif ?>
                      		<a href="<?php echo CHtml::normalizeUrl(array('/surgeons/detail', 'id'=>$value->doctor_id)); ?>"><?php echo $value->doctor->name ?></a>
                  		</h3>
	                    <h4>
							<?php if ($value->status == 0): ?>
								<span style="color: red">(Unprocess)</span>
							<?php else: ?>
								<span style="color: green">(Processed)</span>
								
							<?php endif ?>
	                     | <?php echo date('d F Y H:i', strtotime($value->date_input)); ?>
	                 	</h4>
	                 	<div class="divider10"></div>
	                 	<?php if ($value->status == 0): ?>
	                 	<a data-id="<?php echo $value->status ?>" href="<?php echo CHtml::normalizeUrl(array('setStatus', 'id'=>$value->id, 'type'=>'status')); ?>" class="btn btn-small btn-featured"><i class="fa fa-reply"></i> Process Now</a>
	                 	<?php endif ?>
					</div>
					<div class="span6" style="text-align: right;">
						<h3 class="title-product">
                      		Claim By: <?php echo $value->member->first_name ?> <?php echo $value->member->last_name ?>
                  		</h3>
	                    <h4>
							Email: <?php echo $value->member->email ?> <br>
							Tel: <?php echo $value->member->phone ?> <br>
	                 	</h4>
					</div>
				</div>
				<hr>
				<?php endforeach ?>
				<?php $this->widget('CLinkPager', array(
				    'pages' => $dataReview->getPagination(),
				)) ?>

		    </div><!--widgetcontent-->
		</div>
	</div>
	<?php /*
	<div class="span4">
		<?php $this->renderPartial('product_category', array(
			'categoryModel'=>$categoryModel,
			'categoryModelDesc'=>$categoryModelDesc,
			'nestedCategory'=>$nestedCategory,
		)) ?>
	</div>
	*/ ?>
</div>
