<?php
$this->breadcrumbs=array(
	'Redirect'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-comments-o',
	'title'=>'Redirect',
	'subtitle'=>'Data Redirect',
);

$this->menu=array(
	array('label'=>'List Redirect', 'icon'=>'th-list','url'=>array('index')),
	// array('label'=>'Add Redirect', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Redirect', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<div class="row-fluid">
	<div class="span8">
		<h1>Edit Redirect</h1>
		<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
	</div>
	<!-- <div class="span4">
		<?php // $this->renderPartial('/pages/page_menu') ?>
	</div> -->
</div>
