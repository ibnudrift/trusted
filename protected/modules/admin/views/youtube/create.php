<?php
$this->breadcrumbs=array(
	'Youtube Video'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-comments-o',
	'title'=>'Youtube Video',
	'subtitle'=>'Data Youtube Video',
);

$this->menu=array(
	array('label'=>'List Youtube Video', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
