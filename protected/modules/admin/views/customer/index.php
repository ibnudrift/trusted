<?php
$this->breadcrumbs=array(
	'Patients/Doctors',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Patients/Doctors',
	'subtitle'=>'Patients/Doctors',
);

$this->menu=array(
	array('label'=>'Add Patients/Doctors', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Patients/Doctors</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'cs-customer-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'email',
		// 'pass',
		'phone',
		'first_name',
		'last_name',
		// 'group_member_id',
		array(
			'name'=>'type',
			'type'=>'raw',
			'value'=>'($data->type == "1") ? "Doctor" : "Member"',
		),
		/*
		// array(
		// 	'name'=>'aktif',
		// 	'filter'=>array(
		// 		'0'=>'Non Active',
		// 		'1'=>'Active',
		// 	),
		// 	'type'=>'raw',
		// 	'value'=>'($data->aktif == "1") ? "Aktif" : "Tidak Aktif"',
		// ),
		'date_join',
		'last_login',
		'data',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete} &nbsp; {doctor}',
			'buttons'=>array(
				'doctor' => array(
				    'label'=>'<i class="fa fa-user"></i>',     // text label of the button
				    'url'=>'CHtml::normalizeUrl(array("/admin/memberclinic/index", "member_id"=>$data->id))',       // a PHP expression for generating the URL of the button
				    'visible'=>'$data->type == "1"',   // a PHP expression for determining whether the button is visible
				),
			),
		),
	),
)); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="row-fluid">
	<div class="span4"><?php echo $form->textFieldRow($model2,'name',array('class'=>'span12','maxlength'=>200, 'placeholder'=>'Search....')); ?></div>
</div>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>'Search',
	)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		// 'buttonType'=>'button',
		'type'=>'primary',
		'label'=>'Reset',
		'url'=>Yii::app()->createUrl($this->route),
	)); ?>
<?php $this->endWidget(); ?>
<h1>Doctor Non Member</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'cs-customer-grid',
	'dataProvider'=>$model2->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'name',
		// 'pass',
		'certification',
		/*
		// array(
		// 	'name'=>'aktif',
		// 	'filter'=>array(
		// 		'0'=>'Non Active',
		// 		'1'=>'Active',
		// 	),
		// 	'type'=>'raw',
		// 	'value'=>'($data->aktif == "1") ? "Aktif" : "Tidak Aktif"',
		// ),
		'date_join',
		'last_login',
		'data',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {edit_doctor} {doctor}',
			'updateButtonUrl'=>'CHtml::normalizeUrl(array("/admin/memberclinic/index", "doctor_id"=>$data->id))',
			'buttons'=>array(
				'doctor' => array(
				    'label'=>'<i class="fa fa-user-md"></i> Create Account',     // text label of the button
				    'url'=>'CHtml::normalizeUrl(array("/admin/customer/create", "doctor_id"=>$data->id))',       // a PHP expression for generating the URL of the button
				    'visible'=>'$data->member_id == "0"',   // a PHP expression for determining whether the button is visible
				    // 'htmlOptions'=>array('class'=>'btn')
				),
				'edit_doctor' => array(
				    'label'=>'<i class="fa fa-user-md"></i>',     // text label of the button
				    'url'=>'CHtml::normalizeUrl(array("/admin/customer/update", "id"=>$data->member_id))',       // a PHP expression for generating the URL of the button
				    'visible'=>'$data->member_id != "0"',   // a PHP expression for determining whether the button is visible
				    // 'htmlOptions'=>array('class'=>'btn')
				),
			),
		),
	),
)); ?>
