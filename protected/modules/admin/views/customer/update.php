<?php
$this->breadcrumbs=array(
	'Patients/Doctors'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Patients/Doctors',
	'subtitle'=>'Edit Patients/Doctors',
);

$this->menu=array(
	array('label'=>'List Patients/Doctors', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Patients/Doctors', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Patients/Doctors', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>