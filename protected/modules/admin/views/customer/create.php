<?php
$this->breadcrumbs=array(
	'Patients/Doctors'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Patients/Doctors',
	'subtitle'=>'Add Patients/Doctors',
);

$this->menu=array(
	array('label'=>'List Patients/Doctors', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>