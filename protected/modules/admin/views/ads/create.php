<?php
$this->breadcrumbs=array(
	'Advertising'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Advertising',
	'subtitle'=>'Add Advertising',
);

$this->menu=array(
	array('label'=>'List Advertising', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>