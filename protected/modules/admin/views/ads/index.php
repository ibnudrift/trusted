<?php
$this->breadcrumbs=array(
	'Advertising',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Advertising',
	'subtitle'=>'Advertising Data',
);

$this->menu=array(
	// array('label'=>'Add Advertising', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<div class="row-fluid">
	<div class="span8">
<h1>Advertising</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'bank-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'name',
		'location',
		'size',
		// 'fax',
		// 'email',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{image}',
			'buttons'=>array(
				'image' => array(
				    'label'=>'<i class="fa fa-picture-o"></i>',     // text label of the button
				    'url'=>'CHtml::normalizeUrl(array("/admin/adsimage/index", "ads_id"=>$data->id))',       // a PHP expression for generating the URL of the button
				    // 'visible'=>'$data->type == "1"',   // a PHP expression for determining whether the button is visible
				),
			),
		),
	),
)); ?>
</div>
	<div class="span4">
		<?php $this->renderPartial('/setting/page_menu') ?>
	</div>
</div>