<?php
$this->breadcrumbs=array(
	'Advertising'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Advertising',
	'subtitle'=>'Edit Advertising',
);

$this->menu=array(
	array('label'=>'List Advertising', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Advertising', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Advertising', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>