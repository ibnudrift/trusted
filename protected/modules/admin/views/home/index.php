<!-- loginpanel -->
<div class="outer-login">
    <div class="in">
        <div class="loginpanelinner">
            <!-- <div class="logo"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/backend/images/logo-2.png" alt="Cms Trusted Surgeons" class="img-responsive" /></div> -->
            <div class="logo"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/logo-footer.png" alt="Cms Trusted Surgeons" class="img-responsive" /></div>
                <?php /** @var BootActiveForm $form */
                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id'=>'verticalForm',
                        //'htmlOptions'=>array('class'=>'well'),
                        'enableClientValidation'=>false,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>false,
                        ),
                    )); ?>
                <div class="inputwrapper login-alert">
                    <div class="alert alert-error">Invalid username or password</div>
                </div>
                <div class="mylogin">
                    <input type="text" name="LoginForm[username]" placeholder="Enter username" class="form-control" />
                </div>
                <div class="mylogin">
                    <input type="password" name="LoginForm[password]" placeholder="Enter password" class="form-control" />
                </div>
                <div class="mylogin">
                    <button name="submit">Sign In</button>
                </div>
                <div class="mylogin mesign">
                    <label><input type="checkbox" class="remember" name="signin" /> Keep me sign in</label>
                </div>
                
            <?php $this->endWidget(); ?>
        </div><!--loginpanelinner-->
    </div>
</div><!--loginpanel-->
<style type="text/css">
    body.loginpage{
        background-color: #fff;
    }
    html,
    body,
    .container{
        height: 100%;
    }
    .outer-login{
        display: table;
        width: 100%;
        height: 95%;
    }
    .outer-login .in{
        display: table-cell;
        text-align: center;
        vertical-align: middle;
    }
    .mylogin input{
        border: 1px solid #adb6ba;
        padding: 10px;
        background: #fff;
        display: block;
        width: 298px;
    }
    .mylogin button{
        /*border: 1px solid #ef1313;
        background: #d10808;*/
        border: 1px solid #235b8f;
        background: #235b8f;
        
        display: block;
        padding: 10px;
        width: 100%;
        color: #fff;
        text-transform: uppercase;
        font-family: 'LatoBold', 'Helvetica Neue', Helvetica, sans-serif;
        font-weight: normal;
        font-size: 13px;
    }
    .mylogin.mesign{
        text-align: left;
    }
    .mylogin label {
        display: inline-block;
        margin-top: 10px;
        color: #555f63;
        font-size: 11px;
        vertical-align: middle;
    }
    .mylogin label input {
        width: auto;
        margin: -3px 5px 0 0;
        vertical-align: middle;
    }
    .mylogin .remember {
        padding: 0;
        background: none;
    }
    .loginpanelinner{
        position: relative;
        top: inherit; 
        left: inherit; 
        min-width: 270px;
        max-width: 320px;
        margin: 0 auto;
        padding: 30px 20px;
        border:1px solid #adb6ba;
    }
    #pushstat{
        display: none;
    }
    .loginfooter{
        color: #555f63;
    }
</style>