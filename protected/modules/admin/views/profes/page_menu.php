<div class="widgetbox block-rightcontent">                        
    <div class="headtitle">
        <h4 class="widgettitle">Setting List</h4>
    </div>
    <div class="widgetcontent">
        <ul class="userlist">
                <li> Why Your Clinic <a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/index')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <li> Surgeons Blogs <a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/blog')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <li> Clinic FAQ <a href="<?php echo CHtml::normalizeUrl(array('/admin/profes/clinic')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
        </ul>
    </div><!--widgetcontent-->
</div>