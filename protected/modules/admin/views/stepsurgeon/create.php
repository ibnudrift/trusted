<?php
$this->breadcrumbs=array(
	'Surgeon organization'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-weixin',
	'title'=>'Surgeon organization',
	'subtitle'=>'Add Surgeon organization',
);

$this->menu=array(
	array('label'=>'List Surgeon organization', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>