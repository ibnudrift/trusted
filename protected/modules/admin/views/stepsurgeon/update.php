<?php
$this->breadcrumbs=array(
	'Surgeon organization'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-weixin',
	'title'=>'Surgeon organization',
	'subtitle'=>'Edit Surgeon organization',
);

$this->menu=array(
	array('label'=>'List Surgeon organization', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Surgeon organization', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Surgeon organization', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>