<div class="widgetbox block-rightcontent">                        
    <div class="headtitle">
        <h4 class="widgettitle">Setting List</h4>
    </div>
    <div class="widgetcontent">
        <ul class="userlist">
                <li> 3D Surgery Animation <a href="<?php echo CHtml::normalizeUrl(array('/admin/education/index')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
                <li> FAQ <a href="<?php echo CHtml::normalizeUrl(array('/admin/education/faq')); ?>" class="editright"> <span class="fa fa-pencil"></span> &nbsp; Edit</a></li>
        </ul>
    </div><!--widgetcontent-->
</div>