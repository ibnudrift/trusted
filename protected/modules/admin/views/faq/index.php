<?php
$this->breadcrumbs=array(
	'Faq',
);

$this->pageHeader=array(
	'icon'=>'fa fa-weixin',
	'title'=>'Faq',
	'subtitle'=>'Faq',
);

$this->menu=array(
	array('label'=>'Add Faq', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>

<div class="row-fluid">
	<div class="span12">
			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'get',
			)); ?>
		<div class="span3">
			<?php echo $form->dropDownList($model,'topic',array(
			''=>'-- Choose Category Faq --',
			'1'=>'Trusted surgeons',
			'2'=>'Surgery',
			'3'=>'Travel',
		),array('class'=>'span11')); ?>
		</div>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>'Search',
	)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		// 'buttonType'=>'button',
		'type'=>'primary',
		'label'=>'Reset',
		'url'=>Yii::app()->createUrl($this->route),
	)); ?>
	<?php $this->endWidget(); ?>
	</div>
</div>

<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>

<div class="row-fluid">
	<div class="span8">
<h1>Faq</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'faq-grid',
	'dataProvider'=>$model->search($this->languageID),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'question',
		// 'answer',
		array(
					'name'=>'status',
					'filter'=>array(
						'0'=>'Non Active',
						'1'=>'Active',
					),
					'type'=>'raw',
					'value'=>'($data->status == "1") ? "Show" : "Hide"',
				),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
</div>
	<div class="span4">
		<?php $this->renderPartial('/setting/page_menu') ?>
	</div>
</div>