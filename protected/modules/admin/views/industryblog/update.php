<?php
$this->breadcrumbs=array(
	'Industry Blog'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-comments-o',
	'title'=>'Industry Blog',
	'subtitle'=>'Data Industry Blog',
);

$this->menu=array(
	array('label'=>'List Industry Blog', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Industry Blog', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Industry Blog', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
