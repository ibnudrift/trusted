<?php
$this->breadcrumbs=array(
	'Industry Blog'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-comments-o',
	'title'=>'Industry Blog',
	'subtitle'=>'Data Industry Blog',
);

$this->menu=array(
	array('label'=>'List Industry Blog', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
		<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
