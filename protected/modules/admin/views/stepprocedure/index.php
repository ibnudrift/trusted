<?php
$this->breadcrumbs=array(
	'List Procedure',
);

$this->pageHeader=array(
	'icon'=>'fa fa-weixin',
	'title'=>'List Procedure',
	'subtitle'=>'List Procedure',
);

$this->menu=array(
	array('label'=>'Add List Procedure', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Back to step 3', 'icon'=>'plus-sign','url'=>array('/admin/journey/step3')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<div class="row-fluid">
	<div class="span8">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->dropDownList($model,'category_id',array(
				'1'=>'Face',
				'2'=>'Breast',
				'3'=>'Body',

			), array('class'=>'span12','maxlength'=>200, 'empty'=>'Search Category')); ?>
		</div>
		<div class="span4">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'Search',
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				// 'buttonType'=>'button',
				'type'=>'primary',
				'label'=>'Reset',
				'url'=>Yii::app()->createUrl($this->route),
			)); ?>
		</div>
	</div>



<?php $this->endWidget(); ?>
<h1>List Procedure</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'faq-grid',
	'dataProvider'=>$model->search($this->languageID),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'title',
		// 'url',
		// 'answer',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
</div>
	<div class="span4">
		<?php $this->renderPartial('/journey/_page_menu') ?>
	</div>
</div>