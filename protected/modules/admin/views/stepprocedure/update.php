<?php
$this->breadcrumbs=array(
	'List Procedure'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-weixin',
	'title'=>'List Procedure',
	'subtitle'=>'Edit List Procedure',
);

$this->menu=array(
	array('label'=>'List Procedure', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add List Procedure', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View List Procedure', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>