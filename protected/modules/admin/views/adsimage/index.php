<?php $ads = Ads::model()->findByPk($_GET['ads_id']) ?>
<?php
$this->breadcrumbs=array(
	'Ads '.$ads->name,
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Advertising Image',
	'subtitle'=>'Ads '.$ads->name,
);

$this->menu=array(
	array('label'=>'Add Advertising Image', 'icon'=>'plus-sign','url'=>array('create', 'ads_id'=>$_GET['ads_id'])),
	array('label'=>'Back', 'icon'=>'chevron-left','url'=>array('/admin/ads/index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<div class="row-fluid">
	<div class="span12">
<h1>Ads <?php echo $ads->name ?></h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'bank-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		array(
	        'name'=>'image',
	        'type'=>'html',
	        'value'=>'CHtml::image(Yii::app()->baseUrl.ImageHelper::thumb(150,150, "/images/ads_image/".$data->image , array("method" => "resize", "quality" => "90")),"",array())',
	    ),
		'url',
		'view',
		'sort',
		array(
			'name'=>'active',
			'filter'=>array(
				'0'=>'Hide',
				'1'=>'Show',
			),
			'type'=>'raw',
			'value'=>'($data->active == "1") ? "Show" : "Hide"',
		),
		// 'fax',
		// 'email',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
			'updateButtonUrl'=>'CHtml::normalizeUrl(array("/admin/adsimage/update", "id"=>$data->id, "ads_id"=>$_GET["ads_id"]))',
			'deleteButtonUrl'=>'CHtml::normalizeUrl(array("/admin/adsimage/delete", "id"=>$data->id, "ads_id"=>$_GET["ads_id"]))'
			// 'buttons'=>array(
			// 	'image' => array(
			// 	    'label'=>'<i class="fa fa-picture-o"></i>',     // text label of the button
			// 	    'url'=>'CHtml::normalizeUrl(array("/admin/adsimage/index", "ads_id"=>$data->id))',       // a PHP expression for generating the URL of the button
			// 	    // 'visible'=>'$data->type == "1"',   // a PHP expression for determining whether the button is visible
			// 	),
			// ),
		),
	),
)); ?>
</div>
</div>