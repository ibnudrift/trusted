<?php $ads = Ads::model()->findByPk($_GET['ads_id']) ?>
<?php
$this->breadcrumbs=array(
	'Ads '.$ads->name=>array('index', 'ads_id'=>$_GET['ads_id']),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Advertising Image',
	'subtitle'=>'Ads '.$ads->name,
);

$this->menu=array(
	array('label'=>'List Advertising Image', 'icon'=>'th-list','url'=>array('index', 'ads_id'=>$_GET['ads_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>