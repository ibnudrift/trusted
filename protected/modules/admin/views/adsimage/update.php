<?php $ads = Ads::model()->findByPk($_GET['ads_id']) ?>
<?php
$this->breadcrumbs=array(
	'Ads '.$ads->name =>array('index', 'ads_id'=>$_GET['ads_id']),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Advertising Image',
	'subtitle'=>'Edit '.$ads->name,
);

$this->menu=array(
	array('label'=>'List Advertising Image', 'icon'=>'th-list','url'=>array('index', 'ads_id'=>$_GET['ads_id'])),
	array('label'=>'Add Advertising Image', 'icon'=>'plus-sign','url'=>array('create', 'ads_id'=>$_GET['ads_id'])),
	// array('label'=>'View Advertising Image', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>