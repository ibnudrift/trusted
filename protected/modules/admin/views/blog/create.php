<?php
$this->breadcrumbs=array(
	'Blog'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-comments-o',
	'title'=>'Blog',
	'subtitle'=>'Data Blog',
);

$this->menu=array(
	array('label'=>'List Blog', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
