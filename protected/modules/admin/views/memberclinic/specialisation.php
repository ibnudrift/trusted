<?php
$this->breadcrumbs=array(
	'Doctor',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Doctor',
	'subtitle'=>'Doctor',
);

$this->menu=array(
	array('label'=>'Back', 'icon'=>'arrow-left','url'=>array('/admin/customer/index')),
	array('label'=>'Clinic', 'icon'=>'glass','url'=>array('index', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Specialisation', 'icon'=>'heart','url'=>array('specialisation', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'About Us', 'icon'=>'star','url'=>array('about', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Photo', 'icon'=>'camera','url'=>array('photo', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Video', 'icon'=>'film','url'=>array('video', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>

<div class="row-fluid">
	<div class="span8">

		<div class="widget">
		<h4 class="widgettitle">Specialization</h4>
		<div class="widgetcontent">
	
			<?php 
			$dataCategory = (PrdCategory::model()->categoryTree('spesialis', $this->languageID));
			$modelSpesialis = DoctorSpecialication::model()->findAll('doctor_id = :doctor_id', array(':doctor_id'=>$model->id));
			$dataKey = array();
			foreach ($modelSpesialis as $key => $value) {
			  $dataKey[$value->specialitation_id] = 1;
			}
			?>
			<form action="" method="post">
			  <?php foreach ($dataCategory as $key => $value): ?>

			  <div class="box_default prelatife">
			    <h3 class="sub_title"><?php echo $value['title'] ?></h3>
			        <div class="clear"></div>
			          <?php if (count($value['children']) > 0) { ?>
			          <?php 
			          $dataChild = array_chunk($value['children'], 3);
			          ?>
			          <?php foreach ($dataChild as $k_child => $v_child): ?>
			        <div class="row-fluid">
			          <?php foreach ($v_child as $k => $v): ?>
			          <div class="span4">
			            <div class="form-group">
			              <div class="checkbox">
			              <label>

			                <input type="checkbox" <?php if (array_key_exists($v['id'], $dataKey)): ?>checked="checked"<?php endif ?> name="Specialisation[<?php echo $v['id'] ?>]" value="1"> <?php echo $v['title'] ?>
			              </label>
			            </div>
			            </div>
			          </div>
			          <?php endforeach ?>
			        </div>
			          <?php endforeach ?>
			          <?php } ?>
			  </div>
			  <div class="clear height-35"></div>
			  <?php endforeach ?>
		      <button class="btn btn-primary">SUBMIT</button>
			</form>

		</div>
		</div>
	</div>
	<div class="span4">
		<div class="widgetbox block-rightcontent">                        
		</div>
	</div>
</div>


