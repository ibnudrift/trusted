<?php
$this->breadcrumbs=array(
	'Doctor',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Doctor',
	'subtitle'=>'Doctor',
);

$this->menu=array(
	array('label'=>'Back', 'icon'=>'arrow-left','url'=>array('/admin/customer/index')),
	array('label'=>'Clinic', 'icon'=>'glass','url'=>array('index', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Specialisation', 'icon'=>'heart','url'=>array('specialisation', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'About Us', 'icon'=>'star','url'=>array('about', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Photo', 'icon'=>'camera','url'=>array('photo', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Video', 'icon'=>'film','url'=>array('video', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>

<div class="row-fluid">
	<div class="span8">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cs-customer-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php // echo $form->errorSummary($model); ?>
		<div class="widget">
		<h4 class="widgettitle">Photo</h4>
		<div class="widgetcontent">
<?php
$criteria = new CDbCriteria;
$criteria->addCondition('doctor_id = :doctor_id');
$criteria->params[':doctor_id'] = $model->id;
$dataPhoto = new CActiveDataProvider('DoctorPhoto', array(
	'criteria'=>$criteria,
));
?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'promotion-grid',
	'dataProvider'=>$dataPhoto,
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		'title',
		// 'phone',
		// 'website',
		// 'suburb',
		// array(
		// 	'name'=>'last_update_by',
		// ),
		// array(
		// 	'name'=>'active',
		// 	'filter'=>array(
		// 		'0'=>'Non Active',
		// 		'1'=>'Active',
		// 	),
		// 	'type'=>'raw',
		// 	'value'=>'($data->active == "1") ? "Show" : "Hide"',
		// ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'deleteButtonUrl'=>'CHtml::normalizeUrl(array("photo", "member_id"=>"'.$member_id.'", "doctor_id"=>"'.(($_GET['doctor_id'] == '') ? 0 : $_GET['doctor_id']).'", "delete"=>$data->id))',
			'updateButtonUrl'=>'CHtml::normalizeUrl(array("photo", "member_id"=>"'.$member_id.'", "doctor_id"=>"'.(($_GET['doctor_id'] == '') ? 0 : $_GET['doctor_id']).'", "update"=>$data->id))',
		),
	),
)); ?>
			<?php echo $form->errorSummary($modelPhoto); ?>
			<div class="row-fluid">
				<div class="span8">
					<?php echo $form->textFieldRow($modelPhoto,'title',array('class'=>'span12', 'placeholder'=>'Title')); ?>
					<?php // echo $form->textFieldRow($modelPhoto,'procedures_id',array('class'=>'span12', 'placeholder'=>'Procedures')); ?>
				</div>
				<div class="span4">
					<?php echo $form->fileFieldRow($modelPhoto,'image',array(
					'hint'=>'<b>Note:</b> Image size is 650 x 650px. Larger image will be automatically cropped.', 'style'=>"width: 100%")); ?>
					<?php if ($modelPhoto->scenario == 'update'): ?>
					<img style="width: 100%;" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(650,650, '/images/doctor_photo/'.$modelPhoto->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
					<?php endif; ?>
				</div>
			</div>

			<?php if ($modelPhoto->scenario == 'update'): ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				// 'buttonType'=>'submit',
				// 'type'=>'info',
				'url'=>CHtml::normalizeUrl(array('photo', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
				'label'=>'Back',
				'htmlOptions'=>array('class'=>'btn-large'),
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'Update Photo',
				'htmlOptions'=>array('class'=>'btn-large'),
			)); ?>
			<?php else: ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'Add Photo',
				'htmlOptions'=>array('class'=>'btn-large'),
			)); ?>
			<?php endif ?>

		</div>
		</div>

<?php $this->endWidget(); ?>


	</div>
	<div class="span4">
		<div class="widgetbox block-rightcontent">                        
		</div>
	</div>
</div>


