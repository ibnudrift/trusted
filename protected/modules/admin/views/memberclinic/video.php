<?php
$this->breadcrumbs=array(
	'Doctor',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Doctor',
	'subtitle'=>'Doctor',
);

$this->menu=array(
	array('label'=>'Back', 'icon'=>'arrow-left','url'=>array('/admin/customer/index')),
	array('label'=>'Clinic', 'icon'=>'glass','url'=>array('index', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Specialisation', 'icon'=>'heart','url'=>array('specialisation', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'About Us', 'icon'=>'star','url'=>array('about', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Photo', 'icon'=>'camera','url'=>array('photo', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Video', 'icon'=>'film','url'=>array('video', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>

<div class="row-fluid">
	<div class="span8">

		<div class="widget">
		<h4 class="widgettitle">Video</h4>
		<div class="widgetcontent">
			<form action="" method="post">
	            <div class="video-tempel">
	              <?php foreach ($modelVideo as $key => $value): ?>
	              <div class="col-md-12">
	                <div class="form-group">
	                  <input type="hidden" name="DoctorVideo[id_str][]" value="<?php echo $value->id_str ?>">
	                  <input type="text" class="span12" name="DoctorVideo[url][]" value="<?php echo $value->url ?>" placeholder="Video Link">
	                </div>
	              </div>
	              <?php endforeach ?>
	            </div>
	            <div class="video-add">
	              <div class="col-md-12">
	                <div class="form-group">
	                  <input type="hidden" name="DoctorVideo[id_str][]">
	                  <input type="text" class="span12" name="DoctorVideo[url][]" placeholder="Video Link">
	                </div>
	              </div>
	            </div>
				<button type="button" class="btn btn-default tambah-video">ADD VIDEO</button>
				<button type="submit" class="btn btn-primary">SUBMIT</button>

                <script type="text/javascript">
                jQuery(function( $ ) {
					$('.tambah-video').tambahData({
						targetHtml: '.video-add',
						// html: '<tr><td></td></tr>',
						tambahkan: '.video-tempel',
					});
				})

                </script>
        	</form>
		</div>
		</div>
	</div>
	<div class="span4">
		<div class="widgetbox block-rightcontent">                        
		</div>
	</div>
</div>


