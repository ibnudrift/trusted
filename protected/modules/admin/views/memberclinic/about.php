<?php
$this->breadcrumbs=array(
	'Doctor',
);

$this->pageHeader=array(
	'icon'=>'fa fa-group',
	'title'=>'Doctor',
	'subtitle'=>'Doctor',
);

$this->menu=array(
	array('label'=>'Back', 'icon'=>'arrow-left','url'=>array('/admin/customer/index')),
	array('label'=>'Clinic', 'icon'=>'glass','url'=>array('index', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Specialisation', 'icon'=>'heart','url'=>array('specialisation', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'About Us', 'icon'=>'star','url'=>array('about', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Photo', 'icon'=>'camera','url'=>array('photo', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
	array('label'=>'Update Video', 'icon'=>'film','url'=>array('video', 'member_id'=>$member_id, 'doctor_id'=>$_GET['doctor_id'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
<?php $this->widget('ImperaviRedactorWidget', array(
    'selector' => '.redactor',
    // 'options' => array(
    //     'imageUpload'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'image')),
    //     'clipboardUploadUrl'=> $this->createUrl('/admin/setting/imgUpload', array('type'=>'clip')),
    // ),
)); ?>
<div class="row-fluid">
	<div class="span8">

		<div class="widget">
		<h4 class="widgettitle">About</h4>
		<div class="widgetcontent">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    // 'id'=>'daftar-form',
    // 'type'=>'horizontal',
  'enableClientValidation'=>false,
  'clientOptions'=>array(
    'validateOnSubmit'=>false,
  ),
)); ?>
			<?php echo $form->textAreaRow($model,'about',array('class'=>'form-control redactor')); ?>
			<div class="divider15"></div>
			<button type="submit" class="btn btn-primary">SUBMIT</button>
<?php $this->endWidget(); ?>
		</div>
		</div>
	</div>
	<div class="span4">
		<div class="widgetbox block-rightcontent">                        
		</div>
	</div>
</div>


