<?php

class MemberreviewController extends ControllerAdmin
{
	public $layout = '//layoutsAdmin/column2';

	public function actionIndex()
	{
		// https://www.youtube.com/watch?v=mC2q2SGy2e0
		// $data = file_get_contents("http://youtube.com/get_video_info?video_id=".'mC2q2SGy2e0');
		// parse_str($data, $output);
		$model = new DoctorReview;

		$criteria = new CDbCriteria;
		$criteria->with = array(
			'doctor'=>array(
				// 'select'=>false,
			),
		);
		
		if ($_GET['review_order'] == 'featured') {
		  $criteria->order = 'featured DESC, date_input DESC';
		} elseif($_GET['review_order'] == 'higest') {
		  $criteria->order = 't.date_input ASC';
		} elseif($_GET['review_order'] == 'lowest') {
		  $criteria->order = 't.date_input DESC';
		} elseif($_GET['review_order'] == 'recent') {
		} else {
		  $criteria->order = 't.date_input DESC';
		}
		if ($_GET['DoctorReview']) {
			$model->attributes = $_GET['DoctorReview'];
			if ($model->search_name != '') {
				$criteria->addCondition('doctor.name LIKE :name');
				$criteria->params[':name'] = '%'.$model->search_name.'%';
			}
			if ($model->search_date_from != '' AND $model->search_date_to == '') {
				$criteria->addCondition('t.date_input > :date_from');
				$criteria->params[':date_from'] = date('Y-m-d H:i:s',strtotime($model->search_date_from.' 00:00:01'));
			}
			if ($model->search_date_to != '' AND $model->search_date_from == '') {
				$criteria->addCondition('t.date_input < :date_to');
				$criteria->params[':date_to'] = date('Y-m-d H:i:s',strtotime($model->search_date_to.' 23:59:59'));
			}
			if ($model->search_date_from != '' AND $model->search_date_to != '') {
				$criteria->addCondition('t.date_input > :date_from AND t.date_input < :date_to');
				$criteria->params[':date_from'] = date('Y-m-d H:i:s',strtotime($model->search_date_from.' 00:00:01'));
				$criteria->params[':date_to'] = date('Y-m-d H:i:s',strtotime($model->search_date_to.' 23:59:59'));
			}
			if ($model->search_status != '') {
				if ($model->search_status == 1) {
					$criteria->addCondition('doctor.member_id != 0');
				}elseif ($model->search_status == 2){
					$criteria->addCondition('doctor.member_id = 0');
				}
			}
			if ($model->search_review_status != '') {
				if ($model->search_review_status == 1) {
					$criteria->addCondition('t.status = 0');
				} elseif($model->search_review_status == 2) {
					$criteria->addCondition('t.status = 1');
				} elseif($model->search_review_status == 3) {
					$criteria->addCondition('t.featured = 1');
				}
				
			}
		}

		$dataReview = new CActiveDataProvider('DoctorReview', array(
		  'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		));

		$this->render('index', array(
			'model'=>$model,
			'dataReview'=>$dataReview,
		));	
	}

	public function actionApprove($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$data->status = 1;
		$data->save(false);
		
		$dataDoctor = Doctor::model()->findByPk($data->doctor_id);
		
		//rating keseluruhan
		$dataReviewValue = Yii::app()->db->createCommand()
		    ->select('SUM(`doctor_category_review`.`value`)/COUNT(`doctor_category_review`.`value`) as `value`')
		    // ->select('*')
		    ->from('doctor_review')
		    ->join('doctor_category_review', 'doctor_review.id = doctor_category_review.review_id')
		    ->where('doctor_review.doctor_id=:doctor_id', array(':doctor_id'=>$dataDoctor->id))
		    ->group('doctor_review.doctor_id')
		    ->queryRow();
		$dataDoctor->rating = $dataReviewValue['value'];

		//jumlah yang mereview
		$criteria = new CDbCriteria;
		$criteria->addCondition('doctor_id = :doctor_id');
		$criteria->addCondition('status = 1');
		$criteria->params[':doctor_id'] = $dataDoctor->id;
		$jmlReview = DoctorReview::model()->count($criteria);

		$dataDoctor->n_review = $jmlReview;
		$dataDoctor->save(false);

		Yii::app()->user->setFlash('success','You have approved the review');

		$this->redirect(array('index'));
	}

	public function actionHide($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$data->status = 0;
		$data->save(false);
		$dataDoctor = Doctor::model()->findByPk($data->doctor_id);
		
		//rating keseluruhan
		$dataReviewValue = Yii::app()->db->createCommand()
		    ->select('SUM(`doctor_category_review`.`value`)/COUNT(`doctor_category_review`.`value`) as `value`')
		    // ->select('*')
		    ->from('doctor_review')
		    ->join('doctor_category_review', 'doctor_review.id = doctor_category_review.review_id')
		    ->where('doctor_review.doctor_id=:doctor_id', array(':doctor_id'=>$dataDoctor->id))
		    ->group('doctor_review.doctor_id')
		    ->queryRow();
		$dataDoctor->rating = $dataReviewValue['value'];

		//jumlah yang mereview
		$criteria = new CDbCriteria;
		$criteria->addCondition('doctor_id = :doctor_id');
		$criteria->addCondition('status = 1');
		$criteria->params[':doctor_id'] = $dataDoctor->id;
		$jmlReview = DoctorReview::model()->count($criteria);

		$dataDoctor->n_review = $jmlReview;
		$dataDoctor->save(false);

		Yii::app()->user->setFlash('success','You have to hide a review');

		$this->redirect(array('index'));
	}

	public function actionFeatured($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$data->featured = 1;
		$data->save(false);
		Yii::app()->user->setFlash('success','You have featured the review');
		$this->redirect(array('index'));
	}

	public function actionDel_featured($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$data->featured = 0;
		$data->save(false);
		Yii::app()->user->setFlash('success','You have delete featured review');
		$this->redirect(array('index'));
	}

	public function actionDelete($id='')
	{
		$data = DoctorReview::model()->findByPk($id);
		$dataDoctor = Doctor::model()->findByPk($data->doctor_id);
		$data->delete();
		
		//rating keseluruhan
		$dataReviewValue = Yii::app()->db->createCommand()
		    ->select('SUM(`doctor_category_review`.`value`)/COUNT(`doctor_category_review`.`value`) as `value`')
		    // ->select('*')
		    ->from('doctor_review')
		    ->join('doctor_category_review', 'doctor_review.id = doctor_category_review.review_id')
		    ->where('doctor_review.doctor_id=:doctor_id', array(':doctor_id'=>$dataDoctor->id))
		    ->group('doctor_review.doctor_id')
		    ->queryRow();
		$dataDoctor->rating = $dataReviewValue['value'];

		//jumlah yang mereview
		$criteria = new CDbCriteria;
		$criteria->addCondition('doctor_id = :doctor_id');
		$criteria->addCondition('status = 1');
		$criteria->params[':doctor_id'] = $dataDoctor->id;
		$jmlReview = DoctorReview::model()->count($criteria);

		$dataDoctor->n_review = $jmlReview;
		$dataDoctor->save(false);

		Yii::app()->user->setFlash('success','You have deleted the review');
		$this->redirect(array('index'));
	}
	public function actionSetStatus($id, $type)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = DoctorReview::model()->findByPk($id);
			$model->{$type} = ($model->{$type}-1)*-1;
			$model->save();

			$dataDoctor = Doctor::model()->findByPk($model->doctor_id);
			//rating keseluruhan
			$dataReviewValue = Yii::app()->db->createCommand()
			    ->select('SUM(`doctor_category_review`.`value`)/COUNT(`doctor_category_review`.`value`) as `value`')
			    // ->select('*')
			    ->from('doctor_review')
			    ->join('doctor_category_review', 'doctor_review.id = doctor_category_review.review_id')
			    ->where('doctor_review.doctor_id=:doctor_id', array(':doctor_id'=>$dataDoctor->id))
			    ->group('doctor_review.doctor_id')
			    ->queryRow();
			$dataDoctor->rating = $dataReviewValue['value'];

			//jumlah yang mereview
			$criteria = new CDbCriteria;
			$criteria->addCondition('doctor_id = :doctor_id');
			$criteria->addCondition('status = 1');
			$criteria->params[':doctor_id'] = $dataDoctor->id;
			$jmlReview = DoctorReview::model()->count($criteria);

			$dataDoctor->n_review = $jmlReview;
			$dataDoctor->save(false);
			echo $model->{$type};
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

}